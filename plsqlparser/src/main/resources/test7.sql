-- Handling of EXCEPTION_INIT Pragma
 function upddelplusepielehd_f (i_id in chetepielehd.id%type)
      return number
   is
      row_is_locked   exception;
      pragma exception_init (row_is_locked, -54);
   begin
end;
