--CREATE TYPE project_t AS OBJECT(pno NUMBER, pname VARCHAR2(80));

--CREATE TYPE project_set AS TABLE OF project_t;

CREATE TABLE proj_tab (eno NUMBER, projects PROJECT_SET)
    NESTED TABLE randomName STORE AS emp_project_tab
                ((PRIMARY KEY(nested_table_id, pno))
    ORGANIZATION INDEX)
    RETURN AS LOCATOR;
