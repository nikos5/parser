CREATE TABLE part_order_items 
(
    order_id        NUMBER(12) PRIMARY KEY,
    line_item_id    NUMBER(3),
    product_id      NUMBER(6) NOT NULL,
    unit_price      NUMBER(8,2),
    quantity        NUMBER(8),
    CONSTRAINT product_id_fk FOREIGN KEY (product_id) REFERENCES other_table(product_id)
	)
 PARTITION BY REFERENCE (product_id_fk);