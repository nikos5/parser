CREATE TABLE contacts
( last_name VARCHAR(30) NOT NULL,
  first_name VARCHAR(25) NOT NULL,
  birthday DATE,
  CONSTRAINT contacts_pk PRIMARY KEY (last_name, first_name)
);