CREATE MATERIALIZED VIEW foreign_customers FOR UPDATE
   AS SELECT * FROM sh.customers@remote cu
   WHERE EXISTS
     (SELECT * FROM sh.countries@remote co
      WHERE co.country_id = cu.country_id);

--table with foreign key
CREATE TABLE part_order_items 
(
    order_id        NUMBER(12) PRIMARY KEY,
    line_item_id    NUMBER(3),
    product_id      NUMBER(6) NOT NULL,
    unit_price      NUMBER(8,2),
    quantity        NUMBER(8),
    CONSTRAINT product_id_fk FOREIGN KEY (product_id) REFERENCES other_table(product_id)
	)
 PARTITION BY REFERENCE (product_id_fk);
 
-- packageLessFunction created problem to parser
 CREATE OR REPLACE FUNCTION packageLessFunction
RETURN number IS
   total number(2) := 0;
BEGIN
   SELECT count(*) into total
   FROM customers;
   
   RETURN total;
  END; 
  
CREATE OR REPLACE PACKAGE packageTen AS  -- spec
   PROCEDURE procedureSomething (deptno NUMBER);
END packageThree;

CREATE OR REPLACE PACKAGE BODY packageTen AS  -- body
   PROCEDURE procedureSomething (deptno NUMBER) IS
   BEGIN
   sch1.pck2.HelloPPL();
   pck4.WoW();
   izi.HelloPPL();
   miniPack.trial();
   pcOne.procedureOne ( 1 ) ;
   END procedureThree;
END packageTen;

CREATE OR REPLACE PROCEDURE packageLessProcedure IS
BEGIN
      packageTwo.procedureThree( 2 ); 
END packageLessProcedure;