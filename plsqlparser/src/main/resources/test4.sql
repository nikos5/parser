   function GetColLASTNAME ( i_ID in PAYTPAYAFMBPS.ID%Type)
            return PAYTPAYAFMBPS.LASTNAME%Type is

--    Επώνυμο
--    created 12/11/2015  11:11 by CreateTabApixxx

  cursor GetLASTNAME (c_ID in PAYTPAYAFMBPS.ID%Type) is
      select LASTNAME
        from PAYTPAYAFMBPS
       where ID = c_ID;
  GetLASTNAMERec GetLASTNAME%RowType;
begin  --GetColLASTNAME
   if i_ID is not null then
      open GetLASTNAME ( i_ID );
      fetch GetLASTNAME into GetLASTNAMERec;
      if GetLASTNAME%Found then
         close GetLASTNAME;
         return GetLASTNAMERec.LASTNAME;
      else
         close GetLASTNAME;
         return null;
      end if;
   else
      return null;
   end if;
exception
   when others then
      if GetLASTNAME%IsOpen then
         close GetLASTNAME;
      end if;
      gaee.np$Err.Unhandled_Exception('DB Pkg: PAYDApiPABPS.GetColLASTNAME');
end GetColLASTNAME;