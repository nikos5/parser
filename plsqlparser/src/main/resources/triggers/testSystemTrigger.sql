CREATE OR REPLACE TRIGGER Audit_DDLS
  AFTER CREATE ON schemaTest.SCHEMA
BEGIN
  INSERT INTO Audit_DDL_OPS
    (ObjectOwner,
     ObjectType,
     ObjectName,
     CreationDate
    )
  VALUES
    (ORA_DICT_OBJ_OWNER,
     ORA_DICT_OBJ_TYPE,
     ORA_OBJ_DICT_NAME,
     SYSDATE
    );
END;