CREATE OR REPLACE PACKAGE schema1.packageOne AS
   PROCEDURE procedureOne (id NUMBER);
   PROCEDURE procedureSix (id NUMBER);
   PROCEDURE procedureSeven ( id NUMBER);
END packageOne;

CREATE OR REPLACE PACKAGE BODY schema1.packageOne AS
   PROCEDURE procedureOne (id NUMBER) IS
   BEGIN
    packageTwo.functionOne( 2 );
    schema2.packageFour.procedureTwo(3);
    schema3.packageFive.procedureThree(1);
    schema3.packageFive.procedureThree(1);
   END procedureOne;

   PROCEDURE procedureSix(id NUMBER) IS
   BEGIN
     schema2.packageThree.procedureFour(2);
     schema2.packageThree.procedureFour(2);
     schema2.packageThree.procedureFive(3);
     procedureSeven(3);
   END procedureSix;

   PROCEDURE procedureSeven(id NUMBER) IS
   BEGIN
     packageThree.procedureFour(2);
     packageThree.procedureFour(2);
     packageThree.procedureFive(3);
   END procedureSeven;

END packageOne;

CREATE OR REPLACE PACKAGE schema1.packageTwo AS
  FUNCTION functionOne (id NUMBER) RETURN NUMBER;
  PROCEDURE packageUniter( id NUMBER);
END packageTwo;

CREATE OR REPLACE PACKAGE BODY schema1.packageTwo AS
  FUNCTION functionOne (id number) RETURN NUMBER AS  total NUMBER;
     BEGIN
      total:=id - 1;
      RETURN total;
  END functionOne;
  PROCEDURE packageUniter(id NUMBER) IS
  BEGIN
  	schema1.packageOne.procedureOne(2);
  	schema2.packageThree.procedureFive(3);
  END packageUniter;
END packageTwo;
-- page changeeeeeeeeeeeeeee
CREATE OR REPLACE PACKAGE schema2.packageThree AS
   PROCEDURE procedureFour (id NUMBER);
   PROCEDURE procedureFive (id NUMBER);
END packageThree;

CREATE OR REPLACE PACKAGE BODY schema2.packageThree AS
   PROCEDURE procedureFour (id NUMBER) IS
   BEGIN
    schema1.packageOne.procedureSix( 2 );
    procedureFive(3);
    schema1.packageOne.procedureSeven(1);
    schema1.packageOne.procedureSeven(1);
   END procedureFour;

   PROCEDURE procedureFive (id NUMBER) IS
   BEGIN
     procedureFour(2);
     procedureFour(2);
     procedureFour(2);
     schema1.packageOne.procedureSix(2);
   END procedureFive;
END packageThree;

CREATE OR REPLACE PACKAGE schema2.packageFour AS
  PROCEDURE procedureTwo (id NUMBER);
END packageFour;

CREATE OR REPLACE PACKAGE BODY schema2.packageFour AS


  PROCEDURE procedureTwo (id NUMBER) IS
  BEGIN
    schema1.packageOne.procedureOne(2);
    schema1.packageOne.procedureOne(2);
    schema1.packageOne.procedureOne(2);
    schema3.packageFive.procedureThree(2);
  END procedureTwo;
END packageFour;

CREATE OR REPLACE PACKAGE packageFive AS
  PROCEDURE packageUniter (id NUMBER);
END packageFour;

CREATE OR REPLACE PACKAGE BODY schema3.packageFive AS

PROCEDURE procedureThree(id NUMBER) IS
BEGIN
  schema1.packageOne.procedureOne(2);
  schema1.packageOne.procedureOne(2);
  schema2.packageFour.procedureTwo(3);
END procedureThree;

END packageFive;
