CREATE PUBLIC SYNONYM iziC2 FOR izi;  
CREATE PUBLIC SYNONYM pcOneD FOR schema1.packageOne; 
CREATE PUBLIC SYNONYM miniPack FOR packageSomething;
CREATE PUBLIC SYNONYM sch1.pck2 FOR pck3@something;
CREATE PUBLIC SYNONYM pck4 FOR sch4.pck5@linksomething;
CREATE PUBLIC SYNONYM izi FOR sch1.pck2;
CREATE PUBLIC SYNONYM packageTen FOR testingSchema.testingPackage;

CREATE OR REPLACE PACKAGE packageTen AS  -- spec
   PROCEDURE procedureSomething (deptno NUMBER);
END packageThree;

CREATE OR REPLACE PACKAGE BODY packageTen AS  -- body
   PROCEDURE procedureSomething (deptno NUMBER) IS
   BEGIN
   sch1.pck2.HelloPPL();
   pck4.WoW();
   izi.HelloPPL();
   miniPack.trial();
   pcOne.procedureOne ( 1 ) ;
   END procedureThree;
END packageTen;