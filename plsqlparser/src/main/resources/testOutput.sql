CREATE OR REPLACE PACKAGE schema1.packageOne AS   
   PROCEDURE procedureOne (deptno NUMBER);
END packageOne;

CREATE OR REPLACE PACKAGE BODY schema1.packageOne AS   
   PROCEDURE procedureOne (deptno NUMBER) IS
   BEGIN
      schema2.packageTwo.procedureThree( 2 );
      sch3.pack3.proc4@here( 1 );
   END procedureOne;

END packageOne;

CREATE OR REPLACE PACKAGE schema2.packageTwo AS   
   PROCEDURE procedureThree (deptno NUMBER);
   FUNCTION functtwo (first number, second number, third number ) RETURN number;
END packageTwo;

CREATE OR REPLACE PACKAGE BODY schema2.packageTwo AS   
   PROCEDURE procedureThree (deptno NUMBER) IS
   BEGIN
      schema1.packageOne.procedureOne( 2 );
   END procedureThree;
   
   FUNCTION functtwo (first number, second number, third number ) RETURN number AS  total     number;
      BEGIN
       total:=first + second + third - 1;
       RETURN total;
     END functtwo;
     
END packageTwo;