

CREATE OR REPLACE PACKAGE          payfexatomikeysh
AS
-- function για το αν υπάρχει ο πίνακας
   FUNCTION exists_table (i_table_name IN VARCHAR2)
      RETURN BOOLEAN;

--
-- function για το καθεστώς
   FUNCTION get_listagg_kathestos (i_cdil_id IN chetdiaelehd.ID%TYPE)
      RETURN VARCHAR2;

-- function για την παρτίδα
   FUNCTION get_listagg_partida (
      i_cdil_id   IN   chetdiaelehd.ID%TYPE,
      i_afm       IN   edetedeaeehd.afm%TYPE
   )
      RETURN VARCHAR2;

--
-- exatomikeysh 2014 - search
-- type record
   TYPE payt_exat_rec IS RECORD (
      kodikos              NUMBER,
      protocolarith        NUMBER,
      ID                   NUMBER (12),
      gusr_id              NUMBER (10),
      recordtype           NUMBER,
      etos                 NUMBER,
      dteprotocol          DATE,
      afm                  VARCHAR2 (9),
      lastname             VARCHAR2 (100),
      firstname            VARCHAR2 (100),
      fathername           VARCHAR2 (100),
      prosopotype          NUMBER,
      lkkoi_id_b2   VARCHAR2 (100),
      lkkoi_kodikos_b2   VARCHAR2 (100),
      idno                 VARCHAR2 (100),
      daa_description      VARCHAR2 (100),
      lkpen_description      VARCHAR2 (100),
      efo_description      VARCHAR2 (100),
      NAME                 VARCHAR2 (100),
      gusr_name            VARCHAR2 (100),
      cdil_id              NUMBER (12),
      pch_id               NUMBER (12),
      tab_pah              VARCHAR2 (3),
      dteid                DATE,
      groupkodikos         VARCHAR2 (100),
      partno               VARCHAR2 (100),
      kekdb                VARCHAR2 (200),
      pmle                 NUMBER,
      epielecheckflag      NUMBER,
      dteepielecheckend    DATE,
      is_aio               NUMBER,
      is_lfa               NUMBER,
      cde_id               NUMBER (12),
      cee_id               NUMBER (12)
   );

-- type table
   TYPE payt_exat_tbl IS TABLE OF payt_exat_rec;

-- function
   FUNCTION get_payt_exat (in_afm IN edetedeaeehd.afm%TYPE)
      RETURN payt_exat_tbl PIPELINED;

--
-- exatomikeysh 2014 - genika stoixeia
-- type record
   TYPE payt_ede_rec IS RECORD (
      wcdil_id                NUMBER (12),
      wid                     NUMBER (12),
      wafm                    VARCHAR2 (9 BYTE),
      wname                   VARCHAR2 (100),
      wlastname               VARCHAR2 (100),
      wfirstname              VARCHAR2 (40),
      wfathername             VARCHAR2 (40),
      wprotocolarith          NUMBER (15),
      wdteprotocol            DATE,
      wdaakodikos             VARCHAR (3),
      wdaadescr               VARCHAR2 (100),
      wefo_id                 NUMBER (12),
      wefo_kodikos            NUMBER,
      wefo_descr              VARCHAR2 (100),
      wlkkoi_id_b2     VARCHAR2 (10),
      wlkkoi_kodikos_b2     VARCHAR2 (10),
      wlkkoi_description_b2   VARCHAR2 (100),
      wetos                   NUMBER,
      wkodikos                NUMBER,
      wprosopotype            NUMBER,
      wdtebirth               DATE,
      widtype                 NUMBER,
      widno                   VARCHAR2 (10),
      wlkpen_kodikos_b1         VARCHAR2 (2),
      wlkpen_description_b1     VARCHAR2 (200),
      wlkkoi_kodikos_b1     VARCHAR2 (10),
      wlkkoi_description_b1   VARCHAR2 (100),
      wodos_b1                VARCHAR2 (60),
      wnumodos_b1             VARCHAR2 (9),
      wtaxkodikos_b1          VARCHAR2 (5),
      wbnk_id                 NUMBER (12),
      wbnk_kodikos            VARCHAR2 (20),
      wbnk_description        VARCHAR2 (200),
      wbns_id                 NUMBER (12),
      wiban                   VARCHAR2 (40),
      wbnk_account            VARCHAR2 (20),
      wama                    VARCHAR2 (20),
      wamoga                  VARCHAR2 (20),
      weapothemaflag          NUMBER,
      wamka                   VARCHAR2 (16),
      weidikaflag             NUMBER (2),
      wdnm_kodikos_a          VARCHAR2 (2),
      wgusr_id                NUMBER (10),
      wrecordtype             NUMBER,
      wdikpraxiflag           NUMBER (1),
      wdtedikpraxi            DATE,
      wpolsymflag             NUMBER (1),
      wdtepolsym              DATE,
      wceeflag                NUMBER (1),
      wceebflag               NUMBER (1),
      wcee6flag               NUMBER (1),
      wceedtestart            DATE,
      wceebdtestart           DATE,
      wcee6dtestart           DATE,
      wceedteend              DATE,
      wceebdteend             DATE,
      wcee6dteend             DATE,
      wdeigma_eniaias         NUMBER (2),
      wdeigma_pol             NUMBER (2),
      wdeigma_eidk            NUMBER (2),
      wdeigma_d               NUMBER (2)
   );

-- type table
   TYPE payt_ede_tbl IS TABLE OF payt_ede_rec;

-- function
   FUNCTION get_payt_ede (
      in_id        IN   edetedeaeehd.ID%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_ede_tbl PIPELINED;

--
-- exatomikeysh 2014 - genika stoixeia - parallhles draseis
-- type record
   TYPE payt_eddh_rec IS RECORD (
      wcdil_id           NUMBER(12),
      wede_id            NUMBER,
      wafm               VARCHAR2 (9 BYTE),
      wdaok_kodikos      VARCHAR2 (3),
      wepd_id            NUMBER,
      wepd_kodikos       VARCHAR2 (20 BYTE),
      wepd_description   VARCHAR2 (250 BYTE),
      warapof            VARCHAR2 (100 BYTE),
      wdteapof           DATE
   );

-- type table
   TYPE payt_eddh_tbl IS TABLE OF payt_eddh_rec;

-- function
   FUNCTION get_payt_eddh (
      in_id        IN   edetedeaeehd.ID%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_eddh_tbl PIPELINED;

--
-- exatomikeysh 2014 - genika stoixeia - epitopios elegxos
   FUNCTION get_chetepiele_dtecheckend (
      in_id        IN   edetedeaeehd.ID%TYPE,
      in_afm       IN   VARCHAR2,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN DATE;

--
-- exatomikeysh 2014 - genika stoixeia - mikra nhsia aigaiou
   FUNCTION get_is_aio (
      i_lkkoi_id   IN   gaee.land_kalkoinotites.lkkoi_id%TYPE
   )
      RETURN NUMBER;

--
-- exatomikeysh 2014 - genika stoixeia - oreinh/meionektikh
   FUNCTION get_is_lfa (
      i_lkkoi_id   IN   gaee.land_kalkoinotites.lkkoi_id%TYPE
   )
      RETURN NUMBER;

--
-- exatomikeysh 2014 - genika stoixeia - deigma eniaias
   FUNCTION get_deigma_eniaias (i_afm IN edetedeaeehd.afm%TYPE)
      RETURN NUMBER;

--
-- exatomikeysh 2014 - genika stoixeia - deigma pol sym
   FUNCTION get_deigma_pol (i_afm IN edetedeaeehd.afm%TYPE)
      RETURN NUMBER;

--
-- exatomikeysh 2014 - genika stoixeia - deigma eidika kathestota
   FUNCTION get_deigma_eidk (i_afm IN edetedeaeehd.afm%TYPE)
      RETURN NUMBER;

--
-- exatomikeysh 2014 - genika stoixeia - deigma deuterobathmio
   FUNCTION get_deigma_d (i_afm IN edetedeaeehd.afm%TYPE)
      RETURN NUMBER;

--
-- exatomikeysh 2014 - genika stoixeia - kekdb
   FUNCTION get_kekdb (
      in_id        IN   edetedeaeehd.ID%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN VARCHAR2;

--
-- exatomikeysh 2014 - genika stoixeia - pososta meioshs logo ekp
   FUNCTION get_pmle (
      in_afm       IN   VARCHAR2,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN NUMBER;

   PROCEDURE get_pol (
      in_id             IN       edetedeaeehd.ID%TYPE,
      in_tab_pah        IN       chetdiaelehd.tab_pah%TYPE,
      out_dtecheckend   OUT      DATE
   );

   PROCEDURE get_exists_chetepiele (
      in_id        IN       edetedeaeehd.ID%TYPE,
      in_afm       IN       VARCHAR2,
      in_tab_pah   IN       chetdiaelehd.tab_pah%TYPE,
      dtecheck     OUT      DATE
   );

   PROCEDURE get_epit (
      in_id               IN       edetedeaeehd.ID%TYPE,
      in_tab_pah          IN       chetdiaelehd.tab_pah%TYPE,
      in_bathmostype               NUMBER,
      in_epiloghtype               NUMBER,
      out_dtecheckstart   OUT      DATE,
      out_dtecheckend     OUT      DATE
   );

--
-- exatomikeysh 2014 - genika stoixeia - stoixeia elegxou (a,b,thlepiskophsh)
   FUNCTION get_epit_dtecheckstart (
      in_id            IN   edetedeaeehd.ID%TYPE,
      in_tab_pah       IN   chetdiaelehd.tab_pah%TYPE,
      in_bathmostype        NUMBER,
      in_epiloghtype        NUMBER
   )
      RETURN DATE;

--
-- exatomikeysh 2014 - genika stoixeia - stoixeia elegxou (a,b,thlepiskophsh)
   FUNCTION get_epit_dtecheckend (
      in_id            IN   edetedeaeehd.ID%TYPE,
      in_tab_pah       IN   chetdiaelehd.tab_pah%TYPE,
      in_bathmostype        NUMBER,
      in_epiloghtype        NUMBER
   )
      RETURN DATE;

--
-- exatomikeysh 2014 - genika stoixeia - pol sym
   FUNCTION get_pol (
      in_id        IN   edetedeaeehd.ID%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN DATE;

--
-- exatomikeysh 2014 - genika stoixeia - dioikit. praxi
   FUNCTION get_dikpraxi (
      in_id        IN   edetedeaeehd.ID%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN DATE;

--
-- exatomikeysh 2014 - eidika kathestota
-- type is record
   TYPE payt_eidika_rec IS RECORD (
      wcdil_id            NUMBER(12),
      wede_id             NUMBER,
      wafm                VARCHAR2 (20 BYTE),
      sitskletiarithdil   NUMBER,
      sitskletiarithele   NUMBER,
      sitskletiarithegk   NUMBER,
      sitskluqty1psdil    NUMBER,
      sitskluqty1psele    NUMBER,
      sitskluqty1psegk    NUMBER,
      bambakiuqty1psdil   NUMBER,
      bambakiuqty1psele   NUMBER,
      bambakiuqty1psegk   NUMBER,
      sporosqty12dil      NUMBER,
      sporosqty12dil_n    NUMBER,
      sporoset12ele       NUMBER,
      sporosqty12ele      NUMBER
   );

-- type table
   TYPE payt_eidika_tbl IS TABLE OF payt_eidika_rec;

-- function
   FUNCTION get_eidika (
      in_id        IN   edetedeaeehd.ID%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_eidika_tbl PIPELINED;

--
-- exatomikeysh 2014 - plhromes - plhromes pou exoun ekdothei
-- type is record
   TYPE payt_plir_ekd_rec IS RECORD (
      wpaf_id           NUMBER (12),
      wpay_id           NUMBER (12),
      wede_id           NUMBER (12),
      wpah_id           NUMBER (12),
      wafm              VARCHAR2 (9),
      wiban             VARCHAR2 (40 BYTE),
      wplhroteovalue    NUMBER,
      wcdil_id          NUMBER (12),
      wpaypa_id         NUMBER (12),
      wpay_partidano    NUMBER,
      wpay_dtepartida   DATE,
      wpay_entolhno     VARCHAR2 (10 BYTE),
      wpay_entolhdate   DATE
   );

-- type table
   TYPE payt_plir_ekd_tbl IS TABLE OF payt_plir_ekd_rec;

-- function
   FUNCTION get_plir_ekd (
      in_cdil_id   IN   chetdiaelehd.ID%TYPE,
      in_afm       IN   edetedeaeehd.afm%TYPE
   )
      RETURN payt_plir_ekd_tbl PIPELINED;

--
-- exatomikeysh 2014 - plhromes - analysh pliromon
-- type is record
   TYPE payt_plir_anal_rec IS RECORD (
      wpal_id             NUMBER (12),
      wpah_id             NUMBER (12),
      wpaf_id             NUMBER (12),
      wlogardescription   VARCHAR2 (100),
      wtelikovalue        NUMBER,
      wdiaforesvalue      NUMBER
   );

-- type table
   TYPE payt_plir_anal_tbl IS TABLE OF payt_plir_anal_rec;

-- function
   FUNCTION get_plir_anal (in_paf_id IN NUMBER)
      RETURN payt_plir_anal_tbl PIPELINED;

--
-- exatomikeysh 2014 - plhromes - analysh diaforopoihshs
-- type is record
   TYPE payt_plir_anal_diaf_rec IS RECORD (
      wpavk_id           NUMBER (16),
      wpah_id            NUMBER (12),
      wafm               VARCHAR2 (9),
      wpartidano         NUMBER,
      wdtepartida        DATE,
      wgroupkodikos      VARCHAR2 (5),
      wpah_description   VARCHAR2 (100),
      wtelikovalue       NUMBER,
      wdiaforesvalue     NUMBER,
      wfdvalue           NUMBER
   );

-- type table
   TYPE payt_plir_anal_diaf_tbl IS TABLE OF payt_plir_anal_diaf_rec;

-- function
   FUNCTION get_plir_anal_diaf (
      in_afm      IN   paytpayafm.afm%TYPE,
      in_pah_id   IN   paytpayhd.ID%TYPE
   )
      RETURN payt_plir_anal_diaf_tbl PIPELINED;

--
-- exatomikeysh 2014 - plhromes - apostolh/epistrofh apo ATE
-- type is record
   TYPE payt_plir_ate_rec IS RECORD (
      wpaybd_id            NUMBER (12),
      wpaf_id              NUMBER (12),
      wpaybh_id            NUMBER (12),
      wpah_id              NUMBER (12),
      wbankdiskinouttype   NUMBER (2),
      wdetlogararith       VARCHAR2 (13),
      wdetiban             VARCHAR2 (27),
      wdetvalue            NUMBER,
      wdetdebitcredit      VARCHAR2 (1),
      wdetlastname         VARCHAR2 (20),
      wdetfirstname        VARCHAR2 (6),
      wdetfathername       VARCHAR2 (4),
      wdetafm              VARCHAR2 (9),
      wdetdtepistosh       DATE,
      wdetreturn           VARCHAR2 (1)
   );

-- type table
   TYPE payt_plir_ate_tbl IS TABLE OF payt_plir_ate_rec;

-- function
   FUNCTION get_plir_ate (in_paf_id IN paytpayafm.ID%TYPE)
      RETURN payt_plir_ate_tbl PIPELINED;

--
-- exatomikeysh 2014 - diastaurotikoi elegxoi
-- type is record
   TYPE payt_diastele_rec IS RECORD (
      wcedec_id          NUMBER (12),
      wafm               VARCHAR2 (9),
      wcec_kodikos       NUMBER,
      wcec_description   VARCHAR2 (255),
      wcdil_id           NUMBER (12),
      wede_id            NUMBER (12)
   );

-- type table
   TYPE payt_diastele_tbl IS TABLE OF payt_diastele_rec;

-- function
   FUNCTION get_diastele (
      in_cdil_id   IN   chetdiaelehd.ID%TYPE,
      in_afm       IN   edetedeaeehd.afm%TYPE
   )
      RETURN payt_diastele_tbl PIPELINED;

--
-- exatomikeysh 2014 - agrotemaxia analytika
-- type is record
   TYPE payt_agroi_rec IS RECORD (
      wid                     NUMBER (12),
      wsynt_sort              NUMBER,
      wcdil_id                NUMBER(12),
      wafm                    VARCHAR2 (9),
      wede_id                 NUMBER,
      wedf_id                 NUMBER,
      wefy_id                 NUMBER,
      wcag_id                 NUMBER,
      wcef_id                 NUMBER,
      wefy_kodikos            VARCHAR2 (5),
      wefy_kodikos_ele        VARCHAR2 (20),
      weda_kodikos            VARCHAR2 (5),
      wsygkalgiaflag          NUMBER,
      wo_pro_100              NUMBER,
      wo                      NUMBER,
      wdenerg                 NUMBER,
      wdentra                 NUMBER,
      wdensity_dil            NUMBER,
      wdikaiomataflag         NUMBER,
      wiiiflag                NUMBER,
      warthro68               NUMBER,
      wtp_flag                NUMBER,
      woc_flag                NUMBER,
      wkanflag                NUMBER,
      weo_a                   NUMBER,
      wed                     NUMBER,
      wpe_g                   NUMBER,
      wpe_sp_g                NUMBER,
      wcode_error             VARCHAR2 (400),
      wpoi_kodikos_dil        VARCHAR2 (20),
      wpoi_kodikos_ele        VARCHAR2 (20),
      wapomeiwsh              NUMBER,
      wmo_100                 NUMBER,
      wps                     NUMBER,
      wiemtype                NUMBER,
      wpk                     NUMBER,
      wd_pro                  NUMBER,
      wee_a                   NUMBER,
      wkl30012                NUMBER,
      wy_pro                  NUMBER,
      wy                      NUMBER,
      wlkkoi_kodikos        VARCHAR2 (10),
      wdteenoikstart          DATE,
      wp_100                  NUMBER,
      wpardra_desc            VARCHAR2 (100),
      wpe                     NUMBER,
      wpe_sp                  NUMBER,
      wdigitized_100          NUMBER,
      wdigit_clr_100          NUMBER,
      wdigitized              NUMBER,
      wdigit_clr              NUMBER,
      wneoxartoypob           VARCHAR2 (20),
      wd_sp_pro_100_pre0      NUMBER,
      wo_pro_100_pre          NUMBER,
      wp_digitized_100        NUMBER,
      wp_digit_clr            NUMBER,
      wsynidioflag            NUMBER,
      we_epilektash           NUMBER,
      we_epilektashkan        NUMBER,
      wde                     NUMBER,
      wd_sp_pro_100           NUMBER,
      wme_sp_pro_100          NUMBER,
      wd_sp                   NUMBER,
      wee_sp                  NUMBER,
      wed_sp_clr              NUMBER,
      wpe_sp_epil_g           NUMBER,
      --s_koufos 25/10/2014
      wcle_checked            NUMBER,
      wpasture_elfactor       NUMBER,
      wd_100_no_pef           NUMBER,
      wd_pro_100              NUMBER,
      wdigit_clr_100_no_pef   NUMBER,
      wsumifs_d_a_pro_100     NUMBER,
      wsumifs_ee_no_dior      NUMBER,
      wee_a_pro_100_no_dior   NUMBER,
      wee_a_pro_100           NUMBER,
      wmpy_cle_checked        NUMBER,
      wy_pro_100              NUMBER,
      wme_a_pro_100_pre       NUMBER,
      wme_pro_100_pre         NUMBER,
      wektash20127_20112      NUMBER,
      wda_pro_100             NUMBER,
      wmy_pro_100             NUMBER,
      wp_mo_pro_100           NUMBER,
      wp_me_a_pro_100         NUMBER,
      wpixel_size_t           NUMBER,
      wed_a_clr_pro_100       NUMBER,
      wed_d_clr_pro_100       NUMBER,
      wee_me_pro_100          NUMBER,
      wd_sp_teliki            NUMBER,
      wed_d_clr               NUMBER,
      wd_aio_teliki           NUMBER,
      wpe_aio                 NUMBER,
      wsumifs_cle_checked     NUMBER,
      wsumifs_eda_kodikos     NUMBER,
      wsumifs_eda_pososto     NUMBER
   );

-- type table
   TYPE payt_agroi_tbl IS TABLE OF payt_agroi_rec;

-- function
   FUNCTION get_agroi (
      in_cdil_id   IN   chetdiaelehd.ID%TYPE,
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_agroi_tbl PIPELINED;

--
-- exatomikeysh 2014 - agrotemaxia sygkentrotika
-- type is record
   TYPE payt_agroi_2_rec IS RECORD (
      wid               NUMBER (12),
      waa               NUMBER,
      wcdil_id          NUMBER (12),
      wefy_kodikos      VARCHAR2 (5),
      weda_kodikos      VARCHAR2 (5),
      wepitopios        NUMBER,
      wepisporhflag     NUMBER,
      wsygkalgiaflag    NUMBER,
      wde               NUMBER,
      wd_sp_teliki      NUMBER,
      wd_teliki         NUMBER,
      wd_aio_teliki     NUMBER,
      wpe_sp            NUMBER,
      wpe               NUMBER,
      wpe_aio           NUMBER,
      wdeek             NUMBER,
      wdeb              NUMBER,
      wdekk             NUMBER,
      wpeek             NUMBER,
      wpeb              NUMBER,
      wpekk             NUMBER,
      wdikaiomataflag   NUMBER,
      wd_c              NUMBER,
      wpe_c             NUMBER,
      wd_tp             NUMBER,
      wpe_tp            NUMBER,
      wd_sb69           NUMBER,
      wpe_sb69          NUMBER,
      wd_dw_oliki       NUMBER,
      wd_dw             NUMBER,
      wpe_dw            NUMBER,
      wd_oc             NUMBER,
      wpe_oc            NUMBER
   );

-- type table
   TYPE payt_agroi_2_tbl IS TABLE OF payt_agroi_2_rec;

-- function
   FUNCTION get_agroi_2 (
      in_cdil_id   IN   chetdiaelehd.ID%TYPE,
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_agroi_2_tbl PIPELINED;

--
-- exatomikeysh 2014 - zoiko kefalaio
-- type is record
   TYPE payt_zoiko_rec IS RECORD (
      wid                      NUMBER (12),
      wcdil_id                 NUMBER (12),
      wede_id                  NUMBER (12),
      wcee_id                  NUMBER (12),
      wcde_id                  NUMBER (12),
      wafm                     VARCHAR2 (9 BYTE),
      wadt                     VARCHAR2 (10 BYTE),
      wlastname                VARCHAR2 (100 BYTE),
      wfirstname               VARCHAR2 (40 BYTE),
      wfathername              VARCHAR2 (40 BYTE),
      wefo_kodikos             VARCHAR2 (5 BYTE),
      wefo_description         VARCHAR2 (100 BYTE),
      wlkpen_kodikos             VARCHAR2 (2 BYTE),
      wlkpen_description         VARCHAR2 (40 BYTE),
      wdaa_kodikos             VARCHAR2 (3 BYTE),
      wdaa_description         VARCHAR2 (100 BYTE),
      wlkkoi_kodikos_b2      VARCHAR2 (10 BYTE),
      wlkkoi_description            VARCHAR2 (100 BYTE),
      wlfa                     NUMBER (1),
      wmetakinoumenosflag      NUMBER (1),
      wafm_systeg              VARCHAR2 (100 BYTE),
      wedz_zoa11               NUMBER (12),
      wedz_zoa12               NUMBER (12),
      wedz_zoa13               NUMBER (12),
      wedz_zoa14               NUMBER (12),
      wczg_zoa11ele            NUMBER (12),
      wczg_zoa12ele            NUMBER (12),
      wczg_zoa13ele            NUMBER (12),
      wczg_zoa14ele            NUMBER (12),
      wczo_zoa11dikel          NUMBER (12),
      wczo_zoa12dikel          NUMBER (12),
      wczo_zoa13dikel          NUMBER (12),
      wczo_zoa14dikel          NUMBER (12),
      wpz_zoa11                NUMBER (12),
      wpz_zoa12                NUMBER (12),
      wpz_zoa13                NUMBER (12),
      wpz_zoa14                NUMBER (12),
      waiges                   NUMBER,
      waiges_dikele_total      NUMBER,
      waiges_epitop_total      NUMBER,
      wkekdbaig                VARCHAR2 (10 BYTE),
      wpoiotikoaig             NUMBER,
      wedz_zoa21               NUMBER (12),
      wedz_zoa22               NUMBER (12),
      wedz_zoa23               NUMBER (12),
      wczg_zoa21ele            NUMBER (12),
      wczg_zoa22ele            NUMBER (12),
      wczg_zoa23ele            NUMBER (12),
      wczo_zoa21dikel          NUMBER (12),
      wczo_zoa22dikel          NUMBER (12),
      wczo_zoa23dikel          NUMBER (12),
      wpz_zoa21                NUMBER (12),
      wpz_zoa22                NUMBER (12),
      wpz_zoa23                NUMBER (12),
      wbooeidh                 NUMBER,
      wbooeidh_dikele_total    NUMBER,
      wbooeidh_epitop_total    NUMBER,
      wkekdbboo                VARCHAR2 (10 BYTE),
      wpoiotikoboo             NUMBER,
      wedz_zoa31               NUMBER (12),
      wedz_zoa32               NUMBER (12),
      wedz_zoa33               NUMBER (12),
      wczg_zoa31ele            NUMBER (12),
      wczg_zoa32ele            NUMBER (12),
      wczg_zoa33ele            NUMBER (12),
      wczo_zoa31dikel          NUMBER (12),
      wczo_zoa32dikel          NUMBER (12),
      wczo_zoa33dikel          NUMBER (12),
      wpz_zoa31                NUMBER (12),
      wpz_zoa32                NUMBER (12),
      wpz_zoa33                NUMBER (12),
      wbooeidh3                NUMBER,
      wbooeidh3_dikele_total   NUMBER,
      wbooeidh3_epitop_total   NUMBER,
      wedz_zoa41               NUMBER (12),
      wedz_zoa42               NUMBER (12),
      wczi_zoa41ele            NUMBER (12),
      wczi_zoa42ele            NUMBER (12),
      wczo_zoa41dikel          NUMBER (12),
      wczo_zoa42dikel          NUMBER (12),
      wpz_zoa41                NUMBER (12),
      wpz_zoa42                NUMBER (12),
      wipoiml4                 NUMBER,
      wipoiml4_dikele_total    NUMBER,
      wipoiml4_epitop_total    NUMBER,
      wedz_zoa51               NUMBER (12),
      wedz_zoa52               NUMBER (12),
      wczi_zoa51ele            NUMBER (12),
      wczi_zoa52ele            NUMBER (12),
      wczo_zoa51dikel          NUMBER (12),
      wczo_zoa52dikel          NUMBER (12),
      wpz_zoa51                NUMBER (12),
      wpz_zoa52                NUMBER (12),
      wxoir5                   NUMBER,
      wxoir5_dikele_total      NUMBER,
      wxoir5_epitop_total      NUMBER,
      wedz_zoa53               NUMBER,
      wedz_zoa61               NUMBER,
      wedz_zoa62               NUMBER,
      wedz_zoa63               NUMBER,
      wedz_zoa64               NUMBER
   );

-- type table
   TYPE payt_zoiko_tbl IS TABLE OF payt_zoiko_rec;

-- function
   FUNCTION get_zoiko (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_zoiko_tbl PIPELINED;

--
-- exatomikeysh 20 - zoiko kefalaio - stavloi
-- type is record
   TYPE payt_stavloi_rec IS RECORD (
      wid                  NUMBER (12),
      wcdil_id             NUMBER(12),
      wede_id              NUMBER (12),
      west_id              NUMBER (12),
      wkodikos             NUMBER (5),
      wneoxartoypob        VARCHAR2 (20),
      wkekdb               VARCHAR2 (500),
      wezo_eid_description     VARCHAR2 (200),
      wezoke_description   VARCHAR2 (512)
   );

-- type table
   TYPE payt_stavloi_tbl IS TABLE OF payt_stavloi_rec;

-- function
   FUNCTION get_stavloi (
      in_id        IN   edetedeaeehd.ID%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_stavloi_tbl PIPELINED;

--
-- exatomikeysh 2014 - ΜΜΖ ΖΩΙΚΟΥ ΚΕΦΑΛΑΙΟΥ KAI ΑΝΗΓΜΕΝΗΣ ΕΚΤΑΣΗΣ ΒΟΣΚΟΤΟΠΩΝ
-- v_margiolomas 23/10/2014 --
-- type is record
   TYPE payt_mmz_rec IS RECORD (
      wcdil_id                     NUMBER(12),
      wafm                         VARCHAR2 (9),
      waa_1                        NUMBER,
      wdescription_1               VARCHAR2 (100),
      wmmz_1                       NUMBER,
      wsdl_bo1                     NUMBER,
      wsdl_bo1_mmz                 NUMBER,
      wdez_bo1                     NUMBER,
      wdez_bo1_mmz                 NUMBER,
      wsee_bo1                     NUMBER,
      wsee_bo1_mmz                 NUMBER,
      wde30703_bo1                 NUMBER,
      wde30703_bo1_mmz             NUMBER,
      wsee_30703_bo1               NUMBER,
      wsee_30703_bo1_mmz           NUMBER,
      waa_2                        NUMBER,
      wdescription_2               VARCHAR2 (100),
      wmmz_2                       NUMBER,
      wsdl_bo2                     NUMBER,
      wsdl_bo2_mmz                 NUMBER,
      wdez_bo2                     NUMBER,
      wdez_bo2_mmz                 NUMBER,
      wsee_bo2                     NUMBER,
      wsee_bo2_mmz                 NUMBER,
      wde30703_bo2                 NUMBER,
      wde30703_bo2_mmz             NUMBER,
      wsee_30703_bo2               NUMBER,
      wsee_30703_bo2_mmz           NUMBER,
      waa_3                        NUMBER,
      wdescription_3               VARCHAR2 (100),
      wmmz_3                       NUMBER,
      wsdl_bo3                     NUMBER,
      wsdl_bo3_mmz                 NUMBER,
      wdez_bo3                     NUMBER,
      wdez_bo3_mmz                 NUMBER,
      wsee_bo3                     NUMBER,
      wsee_bo3_mmz                 NUMBER,
      wde30703_bo3                 NUMBER,
      wde30703_bo3_mmz             NUMBER,
      wsee_30703_bo3               NUMBER,
      wsee_30703_bo3_mmz           NUMBER,
      wmmz_sdl_bo                  NUMBER,
      wmmz_sde_bo                  NUMBER,
      wmmz_see_bo                  NUMBER,
      wmmz_de30703_bo              NUMBER,
      wmmz_see_30703_bo            NUMBER,
      wmin_mmz_sde_see_30703_bo    NUMBER,
      waa_4                        NUMBER,
      wdescription_4               VARCHAR2 (100),
      wmmz_4                       NUMBER,
      wsdl_ap_l1y                  NUMBER,
      wsdl_ap_l1y_mmz              NUMBER,
      wdez_ap_l1y                  NUMBER,
      wdez_ap_l1y_mmz              NUMBER,
      wsee_ap_l1y                  NUMBER,
      wsee_ap_l1y_mmz              NUMBER,
      wde30703_ap_l1y              NUMBER,
      wde30703_ap_l1y_mmz          NUMBER,
      wsee_30703_ap_l1y            NUMBER,
      wsee_30703_ap_l1y_mmz        NUMBER,
      waa_5                        NUMBER,
      wdescription_5               VARCHAR2 (100),
      wmmz_5                       NUMBER,
      wsdl_ag_g1y                  NUMBER,
      wsdl_ag_g1y_mmz              NUMBER,
      wdez_ag_g1y                  NUMBER,
      wdez_ag_g1y_mmz              NUMBER,
      wsee_ag_g1y                  NUMBER,
      wsee_ag_g1y_mmz              NUMBER,
      wde30703_ag_g1y              NUMBER,
      wde30703_ag_g1y_mmz          NUMBER,
      wsee_30703_ag_g1y            NUMBER,
      wsee_30703_ag_g1y_mmz        NUMBER,
      waa_6                        NUMBER,
      wdescription_6               VARCHAR2 (100),
      wmmz_6                       NUMBER,
      wsdl_pr_g1y                  NUMBER,
      wsdl_pr_g1y_mmz              NUMBER,
      wdez_pr_g1y                  NUMBER,
      wdez_pr_g1y_mmz              NUMBER,
      wsee_pr_g1y                  NUMBER,
      wsee_pr_g1y_mmz              NUMBER,
      wde30703_pr_g1y              NUMBER,
      wde30703_pr_g1y_mmz          NUMBER,
      wsee_30703_pr_g1y            NUMBER,
      wsee_30703_pr_g1y_mmz        NUMBER,
      waa_7                        NUMBER,
      wdescription_7               VARCHAR2 (100),
      wmmz_7                       NUMBER,
      wsdl_kt_g1y                  NUMBER,
      wsdl_kt_g1y_mmz              NUMBER,
      wdez_kt_g1y                  NUMBER,
      wdez_kt_g1y_mmz              NUMBER,
      wsee_kt_g1y                  NUMBER,
      wsee_kt_g1y_mmz              NUMBER,
      wde30703_kt_g1y              NUMBER,
      wde30703_kt_g1y_mmz          NUMBER,
      wsee_30703_kt_g1y            NUMBER,
      wsee_30703_kt_g1y_mmz        NUMBER,
      wmmz_sdl_ap                  NUMBER,
      wmmz_sde_ap                  NUMBER,
      wmmz_see_ap                  NUMBER,
      wmmz_de30703_ap              NUMBER,
      wmmz_see_30703_ap            NUMBER,
      wmin_mmz_sde_see_30703_ap    NUMBER,
      waa_8                        VARCHAR2 (100),
      wdescription_8               VARCHAR2 (100),
      wmmz_8                       VARCHAR2 (100),
      wsdl_xi                      NUMBER,
      wsde_xi                      NUMBER,
      wsee_xi                      NUMBER,
      wsee_30703_xi                NUMBER,
      waa_8_ke                     VARCHAR2 (100),
      wdescription_8_ke            VARCHAR2 (100),
      wmmz_8_ke                    VARCHAR2 (100),
      wsdl_xi_ke                   NUMBER,
      waa_8_eb                     NUMBER,
      wdescription_8_eb            VARCHAR2 (100),
      wmmz_8_eb                    NUMBER,
      wsdl_xi_eb                   NUMBER,
      wsdl_xi_eb_mmz               NUMBER,
      wdez_xi_eb                   NUMBER,
      wdez_xi_eb_mmz               NUMBER,
      wsee_xi_eb                   NUMBER,
      wsee_xi_eb_mmz               NUMBER,
      wsee_30703_xi_eb             NUMBER,
      wsee_30703_xi_eb_mmz         NUMBER,
      waa_9                        VARCHAR2 (100),
      wdescription_9               VARCHAR2 (100),
      wmmz_9                       VARCHAR2 (100),
      wsdl_px                      NUMBER,
      wsde_px                      NUMBER,
      wsee_px                      NUMBER,
      wsee_30703_px                NUMBER,
      waa_9_ke                     VARCHAR2 (100),
      wdescription_9_ke            VARCHAR2 (100),
      wmmz_9_ke                    VARCHAR2 (100),
      wsdl_px_ke                   NUMBER,
      waa_9_eb                     NUMBER,
      wdescription_9_eb            VARCHAR2 (100),
      wmmz_9_eb                    NUMBER,
      wsdl_px_eb                   NUMBER,
      wsdl_px_eb_mmz               NUMBER,
      wdez_px_eb                   NUMBER,
      wdez_px_eb_mmz               NUMBER,
      wsee_px_eb                   NUMBER,
      wsee_px_eb_mmz               NUMBER,
      wsee_30703_px_eb             NUMBER,
      wsee_30703_px_eb_mmz         NUMBER,
      wmmz_sdl_x                   NUMBER,
      wmmz_sde_x                   NUMBER,
      wmmz_see_x                   NUMBER,
      wmmz_de30703_x               NUMBER,
      wmmz_see_30703_x             NUMBER,
      wmin_mmz_sde_see_30703_x     NUMBER,
      waa_10                       NUMBER,
      wdescription_10              VARCHAR2 (100),
      wmmz_10                      NUMBER,
      wsdl_ioh                     NUMBER,
      wsdl_ioh_mmz                 NUMBER,
      wdez_ioh                     NUMBER,
      wdez_ioh_mmz                 NUMBER,
      wsee_ioh                     NUMBER,
      wsee_ioh_mmz                 NUMBER,
      wde30703_ioh                 NUMBER,
      wde30703_ioh_mmz             NUMBER,
      wsee_30703_ioh               NUMBER,
      wsee_30703_ioh_mmz           NUMBER,
      wmmz_sdl_i                   NUMBER,
      wmmz_sde_i                   NUMBER,
      wmmz_see_i                   NUMBER,
      wmmz_de30703_i               NUMBER,
      wmmz_see_30703_i             NUMBER,
      wmin_mmz_sde_see_30703_i     NUMBER,
      wsmmz_sdl_bos                NUMBER,
      wsyn_min_mmz_sde_see_30703   NUMBER,
      waeb_pro_en                  NUMBER,
      wcode_error                  VARCHAR2 (400)
   );

-- type table
   TYPE payt_mmz_tbl IS TABLE OF payt_mmz_rec;

-- function
   FUNCTION get_mmz (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_mmz_tbl PIPELINED;

--
-- exatomikeysh 2014 - ΜΜΖ ΖΩΙΚΟΥ ΚΕΦΑΛΑΙΟΥ KAI ΑΝΗΓΜΕΝΗΣ ΕΚΤΑΣΗΣ ΒΟΣΚΟΤΟΠΩΝ
-- v_margiolomas 23/10/2014 --
-- type is record
   TYPE payt_mmz2_rec IS RECORD (
      wid                  NUMBER (12),
      wcdil_id             NUMBER (12),
      wafm                 VARCHAR2 (9),
      --'ΒΟΟΕΙΔΗ < 6 ΜΗΝΩΝ'
      waa_1                NUMBER,
      wdescription_1       CHAR (17 BYTE),
      wmmz_1               NUMBER,
      wdlz_bo1             NUMBER,
      wdl31613_bo1         NUMBER,
      wdez_bo1             NUMBER,
      wde31613_bo1         NUMBER,
      weez_bo1             NUMBER,
      wna1_bo1             NUMBER,
      weg1_bo1             NUMBER,
      wna2_bo1             NUMBER,
      weg2_bo1             NUMBER,
      wde32613_bo1         NUMBER,
      wde30703_bo1         NUMBER,
      wde33613_bo1         NUMBER,
      wde31703_bo1         NUMBER,
      wsfg_bo1             NUMBER,
      wkbz_en_bo1          NUMBER,
      wkb32613_en_bo1      NUMBER,
      wkb30703_en_bo1      NUMBER,
      wkb31613_bo1         NUMBER,
      wkbz_pl_bo1          NUMBER,
      wkb32613_pl_bo1      NUMBER,
      --'ΒΟΟΕΙΔΗ 6-24 ΜΗΝΩΝ'
      waa_2                NUMBER,
      wdescription_2       CHAR (18 BYTE),
      wmmz_2               NUMBER,
      wdlz_bo2             NUMBER,
      wdl31613_bo2         NUMBER,
      wdez_bo2             NUMBER,
      wde31613_bo2         NUMBER,
      weez_bo2             NUMBER,
      wna1_bo2             NUMBER,
      weg1_bo2             NUMBER,
      wna2_bo2             NUMBER,
      weg2_bo2             NUMBER,
      wde32613_bo2         NUMBER,
      wde30703_bo2         NUMBER,
      wde33613_bo2         NUMBER,
      wde31703_bo2         NUMBER,
      wsfg_bo2             NUMBER,
      wkbz_en_bo2          NUMBER,
      wkb32613_en_bo2      NUMBER,
      wkb30703_en_bo2      NUMBER,
      wkb31613_bo2         NUMBER,
      wkbz_pl_bo2          NUMBER,
      wkb32613_pl_bo2      NUMBER,
      --'ΒΟΟΕΙΔΗ > 24 ΜΗΝΩΝ'
      waa_3                NUMBER,
      wdescription_3       CHAR (18 BYTE),
      wmmz_3               NUMBER,
      wdlz_bo3             NUMBER,
      wdl31613_bo3         NUMBER,
      wdez_bo3             NUMBER,
      wde31613_bo3         NUMBER,
      weez_bo3             NUMBER,
      wna1_bo3             NUMBER,
      weg1_bo3             NUMBER,
      wna2_bo3             NUMBER,
      weg2_bo3             NUMBER,
      wde32613_bo3         NUMBER,
      wde30703_bo3         NUMBER,
      wde33613_bo3         NUMBER,
      wde31703_bo3         NUMBER,
      wsfg_bo3             NUMBER,
      wkbz_en_bo3          NUMBER,
      wkb32613_en_bo3      NUMBER,
      wkb30703_en_bo3      NUMBER,
      wkb31613_bo3         NUMBER,
      wkbz_pl_bo3          NUMBER,
      wkb32613_pl_bo3      NUMBER,
      --ΣΥΝΟΛΑ ΒΟΟΕΙΔΩΝ
      wmmz_dlz_bo          NUMBER,
      wmmz_dl31613_bo      NUMBER,
      wmmz_dez_bo          NUMBER,
      wmmz_de31613_bo      NUMBER,
      wmmz_eez_bo          NUMBER,
      wmmz_na1_bo          NUMBER,
      wmmz_eg1_bo          NUMBER,
      wmmz_na2_bo          NUMBER,
      wmmz_eg2_bo          NUMBER,
      wmmz_de32613_bo      NUMBER,
      wmmz_de30703_bo      NUMBER,
      wmmz_de33613_bo      NUMBER,
      wmmz_de31703_bo      NUMBER,
      wmmz_sfg_bo          NUMBER,
      wmmz_sdl_bo          NUMBER,
      wmmz_sde_bo          NUMBER,
      wmmz_en_se_see_bo    NUMBER,
      wmmz_kb_en_bo        NUMBER,
      wmmz_pl_se_see_bo    NUMBER,
      wmmz_kb_pl_bo        NUMBER,
      wmmz_en_bos_see_bo   NUMBER,
      wmmz_pro_en_se_bo    NUMBER,
      wmmz_pro_pl_se_bo    NUMBER,
      wmmz_pro_en_bos_bo   NUMBER,
      wmmz_kbz_en_bo       NUMBER,
      wmmz_kb32613_en_bo   NUMBER,
      wmmz_kb30703_en_bo   NUMBER,
      wmmz_kb31613_bo      NUMBER,
      wmmz_kbz_pl_bo       NUMBER,
      wmmz_kb32613_pl_bo   NUMBER,
      --'ΑΙΓΟΠΡΟΒΑΤΑ < 1 ΕΤΟΥΣ'
      waa_4                NUMBER,
      wdescription_4       CHAR (11 BYTE),
      wmmz_4               NUMBER,
      wdlz_ap              NUMBER,
      wdl31613_ap          NUMBER,
      wdez_ap              NUMBER,
      wde31613_ap          NUMBER,
      weez_ap              NUMBER,
      wna1_ap              NUMBER,
      weg1_ap              NUMBER,
      wna2_ap              NUMBER,
      weg2_ap              NUMBER,
      wde32613_ap          NUMBER,
      wde30703_ap          NUMBER,
      wde33613_ap          NUMBER,
      wde31703_ap          NUMBER,
      wsdl_ap              NUMBER,
      --ΣΥΝΟΛΑ ΑΙΓΟΠΡΟΒΑΤΩΝ
      wmmz_dlz_ap          NUMBER,
      wmmz_dl31613_ap      NUMBER,
      wmmz_dez_ap          NUMBER,
      wmmz_de31613_ap      NUMBER,
      wmmz_eez_ap          NUMBER,
      wmmz_na1_ap          NUMBER,
      wmmz_eg1_ap          NUMBER,
      wmmz_na2_ap          NUMBER,
      wmmz_eg2_ap          NUMBER,
      wmmz_de32613_ap      NUMBER,
      wmmz_de30703_ap      NUMBER,
      wmmz_de33613_ap      NUMBER,
      wmmz_de31703_ap      NUMBER,
      wmmz_sdl_ap          NUMBER,
      wmmz_sde_ap          NUMBER,
      wmmz_en_se_see_ap    NUMBER,
      wmmz_pl_se_see_ap    NUMBER,
      wmmz_en_bos_see_ap   NUMBER,
      wmmz_pro_en_se_ap    NUMBER,
      wmmz_pro_pl_se_ap    NUMBER,
      wmmz_pro_en_bos_ap   NUMBER,
      --'ΧΟΙΡΟΜΗΤΕΡΕΣ ΕΛΕΥΘ. ΒΟΣΚ.'
      waa_5_eb             NUMBER,
      wdescription_5_eb    CHAR (25 BYTE),
      wmmz_5_eb            NUMBER,
      wdlz_xi_eb           NUMBER,
      wdl31613_xi_eb       NUMBER,
      wdez_xi_eb           NUMBER,
      wde31613_xi_eb       NUMBER,
      weez_xi_eb           NUMBER,
      wna1_xi_eb           NUMBER,
      weg1_xi_eb           NUMBER,
      wna2_xi_eb           NUMBER,
      weg2_xi_eb           NUMBER,
      wde32613_xi_eb       NUMBER,
      wde30703_xi_eb       NUMBER,
      wde33613_xi_eb       NUMBER,
      wde31703_xi_eb       NUMBER,
      --'ΠΑΧ/ΝΑ ΧΟΙΡΙΔΙΑ ΕΛΕΥΘ. ΒΟΣΚ.'
      waa_6_eb             NUMBER,
      wdescription_6_eb    CHAR (28 BYTE),
      wmmz_6_eb            NUMBER,
      wdlz_px_eb           NUMBER,
      wdl31613_px_eb       NUMBER,
      wdez_px_eb           NUMBER,
      wde31613_px_eb       NUMBER,
      weez_px_eb           NUMBER,
      wna1_px_eb           NUMBER,
      weg1_px_eb           NUMBER,
      wna2_px_eb           NUMBER,
      weg2_px_eb           NUMBER,
      wde32613_px_eb       NUMBER,
      wde30703_px_eb       NUMBER,
      wde33613_px_eb       NUMBER,
      wde31703_px_eb       NUMBER,
      --ΣΥΝΟΛΑ ΧΟΙΡΙΝΩΝ
      wmmz_dlz_x           NUMBER,
      wmmz_dl31613_x       NUMBER,
      wmmz_dez_x           NUMBER,
      wmmz_de31613_x       NUMBER,
      wmmz_eez_x           NUMBER,
      wmmz_na1_x           NUMBER,
      wmmz_eg1_x           NUMBER,
      wmmz_na2_x           NUMBER,
      wmmz_eg2_x           NUMBER,
      wmmz_de32613_x       NUMBER,
      wmmz_de30703_x       NUMBER,
      wmmz_de33613_x       NUMBER,
      wmmz_de31703_x       NUMBER,
      wmmz_sdl_x           NUMBER,
      wmmz_sde_x           NUMBER,
      wmmz_en_bos_see_x    NUMBER,
      wmmz_pro_en_bos_x    NUMBER,
      --'ΙΠΠΟΙ, ΟΝΟΙ, ΗΜΙΟΝΟΙ'
      waa_7                NUMBER,
      wdescription_7       CHAR (20 BYTE),
      wmmz_7               NUMBER,
      wdlz_ioh             NUMBER,
      wdl31613_ioh         NUMBER,
      wdez_ioh             NUMBER,
      wde31613_ioh         NUMBER,
      weez_ioh             NUMBER,
      wna1_ioh             NUMBER,
      weg1_ioh             NUMBER,
      wna2_ioh             NUMBER,
      weg2_ioh             NUMBER,
      wde32613_ioh         NUMBER,
      wde30703_ioh         NUMBER,
      wde33613_ioh         NUMBER,
      wde31703_ioh         NUMBER,
      --ΣΥΝΟΛΑ ΙΠΠΩΝ
      wmmz_dlz_i           NUMBER,
      wmmz_dl31613_i       NUMBER,
      wmmz_dez_i           NUMBER,
      wmmz_de31613_i       NUMBER,
      wmmz_eez_i           NUMBER,
      wmmz_na1_i           NUMBER,
      wmmz_eg1_i           NUMBER,
      wmmz_na2_i           NUMBER,
      wmmz_eg2_i           NUMBER,
      wmmz_de32613_i       NUMBER,
      wmmz_de30703_i       NUMBER,
      wmmz_de33613_i       NUMBER,
      wmmz_de31703_i       NUMBER,
      wmmz_sdl_i           NUMBER,
      wmmz_sde_i           NUMBER,
      wmmz_en_bos_see_i    NUMBER,
      wmmz_pro_en_bos_i    NUMBER,
      --ΣΥΝΟΛΑ
      wsmmz_sdl_se         NUMBER,
      wsmmz_pro_en_se      NUMBER,
      wsmmz_pro_pl_se      NUMBER,
      --
      wsmmz_sdl_bos        NUMBER,
      waeb_sdl             NUMBER,
      wsmmz_pro_en_bos     NUMBER,
      waeb_pro_en          NUMBER,
      wcode_error          VARCHAR2 (400 BYTE)
   );

-- type is record
   TYPE payt_mmz2_tbl_rec IS RECORD (
      wrow_id            NUMBER,
      wid                NUMBER (12),
      wcdil_id           NUMBER (12),
      wafm               VARCHAR2 (9),
      waa                NUMBER,
      wdescription       VARCHAR2 (100),
      wmmz               NUMBER,
      wdlzzoa            NUMBER,
      wdl31613zoa        NUMBER,
      wdezzoa            NUMBER,
      wde31613zoa        NUMBER,
      weezzoa            NUMBER,
      wna1zoa            NUMBER,
      weg1zoa            NUMBER,
      wna2zoa            NUMBER,
      weg2zoa            NUMBER,
      wde32613zoa        NUMBER,
      wde30703zoa        NUMBER,
      wde33613zoa        NUMBER,
      wde31703zoa        NUMBER,
      wsfgzoa            NUMBER,
      wmmz_sdl           NUMBER,
      wmmz_sde           NUMBER,
      wmmz_en_se_see     NUMBER,
      wmmz_kb_en         NUMBER,
      wmmz_pl_se_see     NUMBER,
      wmmz_kb_pl         NUMBER,
      wmmz_en_bos_see    NUMBER,
      wmmz_pro_en_se     NUMBER,
      wmmz_pro_pl_se     NUMBER,
      wmmz_pro_en_bos    NUMBER,
      wkbz_enzoa         NUMBER,
      wkb32613_enzoa     NUMBER,
      wkb30703_enzoa     NUMBER,
      wkb31613zoa        NUMBER,
      wkbz_plzoa         NUMBER,
      wkb32613_plzoa     NUMBER,
      --ΣΥΝΟΛΑ
      wsmmz_sdl_se       NUMBER,
      wsmmz_pro_en_se    NUMBER,
      wsmmz_pro_pl_se    NUMBER,
      wmmz_sfg_bo        NUMBER,
      --
      wsmmz_sdl_bos      NUMBER,
      waeb_sdl           NUMBER,
      wsmmz_pro_en_bos   NUMBER,
      waeb_pro_en        NUMBER,
      wcode_error        VARCHAR2 (400 BYTE)
   );

-- type table
   TYPE payt_mmz2_tbl IS TABLE OF payt_mmz2_tbl_rec;

-- function
   FUNCTION get_mmz_tbl (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_mmz2_tbl PIPELINED;

--
-- exatomikeysh 2013 - energopoihsh dikaiomaton bash ektashs (1)
-- type is record
   TYPE payt_energopoihsh_1_rec IS RECORD (
      wid           NUMBER (12),
      wcdil_id      NUMBER (12),
      wafm          VARCHAR2 (9),
      wdeek         NUMBER,
      wdeb          NUMBER,
      wdebpro       NUMBER,
      waeb_sdl      NUMBER,
      wdekk         NUMBER,
      wdeek_sum     NUMBER,
      wpeek         NUMBER,
      wpeb          NUMBER,
      waeb_pro_en   NUMBER,
      wpeb_pro      NUMBER,
      wpekk         NUMBER,
      wpeek_sum     NUMBER,
      wsd_sp        NUMBER,
      wspe_sp       NUMBER,
      wmaed         NUMBER,
      wcode_error   VARCHAR2 (400),
      wpeb_t        NUMBER,
      wpeek_t       NUMBER,
      wpekk_t       NUMBER,
      wpeek_t_sum   NUMBER
   );

-- type table
   TYPE payt_energopoihsh_1_tbl IS TABLE OF payt_energopoihsh_1_rec;

-- function
   FUNCTION get_energopoihsh_dik_1 (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_energopoihsh_1_tbl PIPELINED;

--
-- exatomikeysh 20 - energopoihsh dikaiomaton bash ektashs (2)
-- type is record
   TYPE payt_energopoihsh_2_rec IS RECORD (
      wid                    NUMBER (12),
      wcdil_id               NUMBER (12),
      wafm                   VARCHAR2 (9),
      waa                    NUMBER (5),
      wdikaiomatype          NUMBER (2),
      wdikaiomatypedescr     VARCHAR2 (40),
      wpcor_description      VARCHAR2 (100),
      wdikaiomaqty1          NUMBER,
      wdikaiomaunitprice     NUMBER,
      wyears_no_energ        VARCHAR2 (10),
      wdikaiomaenergqty1     NUMBER,
      wdikaiomavalue         NUMBER,
      wdikaiomanoenergqty1   NUMBER,
      wfield8                NUMBER,
      wdikaiomasort          NUMBER
   );

-- type table
   TYPE payt_energopoihsh_2_tbl IS TABLE OF payt_energopoihsh_2_rec;

-- function
   FUNCTION get_energopoihsh_dik_2 (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_energopoihsh_2_tbl PIPELINED;

--
-- exatomikeysh 20 - energopoihsh dikaiomaton bash ektashs (synola)
-- type is record
   TYPE payt_energopoihsh_sums_rec IS RECORD (
      wcdil_id          NUMBER(12),
      wafm              VARCHAR2 (9),
      wdil_dik_pa_sum   NUMBER,
      wdil_dik_ek_sum   NUMBER,
      wdil_dik_ea_sum   NUMBER,
      wene_dik_pa_sum   NUMBER,
      wene_dik_ek_sum   NUMBER,
      wene_dik_ea_sum   NUMBER,
      wdik_dil_sum      NUMBER,
      wdik_ene_sum      NUMBER
   );

-- type table
   TYPE payt_energopoihsh_sums_tbl IS TABLE OF payt_energopoihsh_sums_rec;

-- function
   FUNCTION get_energopoihsh_dik_sums (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_energopoihsh_sums_tbl PIPELINED;

--
-- exatomikeysh 20 - energopoihsh eidikon dikaiomaton (1)
-- type is record
   TYPE payt_energopoihsh_mmz1_rec IS RECORD (
      wcdil_id             NUMBER(12),
      wede_id              NUMBER,
      wafm                 VARCHAR2 (20),
      wmmz1_description    VARCHAR2 (100),
      wmmz1                NUMBER,
      wdlz_bo1             NUMBER,
      wdl31613_bo1         NUMBER,
      wdez_bo1             NUMBER,
      wde31613_bo1         NUMBER,
      weez_bo1             NUMBER,
      wna1_bo1             NUMBER,
      weg1_bo1             NUMBER,
      wna2_bo1             NUMBER,
      weg2_bo1             NUMBER,
      wde32613_bo1         NUMBER,
      wde30703_bo1         NUMBER,
      wde33613_bo1         NUMBER,
      wde31703_bo1         NUMBER,
      wsfg_bo1             NUMBER,
      wkbz_en_bo1          NUMBER,
      wkb32613_en_bo1      NUMBER,
      wkb30703_en_bo1      NUMBER,
      wkb31613_bo1         NUMBER,
      wkbz_pl_bo1          NUMBER,
      wkb32613_pl_bo1      NUMBER,
      wmmz2_description    VARCHAR2 (100),
      wmmz2                NUMBER,
      wdlz_bo2             NUMBER,
      wdl31613_bo2         NUMBER,
      wdez_bo2             NUMBER,
      wde31613_bo2         NUMBER,
      weez_bo2             NUMBER,
      wna1_bo2             NUMBER,
      weg1_bo2             NUMBER,
      wna2_bo2             NUMBER,
      weg2_bo2             NUMBER,
      wde32613_bo2         NUMBER,
      wde30703_bo2         NUMBER,
      wde33613_bo2         NUMBER,
      wde31703_bo2         NUMBER,
      wsfg_bo2             NUMBER,
      wkbz_en_bo2          NUMBER,
      wkb32613_en_bo2      NUMBER,
      wkb30703_en_bo2      NUMBER,
      wkb31613_bo2         NUMBER,
      wkbz_pl_bo2          NUMBER,
      wkb32613_pl_bo2      NUMBER,
      wmmz3_description    VARCHAR2 (100),
      wmmz3                NUMBER,
      wdlz_bo3             NUMBER,
      wdl31613_bo3         NUMBER,
      wdez_bo3             NUMBER,
      wde31613_bo3         NUMBER,
      weez_bo3             NUMBER,
      wna1_bo3             NUMBER,
      weg1_bo3             NUMBER,
      wna2_bo3             NUMBER,
      weg2_bo3             NUMBER,
      wde32613_bo3         NUMBER,
      wde30703_bo3         NUMBER,
      wde33613_bo3         NUMBER,
      wde31703_bo3         NUMBER,
      wsfg_bo3             NUMBER,
      wkbz_en_bo3          NUMBER,
      wkb32613_en_bo3      NUMBER,
      wkb30703_en_bo3      NUMBER,
      wkb31613_bo3         NUMBER,
      wkbz_pl_bo3          NUMBER,
      wkb32613_pl_bo3      NUMBER,
      wmmz_dlz_bo          NUMBER,
      wmmz_dl31613_bo      NUMBER,
      wmmz_dez_bo          NUMBER,
      wmmz_de31613_bo      NUMBER,
      wmmz_eez_bo          NUMBER,
      wmmz_na1_bo          NUMBER,
      wmmz_eg1_bo          NUMBER,
      wmmz_na2_bo          NUMBER,
      wmmz_eg2_bo          NUMBER,
      wmmz_de32613_bo      NUMBER,
      wmmz_de30703_bo      NUMBER,
      wmmz_de33613_bo      NUMBER,
      wmmz_de31703_bo      NUMBER,
      wmmz_sfg_bo          NUMBER,
      wmmz_sdl_bo          NUMBER,
      wmmz_sde_bo          NUMBER,
      wmmz_en_se_see_bo    NUMBER,
      wmmz_kb_en_bo        NUMBER,
      wmmz_pl_se_see_bo    NUMBER,
      wmmz_kb_pl_bo        NUMBER,
      wmmz_en_bos_see_bo   NUMBER,
      wmmz_pro_en_se_bo    NUMBER,
      wmmz_pro_pl_se_bo    NUMBER,
      wmmz_pro_en_bos_bo   NUMBER,
      wmmz_kbz_en_bo       NUMBER,
      wmmz_kb32613_en_bo   NUMBER,
      wmmz_kb30703_en_bo   NUMBER,
      wmmz_kb31613_bo      NUMBER,
      wmmz_kbz_pl_bo       NUMBER,
      wmmz_kb32613_pl_bo   NUMBER,
      wcode_error          VARCHAR2 (200),
      wdteminmmz_en        DATE,
      wmmz4_description    VARCHAR2 (100),
      wmmz4                NUMBER,
      wdlz_ap              NUMBER,
      wdl31613_ap          NUMBER,
      wdez_ap              NUMBER,
      wde31613_ap          NUMBER,
      weez_ap              NUMBER,
      wna1_ap              NUMBER,
      weg1_ap              NUMBER,
      wna2_ap              NUMBER,
      weg2_ap              NUMBER,
      wde32613_ap          NUMBER,
      wde30703_ap          NUMBER,
      wde33613_ap          NUMBER,
      wde31703_ap          NUMBER,
      wsdl_ap              NUMBER,
      wmmz_dlz_ap          NUMBER,
      wmmz_dl31613_ap      NUMBER,
      wmmz_dez_ap          NUMBER,
      wmmz_de31613_ap      NUMBER,
      wmmz_eez_ap          NUMBER,
      wmmz_na1_ap          NUMBER,
      wmmz_eg1_ap          NUMBER,
      wmmz_na2_ap          NUMBER,
      wmmz_eg2_ap          NUMBER,
      wmmz_de32613_ap      NUMBER,
      wmmz_de30703_ap      NUMBER,
      wmmz_de33613_ap      NUMBER,
      wmmz_de31703_ap      NUMBER,
      wmmz_sdl_ap          NUMBER,
      wmmz_sde_ap          NUMBER,
      wmmz_en_se_see_ap    NUMBER,
      wmmz_pl_se_see_ap    NUMBER,
      wmmz_en_bos_see_ap   NUMBER,
      wmmz_pro_en_se_ap    NUMBER,
      wmmz_pro_pl_se_ap    NUMBER,
      wmmz_pro_en_bos_ap   NUMBER,
      wmmzd                NUMBER,
      wmmzpe               NUMBER,
      wmmzp                NUMBER,
      wmmz_energ           NUMBER,
      wmmz_plhr            NUMBER
   );

-- type table
   TYPE payt_energopoihsh_mmz1_tbl IS TABLE OF payt_energopoihsh_mmz1_rec;

-- function
   FUNCTION get_energopoihsh_mmz_1 (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_energopoihsh_mmz1_tbl PIPELINED;

--
-- exatomikeysh 20 - energopoihsh eidikon dikaiomaton (2)
-- type is record
   TYPE payt_energopoihsh_mmz2_rec IS RECORD (
      wcdil_id          NUMBER(12),
      wafm              VARCHAR2 (9),
      waa               NUMBER (5),
      wsfagh            NUMBER,
      wsmmz_en_se       NUMBER,
      wsmmz_pl_se       NUMBER,
      wkatatagaxia      NUMBER,
      welaxmmzenerg     NUMBER,
      weidos_kodikos    NUMBER,
      wyears_no_energ   NUMBER,
      wenerg            NUMBER,
      wplhr             NUMBER,
      wenergmmz_avail   NUMBER,
      wenergmmz_req     NUMBER,
      wplhrmmz_avail    NUMBER,
      wplhrmmz_req      NUMBER,
      wplhr_axia        NUMBER
   );

-- type table
   TYPE payt_energopoihsh_mmz2_tbl IS TABLE OF payt_energopoihsh_mmz2_rec;

-- function
   FUNCTION get_energopoihsh_mmz_2 (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_energopoihsh_mmz2_tbl PIPELINED;

--
-- exatomikeysh 20 - ypologismos plhromis (1)
-- type is record
   TYPE payt_pliromes_1_rec IS RECORD (
      wid                NUMBER (12),
      wcdil_id           NUMBER (12),
      wafm               VARCHAR2 (9),
      wede_id            NUMBER,
      wsd_sp             NUMBER,
      wspe_sp_tel        NUMBER,
      wey_sp             NUMBER,
      wpy_sp             NUMBER,
      waelk_sp           NUMBER,
      wpemk_sp           NUMBER,
      wmaed              NUMBER,
      wplp1_sp           NUMBER,
      wplp_sp            NUMBER,
      wplp2_sp           NUMBER,
      wplp4_sp           NUMBER,
      wsd_dw_pre         NUMBER,
      wspe_dw_pre        NUMBER,
      wsd_dw             NUMBER,
      wspe_pososto       NUMBER,
      wmpy               NUMBER,
      wpmle              NUMBER,
      wspe_sp            NUMBER,
      wspe_dw            NUMBER,
      wspe_sb            NUMBER,
      wspe_c             NUMBER,
      wspe_oc            NUMBER,
      wey_sp_pro         NUMBER,
      wey_dw_pro         NUMBER,
      wey_sb_pro         NUMBER,
      wey_c_pro          NUMBER,
      wey_oc_pro         NUMBER,
      wspe_dw_tel        NUMBER,
      wey_dw             NUMBER,
      wpy_dw             NUMBER,
      waelk_dw           NUMBER,
      wpemk_dw           NUMBER,
      wplp1_dw           NUMBER,
      wplp_dw            NUMBER,
      wplp2_dw           NUMBER,
      wplp4_dw           NUMBER,
      wtmm_dw            NUMBER,
      wsd_sb             NUMBER,
      wspe_sb_tel        NUMBER,
      wey_sb             NUMBER,
      wpy_sb             NUMBER,
      waelk_sb           NUMBER,
      wpemk_sb           NUMBER,
      wplp1_sb           NUMBER,
      wplp_sb            NUMBER,
      wplp2_sb           NUMBER,
      wplp4_sb           NUMBER,
      wtmm_sb            NUMBER,
      wsd_c              NUMBER,
      wspe_c_tel         NUMBER,
      wey_c              NUMBER,
      wpy_c              NUMBER,
      waelk_c            NUMBER,
      wpemk_c            NUMBER,
      wplp1_c            NUMBER,
      wplp_c             NUMBER,
      wplp2_c            NUMBER,
      wplp4_c            NUMBER,
      wtmm1_c            NUMBER,
      wsd_c10            NUMBER,
      wspe_c10_tel       NUMBER,
      wey_c10            NUMBER,
      wpy_c10            NUMBER,
      waelk_c10          NUMBER,
      wpemk_c10          NUMBER,
      wplp1_c10          NUMBER,
      wplp_c10           NUMBER,
      wplp2_c10          NUMBER,
      wplp4_c10          NUMBER,
      wtmm_c10           NUMBER,
      wsd_oc             NUMBER,
      wspe_oc_tel        NUMBER,
      wey_oc             NUMBER,
      wpy_oc             NUMBER,
      waelk_oc           NUMBER,
      wpemk_oc           NUMBER,
      wplp1_oc           NUMBER,
      wplp_oc            NUMBER,
      wplp2_oc           NUMBER,
      wplp4_oc           NUMBER,
      wtmm_oc            NUMBER,
      wsd_aio            NUMBER,
      wey_aio            NUMBER,
      wpy_aio            NUMBER,
      waelk_aio          NUMBER,
      wpemk_aio          NUMBER,
      wplp1_aio          NUMBER,
      wplp_aio           NUMBER,
      wplp2_aio          NUMBER,
      wtmm_aio           NUMBER,
      wcode_error        VARCHAR2 (400),
      wdik_ys            NUMBER,
      waxiadiksthrixi    NUMBER,
      wspe_sp_tel_ys     NUMBER,
      wenerg_eid         NUMBER,
      wenerg_ekt         NUMBER,
      wenerg_ys          NUMBER,
      wplp0_ys           NUMBER,
      wplp1_ys           NUMBER,
      wplp2_ys           NUMBER,
      wplp4_ys           NUMBER,
      wplp5_ys           NUMBER,
      waa_sp             NUMBER,
      waa_dw             NUMBER,
      waa_sb             NUMBER,
      waa_c              NUMBER,
      waa_c10            NUMBER,
      waa_oc             NUMBER,
      waa_aio            NUMBER,
      wdescription_sp    VARCHAR2 (50),
      wdescription_dw    VARCHAR2 (50),
      wdescription_sb    VARCHAR2 (50),
      wdescription_c     VARCHAR2 (50),
      wdescription_c10   VARCHAR2 (50),
      wdescription_oc    VARCHAR2 (50),
      wdescription_aio   VARCHAR2 (50),
      wsynt_sp           VARCHAR2 (5),
      wsynt_dw           VARCHAR2 (5),
      wsynt_sb           VARCHAR2 (5),
      wsynt_c            VARCHAR2 (5),
      wsynt_c10          VARCHAR2 (5),
      wsynt_oc           VARCHAR2 (5),
      wsynt_aio          VARCHAR2 (5),
      wplp1_sum_all      NUMBER,
      wplp2_sum_all      NUMBER,
      wplp_sum_all       NUMBER,
      waa_ys             NUMBER,
      wdescription_ys    VARCHAR2 (50),
      wsynt_ys           VARCHAR2 (5)
   );

-- type table
   TYPE payt_pliromes_1_tbl IS TABLE OF payt_pliromes_1_rec;

-- function
   FUNCTION get_pliromes_1 (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_pliromes_1_tbl PIPELINED;

--
-- exatomikeysh 20 - ypologismos plhromis (1)
-- type is record
   TYPE payt_pliromes_1_dtl_rec IS RECORD (
      wid            NUMBER (12),
      wcdil_id       NUMBER (12),
      wafm           VARCHAR2 (9),
      wede_id        NUMBER,
      waa            NUMBER,
      wdescription   VARCHAR2 (50),
      wsynt          VARCHAR2 (5),
      wsd_pre        NUMBER,
      wsde_pre       NUMBER,
      wsd            NUMBER,
      wspe           NUMBER,
      wey_pro        NUMBER,
      wspe_tel       NUMBER,
      wey            NUMBER,
      wpy            NUMBER,
      waelk          NUMBER,
      wsda           NUMBER,
      wpemk          NUMBER,
      wtmm           NUMBER,
      wplp1          NUMBER,
      wplp2          NUMBER,
      wplp           NUMBER,
      wcode_error    VARCHAR2 (400)
   );

-- type table
   TYPE payt_pliromes_1_2_tbl IS TABLE OF payt_pliromes_1_dtl_rec;

-- function
   FUNCTION get_pliromes_1_table (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_pliromes_1_2_tbl PIPELINED;

--
-- exatomikeysh 20 - ypologismos plhromis (3)
-- type is record
   TYPE payt_pliromes_1_dtl2_rec IS RECORD (
      wid               NUMBER (12),
      wcdil_id          NUMBER (12),
      wafm              VARCHAR2 (9),
      wede_id           NUMBER,
      waa               NUMBER,
      wdescription      VARCHAR2 (50),
      wsynt             VARCHAR2 (5),
      wdik_ys           NUMBER,
      waxiadiksthrixi   NUMBER,
      wspe_sp_tel_ys    NUMBER,
      wenerg_ys         NUMBER,
      wplp0_ys          NUMBER,
      wplp1_ys          NUMBER,
      wplp2_ys          NUMBER,
      wplp5_ys          NUMBER
   );

-- type table
   TYPE payt_pliromes_1_2_tbl2 IS TABLE OF payt_pliromes_1_dtl2_rec;

-- function
   FUNCTION get_pliromes_1_table2 (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_pliromes_1_2_tbl2 PIPELINED;

--
-- exatomikeysh 20 - ypologismos plhromis (2)
-- type is record
   TYPE payt_pliromes_2_rec IS RECORD (
      wcdil_id             NUMBER(12),
      w_ede_id             NUMBER,
      w_afm                VARCHAR2 (20),
      w_totaldik           NUMBER,
      w_totalaxia          NUMBER,
      w_totalmmz           NUMBER,
      w_smmz_sdl_se        NUMBER,
      w_smmz_pro_en_se     NUMBER,
      w_smmz_pro_pl_se     NUMBER,
      wmmz_sfg_bo          NUMBER,
      w_totalmmzenergdik   NUMBER,
      w_totalmmzplirdik    NUMBER,
      w_plp1_se            NUMBER,
      w_plp2_se            NUMBER,
      w_plp4_se            NUMBER,
      w_plp5_se            NUMBER,
      waa_plir2            NUMBER,
      wdescription_plir2   VARCHAR2 (50),
      wsynt_plir2          VARCHAR2 (5)
   );

-- type table
   TYPE payt_pliromes_2_tbl IS TABLE OF payt_pliromes_2_rec;

-- function
   FUNCTION get_pliromes_2 (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_pliromes_2_tbl PIPELINED;

--
-- exatomikeysh 20 - Arthro 68 - M3 - Ypologismos plhromhs
-- type is record
   TYPE payt_a68m3afm_rec IS RECORD (
      wafm               VARCHAR2 (9),
      wekm_kodikos       VARCHAR2 (15),
      wekm_kodikosv      VARCHAR2 (1),
      wlpk_kodikos       VARCHAR2 (5),
      wlpk_kodikosv      VARCHAR2 (200),
      wis_lfa_b2         NUMBER (1),
      wis_lfa1_b2        NUMBER (1),
      wis_lfa2_b2        NUMBER (1),
      wmun_b2v_flag      VARCHAR2 (5),
      wis_lfa_ms         NUMBER (1),
      wis_lfa1_ms        NUMBER (1),
      wis_lfa2_ms        NUMBER (1),
      wmune_b2v_flag     VARCHAR2 (5),
      wepitopios         NUMBER,
      wepitopiosv        VARCHAR2 (5),
      wsfyles_flag       NUMBER,
      wsfylesv_flag      VARCHAR2 (5),
      welogak_flag       NUMBER,
      welogakv_flag      VARCHAR2 (5),
      wma_p_flag         NUMBER,
      wma_pv_flag        VARCHAR2 (1),
      wma_s_flag         NUMBER,
      wma_sv_flag        VARCHAR2 (1),
      wdl_ma_p_ait       NUMBER,
      wdl_ma_s_ait       NUMBER,
      wdl_bo_ayt_p       NUMBER,
      wdl_ubo_6_24_ayt   NUMBER,
      wdl_ubo_6_24_kau   NUMBER,
      wdl_bo_ayt_s       NUMBER,
      wdl_ma_bel         NUMBER,
      wdl_bop_sum        NUMBER,
      wdl_bos_sum        NUMBER,
      d15                NUMBER,
      d16                NUMBER,
      d17                NUMBER,
      wsdl_bo_mtk        NUMBER,
      e18                NUMBER,
      wde_bo_ads_prs     NUMBER,
      wde_bo_ads_apo     NUMBER,
      wde_bo_ads         NUMBER,
      wde_boper_ads      NUMBER,
      e20                NUMBER,
      e21                NUMBER,
      e22                NUMBER,
      e23                NUMBER,
      d25                NUMBER,
      e25                NUMBER,
      d26                NUMBER,
      e26                NUMBER,
      d27                NUMBER,
      e27                NUMBER,
      d28                NUMBER,
      e28                NUMBER,
      d29                NUMBER,
      e29                NUMBER,
      d30                NUMBER,
      e30                NUMBER,
      d31                NUMBER,
      e31                NUMBER,
      d32                NUMBER,
      e32                NUMBER,
      wtm_map            NUMBER,
      wmy_map            NUMBER,
      wpy_map            NUMBER,
      wpm_map            NUMBER,
      wplp0_map          NUMBER,
      wplp1_map          NUMBER,
      wplp2_map          NUMBER,
      wplp5_map          NUMBER,
      wtm_mas            NUMBER,
      wmy_mas            NUMBER,
      wpy_mas            NUMBER,
      wpm_mas            NUMBER,
      wplp0_mas          NUMBER,
      wplp1_mas          NUMBER,
      wplp2_mas          NUMBER,
      wplp5_mas          NUMBER,
      posokyr_p          NUMBER,
      posokyr_s          NUMBER,
      wdte_mtk_bo_prs    DATE,
      wdte_mtk_bo_apo    DATE,
      wdays_mtk          NUMBER
   );

-- type table
   TYPE payt_a68m3afm_tbl IS TABLE OF payt_a68m3afm_rec;

-- function
   FUNCTION get_a68m3afm (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_a68m3afm_tbl PIPELINED;

--
-- exatomikeysh 20 - Arthro 68 - M3 - Mosxides anaparagoghs
-- exatomikeysh 20 - Arthro 68 - M4 - Thilazouses Agelades
-- type is record
   TYPE payt_a68m3mosx_rec IS RECORD (
      wafm                     VARCHAR2 (9),
      wekm_kodikos             VARCHAR2 (15),
      wboo_arithmos_shmansis   VARCHAR2 (20),
      whmeromhnia_genishs      VARCHAR2 (11),
      wage                     VARCHAR2 (10),
      wlf1_kodikos             VARCHAR2 (11),
      wprod_dest               VARCHAR2 (5),
      wmother_lf1_kodikos      VARCHAR2 (11),
      wfather_lf1_kodikos      VARCHAR2 (11),
      wlpk_kodikos             VARCHAR2 (5),
      wk080_1dkkf_flag         NUMBER (1),
      wk080_1dkkf_vflag        VARCHAR2 (5),
      wkbd_eligible_flag_p1    NUMBER (1),
      wk60_eligible_flag       NUMBER (1),
      wkbd_eligiblev_flag_p1   VARCHAR2 (5),
      wk070_mkkf_flag          NUMBER (1),
      wk070_mkkfv_flag         VARCHAR2 (5),
      m13                      VARCHAR2 (5),
      mv13                     VARCHAR2 (5),
      n13                      VARCHAR2 (5),
      nv13                     VARCHAR2 (5),
      o13                      NUMBER (1),
      p13                      NUMBER (1),
      q13                      VARCHAR2 (5),
      qv13                     VARCHAR2 (5),
      r13                      VARCHAR2 (5),
      rv13                     VARCHAR2 (5),
      wcode_error              VARCHAR2 (400)
   );

-- type table
   TYPE payt_a68m3mosx_tbl IS TABLE OF payt_a68m3mosx_rec;

-- function
   FUNCTION get_a68m3mosx (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_a68m3mosx_tbl PIPELINED;

--
-- exatomikeysh 20 - Arthro 68 - M3 - Mosxides anaparagoghs (sums)
-- exatomikeysh 20 - Arthro 68 - M4 - Thilazouses Agelades (sums)
-- type is record
   TYPE payt_a68m3mosx_sums_rec IS RECORD (
      wafm           VARCHAR2 (9),
      wcount_boo     NUMBER,
      wsum_elig      NUMBER,
      wsum_elig_th   NUMBER,
      wsum_wk070     NUMBER,
      wsum_m13       NUMBER,
      wsum_m13_th    NUMBER,
      wsum_n13       NUMBER,
      wsum_n13_th    NUMBER,
      wsum_q13       NUMBER,
      wsum_q13_th    NUMBER,
      wsum_r13       NUMBER,
      wsum_r13_th    NUMBER
   );

-- type table
   TYPE payt_a68m3mosx_sums_tbl IS TABLE OF payt_a68m3mosx_sums_rec;

-- function
   FUNCTION get_a68m3mosx_sums (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_a68m3mosx_sums_tbl PIPELINED;

--
-- exatomikeysh 20 - Arthro 68 - M4 - Ypologismos plhromhs
-- type is record
   TYPE payt_a68m4afm_rec IS RECORD (
      wafm                VARCHAR2 (9),
      wekm_kodikos        VARCHAR2 (15),
      wekm_kodikosv       VARCHAR2 (1),
      wlpk_kodikos        VARCHAR2 (5),
      wlpk_kodikosv       VARCHAR2 (200),
      wis_lfa_b2          NUMBER (1),
      wis_lfa1_b2         NUMBER (1),
      wis_lfa2_b2         NUMBER (1),
      wmun_b2v_flag       VARCHAR2 (5),
      wis_lfa_ms          NUMBER (1),
      wis_lfa1_ms         NUMBER (1),
      wis_lfa2_ms         NUMBER (1),
      wmune_b2v_flag      VARCHAR2 (5),
      wepitopios          NUMBER,
      wepitopiosv         VARCHAR2 (5),
      wsfyles_flag        NUMBER,
      wsfylesv_flag       VARCHAR2 (5),
      welogak_flag        NUMBER,
      welogakv_flag       VARCHAR2 (5),
      wua_p_flag          NUMBER,
      wua_pv_flag         VARCHAR2 (1),
      wua_s_flag          NUMBER,
      wua_sv_flag         VARCHAR2 (1),
      wdl_ua_p_ait        NUMBER,
      wdl_ua_s_ait        NUMBER,
      wdl_bo_ayt_p        NUMBER,
      wdl_ubo_6_24_ayt    NUMBER,
      wdl_ubo_6_24_kau    NUMBER,
      wdl_bo_ayt_s        NUMBER,
      wdl_ma_bel          NUMBER,
      wsdl_ubo_gt24_ayt   NUMBER,
      wsdl_ubo_gt24_kau   NUMBER,
      wsdl_ubo_p_gt24     NUMBER,
      wsdl_ubo_s_gt24     NUMBER,
      wsdl_ubo_gt24_bel   NUMBER,
      wsdl_ubo_gt24_loi   NUMBER,
      wsdl_ubobl_p_gt24   NUMBER,
      wsdl_ubobl_s_gt24   NUMBER,
      wdl_bop_sum         NUMBER,
      wdl_bos_sum         NUMBER,
      d15                 NUMBER,
      d16                 NUMBER,
      d17                 NUMBER,
      wsdl_bo_mtk         NUMBER,
      e18                 NUMBER,
      wde_bo_ads_prs      NUMBER,
      wde_bo_ads_apo      NUMBER,
      wde_bo_ads          NUMBER,
      wde_boper_ads       NUMBER,
      e20                 NUMBER,
      e21                 NUMBER,
      e22                 NUMBER,
      e23                 NUMBER,
      d25                 NUMBER,
      e25                 NUMBER,
      d26                 NUMBER,
      e26                 NUMBER,
      d27                 NUMBER,
      e27                 NUMBER,
      d28                 NUMBER,
      e28                 NUMBER,
      d29                 NUMBER,
      e29                 NUMBER,
      d30                 NUMBER,
      e30                 NUMBER,
      d31                 NUMBER,
      e31                 NUMBER,
      d32                 NUMBER,
      e32                 NUMBER,
      wtm_map             NUMBER,
      wmy_map             NUMBER,
      wpy_map             NUMBER,
      wpm_map             NUMBER,
      wplp0_map           NUMBER,
      wplp1_map           NUMBER,
      wplp2_map           NUMBER,
      wplp5_map           NUMBER,
      wtm_mas             NUMBER,
      wmy_mas             NUMBER,
      wpy_mas             NUMBER,
      wpm_mas             NUMBER,
      wplp0_mas           NUMBER,
      wplp1_mas           NUMBER,
      wplp2_mas           NUMBER,
      wplp5_mas           NUMBER,
      posokyr_p           NUMBER,
      posokyr_s           NUMBER,
      wdte_mtk_bo_prs     DATE,
      wdte_mtk_bo_apo     DATE,
      wdays_mtk           NUMBER
   );

-- type table
   TYPE payt_a68m4afm_tbl IS TABLE OF payt_a68m4afm_rec;

-- function
   FUNCTION get_a68m4afm (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_a68m4afm_tbl PIPELINED;

--
-- exatomikeysh 20 - Arthro 68 - M5 - Ypologismos plhromis
-- type is record
   TYPE payt_a68m5afm_rec IS RECORD (
      wafm                     VARCHAR2 (9),
      ekm_kodikos              VARCHAR2 (4000),
      wap1p_flag               VARCHAR2 (1),
      wap1s_flag               VARCHAR2 (1),
      wap12p_flag              VARCHAR2 (1),
      ap1p_flag                NUMBER,
      wepitopios               VARCHAR2 (1),
      epitopios                NUMBER,
      wprosthethaigflag        VARCHAR2 (1),
      wsymplprosthethaigflag   VARCHAR2 (1),
      wprosthethmetaigflag     VARCHAR2 (1),
      wdl_ap1p_ait             NUMBER,
      wdl_ap1s_ait             NUMBER,
      wdl_ap2p_ait             NUMBER,
      wsdl_ag_st1              NUMBER,
      wsdl_pr_st1              NUMBER,
      sum_st1                  NUMBER,
      wsdl_ag_gt1              NUMBER,
      wsdl_pr_gt1              NUMBER,
      sum_gt1                  NUMBER,
      sum_stgt1                NUMBER,
      wsdl_ag_st2              NUMBER,
      wsdl_pr_st2              NUMBER,
      sum_st2                  NUMBER,
      wsdl_ag_gt2              NUMBER,
      wsdl_pr_gt2              NUMBER,
      sum_gt2                  NUMBER,
      sum_stgt2                NUMBER,
      wsdl_ag_st3              NUMBER,
      wsdl_pr_st3              NUMBER,
      sum_st3                  NUMBER,
      wsdl_ag_gt3              NUMBER,
      wsdl_pr_gt3              NUMBER,
      sum_gt3                  NUMBER,
      sum_stgt3                NUMBER,
      wsyn_rap                 NUMBER,
      wde_ap_ads_prs           NUMBER,
      wde_ap_ads_apo           NUMBER,
      wde_ap_ads_90            NUMBER,
      wde_ap_ads_90m           NUMBER,
      wde_ag_gt1_apg_p         NUMBER,
      wde_ag_gt1_apg_p2        NUMBER,
      wde_ag_gt1_apg_p3        NUMBER,
      wde_pr_gt1_apg_p         NUMBER,
      wde_pr_gt1_apg_p2        NUMBER,
      wde_pr_gt1_apg_p3        NUMBER,
      wde_agpr_gt1_apg_p       NUMBER,
      wde_agpr_gt1_apg_p2      NUMBER,
      wde_agpr_gt1_apg_p3      NUMBER,
      wde_ag_gt1_mtr_p         NUMBER,
      wde_ag_gt1_mtr_p2        NUMBER,
      wde_ag_gt1_mtr_p3        NUMBER,
      wde_pr_gt1_mtr_p         NUMBER,
      wde_pr_gt1_mtr_p2        NUMBER,
      wde_pr_gt1_mtr_p3        NUMBER,
      wde_agpr_gt1_mtr_p       NUMBER,
      wde_agpr_gt1_mtr_p2      NUMBER,
      wde_agpr_gt1_mtr_p3      NUMBER,
      wde_ag_st1_mtr_p         NUMBER,
      wde_ag_st1_mtr_p2        NUMBER,
      wde_ag_st1_mtr_p3        NUMBER,
      wde_pr_st1_mtr_p         NUMBER,
      wde_pr_st1_mtr_p2        NUMBER,
      wde_pr_st1_mtr_p3        NUMBER,
      wde_agpr_st1_mtr_p       NUMBER,
      wde_agpr_st1_mtr_p2      NUMBER,
      wde_agpr_st1_mtr_p3      NUMBER,
      wde_agpr_gt1_30703       NUMBER,
      wde_agpr_gt1_307032      NUMBER,
      wde_agpr_gt1_307033      NUMBER,
      wde_agpr_gt1_31613       NUMBER,
      wde_agpr_gt1_316132      NUMBER,
      wde_agpr_gt1_316133      NUMBER,
      wde_ag_s                 NUMBER,
      wde_pr_s                 NUMBER,
      wde_agpr_s               NUMBER,
      wee_ap_st1_ekmnap        NUMBER,
      --wee_ap_st1_ekm           NUMBER,
      wee_ap_st1_ekmnap2       NUMBER,
      wee_ap_st1_ekmnap3       NUMBER,
      wee_ap_gt1_ekmnap        NUMBER,
      wee_ap_gt1_ekmnap2       NUMBER,
      wee_ap_gt1_ekmnap3       NUMBER,
      wee_ap_st1gt1_ekm1       NUMBER,
      wee_ap_gt1_ekmnap_s2     NUMBER,
      wee_ap_st1gt1_ekm3       NUMBER,
      wmy_ap1p                 NUMBER,
      wmy_ap1s                 NUMBER,
      wmy_ap2p                 NUMBER,
      wpy_ap1p                 NUMBER,
      wpy_ap1s                 NUMBER,
      wpy_ap2p                 NUMBER,
      wpm_ap1p                 NUMBER,
      wpm_ap1s                 NUMBER,
      wpm_ap2p                 NUMBER,
      wtm_ap1p                 NUMBER,
      wtm_ap1s                 NUMBER,
      wtm_ap2p                 NUMBER,
      wplp0_ap1p               NUMBER,
      wplp0_ap1s               NUMBER,
      wplp0_ap2p               NUMBER,
      wplp1_ap1p               NUMBER,
      wplp1_ap1s               NUMBER,
      wplp1_ap2p               NUMBER,
      wplp2_ap1p               NUMBER,
      wplp2_ap1s               NUMBER,
      wplp2_ap2p               NUMBER,
      wplp5_ap1p               NUMBER,
      wplp5_ap1s               NUMBER,
      wplp5_ap2p               NUMBER,
      wprep_aigp1              NUMBER,
      wprep_aigp2              NUMBER,
      wprep_aigp3              NUMBER,
      wprep_aigk1              NUMBER,
      wprep_aigk2              NUMBER,
      wprep_aigk3              NUMBER,
      weepr_ap1p               NUMBER,
      weepr_ap1p2              NUMBER,
      weepr_ap1s               NUMBER,
      weepr_ap1s2              NUMBER,
      weepr_ap2p               NUMBER,
      weepr_ap2p2              NUMBER,
      wprlast_ap1p             NUMBER,
      wprlast_ap1p2            NUMBER,
      wprlast_ap1p3            NUMBER,
      wprlast_ap1s             NUMBER,
      wprlast_ap1s2            NUMBER,
      wprlast_ap1s3            NUMBER,
      wprlast_ap2p             NUMBER,
      wprlast_ap2p2            NUMBER,
      wprlast_ap2p3            NUMBER,
      ap1s_flag                NUMBER,
      wdl_uag_sf               NUMBER,
      wdtecheckend             DATE,
      wdl_uag_st1              NUMBER,
      e35b                     NUMBER,
      wdte_mtk_ap_prs          DATE,
      wdte_mtk_ap_apo          DATE,
      wdays_mtk                NUMBER,
      wp_ap_mtk                NUMBER
   );

-- type table
   TYPE payt_a68m5afm_tbl IS TABLE OF payt_a68m5afm_rec;

   TYPE payt_a68m5afm_rec2 IS RECORD (
      wafm                     VARCHAR2 (9),
      ekm_kodikos              VARCHAR2 (4000),
      wap1p_flag               VARCHAR2 (1),
      wap1s_flag               VARCHAR2 (1),
      wap12p_flag              VARCHAR2 (1),
      ap1p_flag                NUMBER,
      wepitopios               VARCHAR2 (1),
      epitopios                NUMBER,
      wprosthethaigflag        VARCHAR2 (1),
      wsymplprosthethaigflag   VARCHAR2 (1),
      wprosthethmetaigflag     VARCHAR2 (1),
      wdl_ap1p_ait             NUMBER,
      wdl_ap1s_ait             NUMBER,
      wdl_ap2p_ait             NUMBER,
      wsdl_ag_st1              NUMBER,
      wsdl_pr_st1              NUMBER,
      sum_st1                  NUMBER,
      wsdl_ag_gt1              NUMBER,
      wsdl_pr_gt1              NUMBER,
      sum_gt1                  NUMBER,
      sum_stgt1                NUMBER,
      wsdl_ag_st2              NUMBER,
      wsdl_pr_st2              NUMBER,
      sum_st2                  NUMBER,
      wsdl_ag_gt2              NUMBER,
      wsdl_pr_gt2              NUMBER,
      sum_gt2                  NUMBER,
      sum_stgt2                NUMBER,
      wsdl_ag_st3              NUMBER,
      wsdl_pr_st3              NUMBER,
      sum_st3                  NUMBER,
      wsdl_ag_gt3              NUMBER,
      wsdl_pr_gt3              NUMBER,
      sum_gt3                  NUMBER,
      sum_stgt3                NUMBER,
      wsyn_rap                 NUMBER,
      wde_ap_ads_prs           NUMBER,
      wde_ap_ads_apo           NUMBER,
      wde_ap_ads_90            NUMBER,
      wde_ap_ads_90m           NUMBER,
      wde_ag_gt1_apg_p         NUMBER,
      wde_ag_gt1_apg_p2        NUMBER,
      wde_ag_gt1_apg_p3        NUMBER,
      wde_pr_gt1_apg_p         NUMBER,
      wde_pr_gt1_apg_p2        NUMBER,
      wde_pr_gt1_apg_p3        NUMBER,
      wde_agpr_gt1_apg_p       NUMBER,
      wde_agpr_gt1_apg_p2      NUMBER,
      wde_agpr_gt1_apg_p3      NUMBER,
      wde_ag_gt1_mtr_p         NUMBER,
      wde_ag_gt1_mtr_p2        NUMBER,
      wde_ag_gt1_mtr_p3        NUMBER,
      wde_pr_gt1_mtr_p         NUMBER,
      wde_pr_gt1_mtr_p2        NUMBER,
      wde_pr_gt1_mtr_p3        NUMBER,
      wde_agpr_gt1_mtr_p       NUMBER,
      wde_agpr_gt1_mtr_p2      NUMBER,
      wde_agpr_gt1_mtr_p3      NUMBER,
      wde_ag_st1_mtr_p         NUMBER,
      wde_ag_st1_mtr_p2        NUMBER,
      wde_ag_st1_mtr_p3        NUMBER,
      wde_pr_st1_mtr_p         NUMBER,
      wde_pr_st1_mtr_p2        NUMBER,
      wde_pr_st1_mtr_p3        NUMBER,
      wde_agpr_st1_mtr_p       NUMBER,
      wde_agpr_st1_mtr_p2      NUMBER,
      wde_agpr_st1_mtr_p3      NUMBER,
      wde_agpr_gt1_30703       NUMBER,
      wde_agpr_gt1_307032      NUMBER,
      wde_agpr_gt1_307033      NUMBER,
      wde_agpr_gt1_31613       NUMBER,
      wde_agpr_gt1_316132      NUMBER,
      wde_agpr_gt1_316133      NUMBER,
      wde_ag_s                 NUMBER,
      wde_pr_s                 NUMBER,
      wde_agpr_s               NUMBER,
      wee_ap_st1_ekmnap        NUMBER,
      wee_ap_st1_ekm           NUMBER,
      wee_ap_st1_ekmnap2       NUMBER,
      wee_ap_st1_ekmnap3       NUMBER,
      wee_ap_gt1_ekmnap        NUMBER,
      wee_ap_gt1_ekmnap2       NUMBER,
      wee_ap_gt1_ekmnap3       NUMBER,
      wee_ap_st1gt1_ekm1       NUMBER,
      wee_ap_gt1_ekmnap_s2     NUMBER,
      wee_ap_st1gt1_ekm3       NUMBER,
      wmy_ap1p                 NUMBER,
      wmy_ap1s                 NUMBER,
      wmy_ap2p                 NUMBER,
      wpy_ap1p                 NUMBER,
      wpy_ap1s                 NUMBER,
      wpy_ap2p                 NUMBER,
      wpm_ap1p                 NUMBER,
      wpm_ap1s                 NUMBER,
      wpm_ap2p                 NUMBER,
      wtm_ap1p                 NUMBER,
      wtm_ap1s                 NUMBER,
      wtm_ap2p                 NUMBER,
      wplp0_ap1p               NUMBER,
      wplp0_ap1s               NUMBER,
      wplp0_ap2p               NUMBER,
      wplp1_ap1p               NUMBER,
      wplp1_ap1s               NUMBER,
      wplp1_ap2p               NUMBER,
      wplp2_ap1p               NUMBER,
      wplp2_ap1s               NUMBER,
      wplp2_ap2p               NUMBER,
      wplp5_ap1p               NUMBER,
      wplp5_ap1s               NUMBER,
      wplp5_ap2p               NUMBER,
      wprep_aigp1              NUMBER,
      wprep_aigp2              NUMBER,
      wprep_aigp3              NUMBER,
      wprep_aigk1              NUMBER,
      wprep_aigk2              NUMBER,
      wprep_aigk3              NUMBER,
      weepr_ap1p               NUMBER,
      weepr_ap1p2              NUMBER,
      weepr_ap1s               NUMBER,
      weepr_ap1s2              NUMBER,
      weepr_ap2p               NUMBER,
      weepr_ap2p2              NUMBER,
      wprlast_ap1p             NUMBER,
      wprlast_ap1p2            NUMBER,
      wprlast_ap1p3            NUMBER,
      wprlast_ap1s             NUMBER,
      wprlast_ap1s2            NUMBER,
      wprlast_ap1s3            NUMBER,
      wprlast_ap2p             NUMBER,
      wprlast_ap2p2            NUMBER,
      wprlast_ap2p3            NUMBER,
      ap1s_flag                NUMBER,
      wdl_uag_sf               NUMBER,
      wdtecheckstart           DATE,
      wdtecheckend             DATE,
      wdl_uag_st1              NUMBER,
      e35b                     NUMBER,
      wdte_mtk_ap_prs          DATE,
      wdte_mtk_ap_apo          DATE,
      wdays_mtk                NUMBER,
      wp_ap_mtk                NUMBER,
      wap2p_flag               NUMBER,
      wsd_ap2p                 NUMBER,
      wsde_ap2p                NUMBER,
      wsde_ap2p_30703          NUMBER,
      wsp_min_h_ap2p           NUMBER,
      wsp_min_ap2p             NUMBER,
      wsp_min_ap2p_30703       NUMBER,
      wsee_ap_p                NUMBER,
      wsee_ap_p_30703          NUMBER,
      wsee_ap_s                NUMBER,
      wsee_ap_s_30703          NUMBER,
      wsdl_ap_31613            NUMBER
   );

-- type table
   TYPE payt_a68m5afm_tbl2 IS TABLE OF payt_a68m5afm_rec2;

-- function
   FUNCTION get_a68m5afm (
      in_afm       IN   edetedeaeehd.afm%TYPE,
      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
   )
      RETURN payt_a68m5afm_tbl2 PIPELINED;
--
--   FUNCTION get_a68m5afm2 (
--      in_afm       IN   edetedeaeehd.afm%TYPE,
--      in_tab_pah   IN   chetdiaelehd.tab_pah%TYPE
--   )
--      RETURN payt_a68m5afm_tbl2 PIPELINED;
--
--   function  get_pliromes_agroi_sum_afm (in_cdil_id in chetdiaelehd.id%type,
--                                         in_afm     in edetedeaeehd.afm%type,
--                                         in_tab_pah in chetdiaelehd.tab_pah%type)
--     return x_payt_plir_agroi_safm_tbl pipelined;
----
--   function get_pliromes_plp5sp (in_afm         in edetedeaeehd.afm%type,
--                                 in_cdil_id   in chetdiaelehd.id%type,
--                                 in_tab_pah     in chetdiaelehd.tab_pah%type default null)
--     return number;
----
END payfexatomikeysh;

