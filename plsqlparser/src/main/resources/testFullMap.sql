CREATE OR REPLACE PACKAGE BODY packageOne AS   
   PROCEDURE procedureOne (deptno NUMBER) IS
   BEGIN
      packageTwo.procedureTwo( 2 );  
	  procedureOne( 2 );
   END procedureOne;
 
   PROCEDURE procedureFour (emp_id NUMBER) IS
   BEGIN
      packageTwo.procedureThree( 2 );  
	  procedureFour( 2 );	
   END procedureFour;
END packageOne;

CREATE OR REPLACE PACKAGE BODY packageTwo AS   
   PROCEDURE procedureThree (deptno NUMBER) IS
   BEGIN
      procedureThree( 2 );  
	  packageOne.procedureFour( 2 );
   END procedureThree;
   
  PROCEDURE procedureTwo (emp_id NUMBER) IS
   BEGIN
      procedureTwo( 2 );  
	  packageOne.procedureOne( 2 );	
   END procedureTwo;
   
END packageTwo;


