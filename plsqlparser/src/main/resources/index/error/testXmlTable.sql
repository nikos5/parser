SELECT li.description
  FROM po_clob p,
       XMLTable('PurchaseOrder/LineItems/LineItem' PASSING p.OBJECT_VALUE
                COLUMNS description VARCHAR2(256)
                PATH '/LineItem/Description') li;