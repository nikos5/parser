CREATE OR REPLACE PACKAGE schema1.packageOne AS   
   PROCEDURE procedureOne (deptno NUMBER);
END packageOne;

CREATE OR REPLACE PACKAGE BODY schema1.packageOne AS   
   PROCEDURE procedureOne (deptno NUMBER) IS
   BEGIN
      schema2.packageTwo.procedureThree( 2 );
      sch3.pack3.proc4@here( 1 );
   END procedureOne;

END packageOne;

CREATE OR REPLACE PACKAGE schema2.packageTwo AS   
   PROCEDURE procedureThree (deptno NUMBER);
   PROCEDURE iDoNothing(inData NUMBER);
END packageTwo;

CREATE OR REPLACE PACKAGE BODY schema2.packageTwo AS   
   PROCEDURE procedureThree (deptno NUMBER) IS
   BEGIN
      schema1.packageOne.procedureOne( 2 );
   END procedureThree;
   PROCEDURE iDoNothing(inData NUMBER) IS
   BEGIN
	inData + 1;
   END
END packageTwo;
