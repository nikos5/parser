--CREATE TABLE sales_sum_table
  -- (month VARCHAR2(8), state VARCHAR2(40), sales NUMBER(10,2));

CREATE MATERIALIZED VIEW asales_sum_table
   ON PREBUILT TABLE WITH REDUCED PRECISION
   ENABLE QUERY REWRITE
   AS SELECT tt.calendar_month_desc AS month, 
             cc.cust_state_province AS state,
             SUM(ss.amount_sold) AS sales
      FROM times tt, customers cc, sales ss
      WHERE ss.time_id = tt.time_id AND ss.cust_id = cc.cust_id
      GROUP BY tt.calendar_month_desc, cc.cust_state_province;