CREATE MATERIALIZED VIEW sales_by_month_by_state
     TABLESPACE example
     PARALLEL 4
     BUILD IMMEDIATE
     REFRESH COMPLETE
     ENABLE QUERY REWRITE
     AS 
	 SELECT tt.calendar_month_desc, cc.cust_state_province,
        SUM(ss.amount_sold) AS sum_sales
        FROM times tt, sales ss, customers cc
        WHERE ss.time_id = tt.time_id AND ss.cust_id = cc.cust_id
        GROUP BY tt.calendar_month_desc, cc.cust_state_province;