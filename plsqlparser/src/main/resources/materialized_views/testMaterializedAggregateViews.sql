CREATE MATERIALIZED VIEW LOG ON times
   WITH ROWID, SEQUENCE (time_id, calendar_year)
   INCLUDING NEW VALUES;

CREATE MATERIALIZED VIEW LOG ON products
   WITH ROWID, SEQUENCE (prod_id)
   INCLUDING NEW VALUES;

CREATE MATERIALIZED VIEW sales_mv
   BUILD IMMEDIATE --it has materialized_view_props but has no physical_properties
   REFRESH FAST ON COMMIT
   AS SELECT tt.calendar_year, pp.prod_id, 
      SUM(ss.amount_sold) AS sum_sales
      FROM times tt, products pp, sales ss
      WHERE tt.time_id = ss.time_id AND pp.prod_id = ss.prod_id
      GROUP BY tt.calendar_year, pp.prod_id; --double the letters for recognizion