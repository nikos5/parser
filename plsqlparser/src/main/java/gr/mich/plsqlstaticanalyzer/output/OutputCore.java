package gr.mich.plsqlstaticanalyzer.output;

import gr.mich.plsqlstaticanalyzer.exceptions.ExceptionCore;
import gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions.ItemsNotFoundException;
import gr.mich.plsqlstaticanalyzer.output.function.OutputFunction;
import java.util.HashMap;
import java.util.Optional;

/**
 * OutputCore is the handler of the output system of the project
 * All the writing in the disk is handled by this class
 * 
 * @author Michael Michailidis
 */
public class OutputCore {    
    
    private final HashMap items;
    private OutputFunction outputFunction;
    
    /**
     * Initialize of the OutputCore. Needs a {@link OutputFunction} to operate
     * 
     * @param outputFunction The {@link OutputFunction} which will be used for the 
     *        actual output
     */
    public OutputCore(OutputFunction outputFunction) {
        items = new HashMap<>();
        this.outputFunction = outputFunction;
    }
   
    /**
     * Set the map that will be passed in the {@link OutputFunction}
     * 
     * @param items The {@link HashMap} which will be exported
     */
    public void setMap(HashMap items){
        this.items.putAll(items);
    }
    
    /**
     * Returns the map that is meant to be passed in the {@link OutputFunction}
     * 
     * @return Returns a HashMap
     */
    public HashMap getMap(){
        return items;        
    }
    
    /**
     * Uses the {@link OutputFunction} to write the map
     * and returns the {@link OutputFunction} results
     * 
     * @return Returns an {@link Optional} which contains the {@link String} which  
     *         was written. Else it is an {@link Optional#empty() empty optional}
     */
    public Optional<String> write(){
        if (items.isEmpty()) {
            ExceptionCore.throwNew(new ItemsNotFoundException());
            return Optional.empty();
        }
        
        return outputFunction.write(items);        
    }

    /**
     * Returns the currently loaded {@link OutputFunction}
     * 
     * @return Returns the {@link OutputFunction} which is currently being used
     */
    public OutputFunction getOutputFunction() {
        return outputFunction;
    }

    /**
     * Sets the {@link OutputFunction} which will be used
     * to print the results
     * 
     * @param outputFunction The new {@link OutputFunction} which will be used
     * @return The {@link OutputFunction} which was just setup
     */
    public OutputFunction setOutputFunction(OutputFunction outputFunction) {
        this.outputFunction = outputFunction;
        return this.outputFunction;
    }    
}
