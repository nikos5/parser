package gr.mich.plsqlstaticanalyzer.output.function;

import gr.mich.plsqlstaticanalyzer.analyzer.inner.AnalysisNode;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import gr.mich.plsqlstaticanalyzer.exceptions.ExceptionCore;
import gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions.MapWrongInstanceTypeException;
import gr.mich.plsqlstaticanalyzer.output.config.GephiProperties;
import gr.mich.plsqlstaticanalyzer.output.config.Properties;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Optional;
import org.openide.util.Exceptions;

/**
 * TreeToGephiFunction writes the HashMap to the disk in gexf format.
 * 
 * @author Michael Michailidis
 */
public class TreeToGephiFunction implements OutputFunction {

    private final Properties properties;
    private final List<String> edgeForParents;
    private final Map<String, Long> translatedEdges;

    private final String NODES_OPENING;
    private final String NODES_CLOSURE;

    private int edgeId;

    /**
     * Initialize the function with the given properties.
     * 
     * This function writes the input into the disk with the
     * gexf format
     * 
     * @param properties The properties which will be used from the function to
     *        operate
     */
    public TreeToGephiFunction(Properties properties) {
        this.edgeForParents = new ArrayList<>();
        this.translatedEdges = new HashMap<>();

        this.properties = properties;
        this.NODES_OPENING = "<nodes>";
        this.NODES_CLOSURE = "</nodes>";
        this.edgeId = 0;
    }

    /**
     * Default constructor. Initializes the function with the default properties
     */
    public TreeToGephiFunction() {
        this(GephiProperties.INSTANCE);
    }

    /**
     * Inserts into translatedEdges so it will be outputted in the {@literal <}edges{@literal >}
     * section
     * @param map The {@link HashMap} from which the translated map will be generated
     */
    private void generateTranslateMap(HashMap map) {
        map.values()
                .stream()
                .forEach(cnsmr -> {
                    translatedEdges
                            .put(
                                    ((AnalysisNode) cnsmr).createIdentifier(),
                                    ((AnalysisNode) cnsmr).getId()
                            );
                });
    }

    @Override
    public Optional<String> write(HashMap map) {
        if (!(map.values().toArray()[0] instanceof AnalysisNode)) {
            ExceptionCore.throwNew(new MapWrongInstanceTypeException());
            return Optional.empty();
        }

        String str;

        generateTranslateMap(map);

        str = getGexfHeaders();

        str = makeNodes(map, str);

        str = makeEdges(map, str);

        str = getGexfClosure(str);

//        System.out.println(str);

        File file = new File(properties.getFilePath());

        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(str);
            fileWriter.flush();
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }

        return Optional.of(str);
    }

    /**
     * The default header generated with the current time for gexf
     * ready for append in the gexf output
     * @return Returns the default header for gexf
     */
    private String getGexfHeaders() {

        DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
        Date today = Calendar.getInstance().getTime();

        String headers = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<gexf xmlns=\"http://www.gexf.net/1.2draft\"\n"
                + " xmlns:viz=\"http://www.gexf.net/1.2draft/viz\"\n"
                + " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n"
                + " xsi:schemaLocation=\"http://www.gexf.net/1.2draft\n"
                + "                     http://www.gexf.net/1.2draft/gexf.xsd\" \n"
                + " version=\"1.2\">\n"
                + "    <meta lastmodifieddate=\" " + df.format(today) + "\">\n"
                + "        <creator>PLSqlAnalyzer</creator>\n"
                //                + "        <description>PLSQL analysis</description>\n"
                + "    </meta>\n"
                + "    <graph mode=\"static\" defaultedgetype=\"directed\">\n       "
                + "<attributes class=\"node\">\n"
                + "            <attribute id=\"0\" title=\"parent\" type=\"string\"/>\n"
                + "            <attribute id=\"1\" title=\"nodeType\" type=\"string\"/>\n"
                + "        </attributes>\n";

        return headers;
    }

    /**
     * The default ending of a gexf file. Requires the variable that holds the 
     * gexf output and appends the ending to the end of that variable. 
     * 
     * @param temp The current variable with the gexf output
     * @return Returns the variable with gexf after it appended the gexf ending
     */
    private String getGexfClosure(String temp) {
        String closure = "    </graph>\n"
                + "</gexf>";

        return temp + closure;
    }

    /**
     * Generates the nodes from the HashMap. It appends the results into the 
     * variable which holds the gexf output
     * 
     * @param items The map with all the nodes to be converted
     * @param temp the variable with the gexf output
     * @return Returns the variable with the gexf output with the nodes appended
     */
    private String makeNodes(HashMap<Long, AnalysisNode> items, String temp) {
        List<String> lines = new ArrayList<>();

        lines.add(NODES_OPENING);

        items.values()
                .stream()
                .filter(item -> ItemType.getType(item.getMyMap().get(ItemField.ITEM_TYPE)).getLevel() == 0)
                .filter(item -> item.getInheritatedCalls().size() > 0)
                .forEach(item -> {
                    StringBuilder lineBuilder = new StringBuilder();
                    UpgradedEnumMap<ItemField, String> myMap = item.getMyMap();

                    lineBuilder.append("<node id=\"")
                            .append(myMap.get(ItemField.ID))
                            .append("\" label=\"")
                            .append(myMap.get(ItemField.ITEM_NAME))
                            .append("\">");
                    lines.add(lineBuilder.toString());

                    lines.add("<attvalues>");
                    lines.add("<attvalue for=\"0\" value=\"no_parent\"/>");
                    lines.add("<attvalue for=\"1\" value=\"" + myMap.get(ItemField.ITEM_TYPE) + "\"/>");
                    lines.add("</attvalues>");

                    lines.addAll(
                            inheritanceBuilder(
                                    item.getInheritatedCalls(),
                                    myMap.get(ItemField.ITEM_NAME),
                                    myMap.get(ItemField.ID))
                    );

                    lines.add("</node>");
                });

        items.values()
                .stream()
                .filter(item -> item.getMyMap().get(ItemField.ITEM_TYPE).equals(ItemType.LINK.toString()))
                .forEach(item -> {
                    StringBuilder lineBuilder = new StringBuilder();
                    UpgradedEnumMap<ItemField, String> myMap = item.getMyMap();

                    lineBuilder.append("<node id=\"")
                            .append(myMap.get(ItemField.ID))
                            .append("\" label=\"")
                            .append(myMap.get(ItemField.ITEM_NAME))
                            .append("\">");
                    lines.add(lineBuilder.toString());

                    lines.add("<attvalues>");
                    lines.add("<attvalue for=\"0\" value=\"no_parent\"/>");
                    lines.add("<attvalue for=\"1\" value=\"" + myMap.get(ItemField.ITEM_TYPE) + "\"/>");
                    lines.add("</attvalues>");

                    lines.add("</node>");
                });

        lines.add(NODES_CLOSURE);

        for (String vL : lines) {
            temp = temp + vL + "\n";
        }

        return temp;
    }

    /**
     * Generates the edges from the HashMap. It appends the results into the 
     * variable which holds the gexf output
     * 
     * @param items The map with all the edges to be converted
     * @param temp the variable with the gexf output
     * @return Returns the variable with the gexf output with the edges appended
     */
    private String makeEdges(HashMap<Long, AnalysisNode> items, String temp) {

        List<String> lines = new ArrayList<>();

        lines.add("<edges>");

        lines.addAll(edgeForParents);

        items.values()
                .stream()
                .filter(item -> ItemType.getType(item.getMyMap().get(ItemField.ITEM_TYPE)).getLevel() == 0)
                .forEach(item -> {
                    lines.addAll(edgeBuilder(item.getInheritatedCalls(), String.valueOf(item.getId())));
                });

        lines.add("</edges>");

        for (String vL : lines) {
            temp = temp + vL + "\n";
        }

        return temp;
    }

    /**
     * Edge extractor from the {@link AnalysisNode}. It find the nodes and collects them in a 
     * format that allows them to be written in the gexf file
     * 
     * @param inheritatedNodes the {@link AnalysisNode} in which it search for the nodes
     * @param parent The name of the parental node because the function is Recursive
     * @param parent_id The id of the parental node because the function is Recursive (not used yet)
     * @return Returns a list of strings with the formated nodes
     */
    private List<String> inheritanceBuilder(List<AnalysisNode> inheritatedNodes, String parent, String parent_id) {
        List<String> lines = new ArrayList<>();

        if (inheritatedNodes.isEmpty()) {
            return lines;
        }

        lines.add(NODES_OPENING);

        inheritatedNodes
                .stream()
                .filter(item -> !item.getMyMap().get(ItemField.ITEM_TYPE).equals(ItemType.CALL.toString()))
                .forEach(item -> {
                    UpgradedEnumMap<ItemField, String> myMap = item.getMyMap();

                    StringBuilder lineBuilder = new StringBuilder();

                    lineBuilder.append("<node id=\"")
                            .append(myMap.get(ItemField.ID))
                            .append("\" label=\"")
                            .append(myMap.get(ItemField.ITEM_NAME))
                            .append("\">");
                    lines.add(lineBuilder.toString());

                    lines.add("<attvalues>");
                    lines.add("<attvalue for=\"0\" value=\"" + parent + "\"/>");
                    lines.add("<attvalue for=\"1\" value=\"" + myMap.get(ItemField.ITEM_TYPE) + "\"/>");
                    lines.add("</attvalues>");

                    lines.addAll(
                            inheritanceBuilder(
                                    item.getInheritatedCalls(),
                                    myMap.get(ItemField.ITEM_NAME),
                                    myMap.get(ItemField.ID))
                    );

                    lines.add("</node>");
                });

        lines.add(NODES_CLOSURE);

        return lines;
    }

    /**
     * Edge extractor from the {@link AnalysisNode}. It find the edges and collects them in a 
     * format that allows them to be written in the gexf file
     * 
     * @param inheritatedNodes The {@link AnalysisNode} in which it will search for edges
     * @param parent_id the id of the parent node because this function is Recursive 
     *        ( not used yet )
     * @return Returns a list of strings with the formated edges
     */
    private List<String> edgeBuilder(List<AnalysisNode> inheritatedNodes, String parent_id) {
        List<String> lines = new ArrayList<>();

        if (inheritatedNodes.isEmpty()) {
            return lines;
        }

        inheritatedNodes
                .stream()
                .forEach(item -> {
                    item.getMap()
                            .values()
                            .stream()
                            .filter(innerItem -> innerItem.getCalls() > 0)
                            .forEach(innerItem -> {
                                StringBuilder row = new StringBuilder();

                                row.append("<edge id=\"")
                                        .append(edgeForParents.size() + edgeId)
                                        .append("\" source=\"")
                                        .append(item.getId())
                                        .append("\" target=\"")
                                        .append(translatedEdges.containsKey(innerItem.createIdentifier())
                                                ? translatedEdges.get(innerItem.createIdentifier())
                                                : "-1")
                                        .append("\" weight=\"")
                                        .append(innerItem.getCalls())
                                        .append("\"/>")
                                        .append(translatedEdges.containsKey(innerItem.createIdentifier())
                                        ? "" : "<!--" + innerItem.getItem().get(ItemField.ITEM_NAME) +"-->");

                                lines.add(row.toString());
                                edgeId++;
                            });
                });

        inheritatedNodes
                .stream()
                .filter(prdct -> !ItemType.getType(prdct.getMyMap().get(ItemField.ITEM_TYPE)).canCall())
                .forEach(item -> {
                    lines.addAll(edgeBuilder(item.getInheritatedCalls(), String.valueOf(item.getId())));
                });

        return lines;
    }
}