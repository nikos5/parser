package gr.mich.plsqlstaticanalyzer.output.config;

/**
 *
 * @author Michael Michailidis
 */
public interface Properties {
    /**
     * The check if the headers should be included or not
     * 
     * @return Returns true if the function should include headers 
     * else it returns false
     */
    boolean isHeaders();
    
    /**
     * @return Returns the file path in which the function should write 
     * the output
     */
    String getFilePath();
    
    /**
     * The delimiter to split the values
     * @return Returns the delimiter which the function use to split the values
     */
    String getDelimiter();
}
