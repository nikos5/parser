package gr.mich.plsqlstaticanalyzer.output.config;

/**
 * The default output properties
 * 
 * @author Michael Michailidis
 */
public enum DefaultProperties implements Properties{
    INSTANCE;

    @Override
    public boolean isHeaders() {
        return false;
    }

    @Override
    public String getFilePath() {
        String userDesktop = System.getProperty("user.home");
        userDesktop = userDesktop + "\\Desktop\\output3.txt";
        return userDesktop;
    }

    @Override
    public String getDelimiter() {
        return ";";
    }    
}
