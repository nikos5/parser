package gr.mich.plsqlstaticanalyzer.output.function;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import gr.mich.plsqlstaticanalyzer.exceptions.ExceptionCore;
import gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions.MapWrongInstanceTypeException;
import gr.mich.plsqlstaticanalyzer.output.config.DefaultProperties;
import gr.mich.plsqlstaticanalyzer.output.config.Properties;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Optional;
import org.openide.util.Exceptions;

/**
 * TreeToTxtFunction writes the HashMap to the disk in txt format
 * @author Michael Michailidis
 */
public class TreeToTxtFunction implements OutputFunction {

    private final Properties properties;

    public TreeToTxtFunction(Properties properties) {
        this.properties = properties;
    } 
    
    public TreeToTxtFunction(){
        this.properties = DefaultProperties.INSTANCE;
    }
    
    @Override
    public Optional<String> write(HashMap map) {
        if( !(map.values() instanceof UpgradedEnumMap )) {
            ExceptionCore.throwNew(new MapWrongInstanceTypeException());
            return Optional.empty();
        }      

        String str = "";

        if(properties.isHeaders()){
            str = str + getHeaders();
        }
        
        for (Object value : map.values()) {
            str = str + getRow((UpgradedEnumMap)value,properties.getDelimiter()) + "\n";
        }
        
        try (PrintWriter writer = new PrintWriter(properties.getFilePath(), "UTF-8")) {
            writer.write(str);
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Exceptions.printStackTrace(ex);
        }
        
        return Optional.of(str);
    }
    
    /**
     * Prerequisites : ItemField
     * 
     * @param field
     * @return 
     */
    private String getRow(UpgradedEnumMap field, String delimiter) {
        String str = "";
        
        for (ItemField vL : ItemField.values()) {
            String tmp = (String) field.get(vL);
            if (tmp == null) {
                tmp = "";
            }
            str = str + tmp + delimiter;
        }

        return str.substring(0, str.length() - 1);
    }
    
    /**
     * Return the basic headers from the properties
     * and the {@link ItemField}
     * @return 
     */
    private String getHeaders(){
        String str = "";
        
        for(ItemField vL : ItemField.values()) {
            str = str + vL.toString() + properties.getDelimiter();
        } 
        
        return str;
    }
}