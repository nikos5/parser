package gr.mich.plsqlstaticanalyzer.output.function;

import java.util.HashMap;
import java.util.Optional;

/**
 * The implementations of this interface are functions that write a HashMap
 * to the disk
 * 
 * @author Michael Michailidis
 */
public interface OutputFunction {
    /**
     * Write the given map where the loaded properties define
     * in the format that the implementation uses.
     * 
     * @param map The {@link HashMap} which will me exported
     * @return Returns the output as String also
     */
    Optional<String> write(HashMap map);    
}
