package gr.mich.plsqlstaticanalyzer.output.config;

/**
 *
 * @author Michael Michailidis
 */
public enum GephiNodePropertiesDefault implements GephiNodeProperties {
    RED(0,"255,0,0",4,4),
    GREEN(1,"0,255,0",3,3),
    BLUE(2,"0,0,255",2,2),
    BLACK(3,"0,0,0",1,1);
    
    private final int level;
    private final String rgb;
    private final double width;
    private final double height;

    private GephiNodePropertiesDefault(int level, String rgb,double width, double height) {
        this.level = level;
        this.rgb = rgb;    
        this.width = width;
        this.height = height;
    }
    
    @Override
    public String getColor() {
        return rgb;
    }
    
    @Override
    public int getLevel(){
        return level;
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getHeight() {
        return height;
    }    
}
