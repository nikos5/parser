package gr.mich.plsqlstaticanalyzer.output.config;

import static gr.mich.plsqlstaticanalyzer.output.config.GephiNodePropertiesDefault.BLACK;
import static gr.mich.plsqlstaticanalyzer.output.config.GephiNodePropertiesDefault.values;

/**
 *
 * @author Michael Michailidis
 */
public interface GephiNodeProperties {
    String getColor();        
    int getLevel();
    double getHeight();
    double getWidth();
    
    static String getColor(int level) {
        for(GephiNodePropertiesDefault vL:values()) {
            if ( vL.getLevel() == level ) {
                return vL.getColor();
            }
        }
        
        return BLACK.getColor();
    }
    
    static double getWidth(int level) {
        for(GephiNodePropertiesDefault vL:values()) {
            if ( vL.getLevel() == level ) {
                return vL.getWidth();
            }
        }
        
        return BLACK.getWidth();        
    }
    
    static double getHeight(int level) {
        for(GephiNodePropertiesDefault vL:values()) {
            if ( vL.getLevel() == level ) {
                return vL.getHeight();
            }
        }
        
        return BLACK.getHeight();        
    }
}
