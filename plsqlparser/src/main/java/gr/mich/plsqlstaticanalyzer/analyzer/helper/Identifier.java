package gr.mich.plsqlstaticanalyzer.analyzer.helper;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;

/**
 * Contains a function which helps in the identification of the nodes.
 * It generate a unique identifier that helps in recognizing the nodes
 * and storing them in HashMaps
 * 
 * @author Michael Michailidis
 */
public class Identifier {
    /**
     * Generates a unique String identifier for the input node
     * @param me The input node
     * @return Returns the unique identifier
     */
    public static String createIdentifier(UpgradedEnumMap me) {
        StringBuilder str = new StringBuilder();
        
        if(me.get(ItemField.ITEM_TYPE).equals(ItemType.CALL.toString())) {            
            str.append(me.get(ItemField.OTHER_ITEM_NAME).toString());
            str.append(me.get(ItemField.OTHER_PACKAGE_NAME).toString());
            str.append(me.get(ItemField.OTHER_SCHEMA_NAME).toString());
            str.append(me.get(ItemField.LINK_NAME).toString());
        } else {
            str.append(me.get(ItemField.ITEM_NAME).toString());
            
            str.append(me.containsKey(ItemField.MY_PACKAGE_NAME) 
                    ? me.get(ItemField.MY_PACKAGE_NAME).toString() 
                    : ItemField.MY_PACKAGE_NAME.getDefault());
            
            str.append(me.containsKey(ItemField.MY_SCHEMA_NAME) 
                    ? me.get(ItemField.MY_SCHEMA_NAME).toString() 
                    : ItemField.MY_SCHEMA_NAME.getDefault());
            
            str.append(me.containsKey(ItemField.LINK_NAME) 
                    ? me.get(ItemField.LINK_NAME).toString() 
                    : ItemField.LINK_NAME.getDefault());            
        }
        
        return str.toString();
    }
}
