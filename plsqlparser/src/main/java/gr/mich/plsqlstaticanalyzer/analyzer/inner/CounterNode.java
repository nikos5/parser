package gr.mich.plsqlstaticanalyzer.analyzer.inner;

import gr.mich.plsqlstaticanalyzer.analyzer.helper.Identifier;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.Objects;

/**
 * The CounterNode class is a holder of a node and its counter
 * Its purpose is to associate a counter with a specific node
 * so the counting of its calls and the print will be easier
 * 
 * @author Michael Michailidis
 */
public class CounterNode {

    private final UpgradedEnumMap<ItemField, String> item;
    private long calls;

    /**
     * Generates a CounterNode from the given Node
     * @param item The node which the {@link CounterNode} will contain
     */
    public CounterNode(UpgradedEnumMap<ItemField, String> item) {
        this.item = item;
        this.calls = 0;
    }

    /**
     * Increase the total calls of the counter by 1
     * @return Returns the same item for further use
     */
    public CounterNode increaseCalls() {
        calls++;
        return this;
    }

    /**
     * The total calls
     * @return Returns the total counted calls 
     */
    public long getCalls() {
        return calls;
    }

    /**
     * The saved item in the CounterNode
     * @return Returns the item that is saved in the CounterNode
     */
    public UpgradedEnumMap<ItemField, String> getItem() {
        return item;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof CounterNode)) {
            return false;
        }

        return isTheSame((CounterNode) o);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.item);
        hash = 61 * hash + (int) (this.calls ^ (this.calls >>> 32));
        return hash;
    }

    /**
     * Validates if the given counterNode is the same with this
     * 
     * @param cN The other {@link CounterNode} which will be checked if its the 
     *        same
     * @return True if Objects.equals() returns true. it compares to 
     * Strings 
     */
    public boolean isTheSame(CounterNode cN) {
        String itemStr = item.toSerializedString();
        itemStr = itemStr.substring(itemStr.indexOf(",") + 1);
        
        String otherStr = cN.getItem().toSerializedString();
        otherStr = otherStr.substring(otherStr.indexOf(",") + 1);
        
        return Objects.equals(itemStr, otherStr);
    }
    
    /**
     * Generates a unique identifier using the {@link Identifier} class
     * @return Returns the identifier of the {@link CounterNode}
     */
    public String createIdentifier(){        
        return Identifier.createIdentifier(item);
    }
}
