package gr.mich.plsqlstaticanalyzer.analyzer.inner;

import com.google.common.base.Objects;
import gr.mich.plsqlstaticanalyzer.analyzer.helper.Identifier;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.ItemGenerator;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPart;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * AnalysisNode is an Object that contains a node and the information 
 * associated with it. 
 * 
 * Those informations are the current id of the node the {@link CounterNode 
 * counterNodes} which contain info about the calls to other nodes and lastly it 
 * has information about inherited calls from the child nodes of the
 * current node inside this item.
 * 
 * @author Michael Michailidis
 */
public class AnalysisNode {

    private final UpgradedEnumMap me;
    private final HashMap<Long, CounterNode> map; //all the other nodes
    private final List<AnalysisNode> inheritatedCalls;
    private final long myId;

    /**
     * Default constructor with initializer and node setup
     * @param me The node which the {@link AnalysisNode} will contain
     */
    public AnalysisNode(UpgradedEnumMap me) {
        this.me = me;
        this.myId = Long.valueOf(me.get(ItemField.ID).toString());
        this.map = new HashMap<>();
        this.inheritatedCalls = new ArrayList<>();
    }

    /**
     * Register a node in the map which will help for the counting
     * @param other The node that will be used for counting
     */
    public void registerNodeForCounting(UpgradedEnumMap other) {
        this.map.put(
                Long.valueOf(
                        other.get(ItemField.ID).toString()
                ),
                new CounterNode(other)
        );
    }

    /**
     * Calculates the calls that this item makes to the other items
     */
    public void count() {
        ItemType myType = ItemType.getType(me.get(ItemField.ITEM_TYPE).toString());
        if (!myType.canCall()) {
            return;
        }

        AnalyzerPart analyzerPart = ItemGenerator.getAnalyzerPart(myType.toString());

        analyzerPart.exec(me, map);
    }

    /**
     * Executes a check for inherited calls in the map.
     * 
     * @param allItems {@link HashMap} all the items that will be checked for 
     *        inheritance
     */
    public void inheritanceCheck(HashMap<Long, AnalysisNode> allItems) {
        ItemType myType = ItemType.getType(me.get(ItemField.ITEM_TYPE).toString());
        if (myType.canCall()) {
            return;
        }

        AnalyzerPart analyzerPart = ItemGenerator.getAnalyzerPart(myType.toString());

        allItems.values()
                .stream()
                .filter(vL -> analyzerPart.inheritanceCheck(me, vL))
                .forEach(vL -> inheritatedCalls.add((AnalysisNode) vL));
    }

    /**
     * Removes all the 0 calls from the map of {@link CounterNode counterNodes}
     */
    public void clearZeroCalls() {
        List<CounterNode> collected = this.map.values()
                .stream()
                .filter((value) -> {
                    return value.getCalls() != 0;
                })
                .collect(
                        Collectors.toList()
                );

        map.clear();

        collected.stream().forEach((value) -> {
            map.put(
                    Long.valueOf(value.getItem().get(ItemField.ID)),
                    value
            );
        });
    }

    /**
     * The map of the {@link CounterNode counterNodes}
     * @return Returns a map of {@link CounterNode counterNodes}
     */
    public HashMap<Long, CounterNode> getMap() {
        return map;
    }

    /**
     * The node that the {@link AnalysisNode} contains
     * @return Returns the node of this {@link AnalysisNode}
     */
    public UpgradedEnumMap<ItemField, String> getMyMap() {
        return me;
    }

    /**
     * The {@link AnalysisNode} Id
     * @return Returns the id of the {@link AnalysisNode}
     */
    public long getId() {
        return myId;
    }

    /**
     * The list of the inherited calls
     * @return Returns a list of inherited calls
     */
    public List<AnalysisNode> getInheritatedCalls() {
        return inheritatedCalls;
    }

    /**
     * Merges the calls that are same by increasing the count
     * @return returns a list of id of the merged calls
     */
    public List<Long> mergeCalls() {
        List<CounterNode> lst = new ArrayList<>();
        List<Long> toRemove = new ArrayList<>();

        inheritatedCalls.stream().filter(prdct
                -> prdct.getMyMap()
                .get(ItemField.ITEM_TYPE)
                .equals(ItemType.CALL.toString()))
                .forEach(cnsmr -> {
                    CounterNode counterNode = new CounterNode(cnsmr.getMyMap());
                    if (lst.contains(counterNode)) {
                        lst.get(lst.indexOf(counterNode)).increaseCalls();
                    } else {
                        lst.add(counterNode.increaseCalls());
                    }
                    toRemove.add(cnsmr.getId());
                });

        lst.stream().forEach(cnsmr -> map.put((long) map.size(), cnsmr));

        return toRemove;
    }

    /**
     * Remove the nodes that where inherited
     * @param lgn A {@link List} of {@link Long} of the identification to 
     * be removed
     */
    public void cleanUp(List<Long> lgn) {
        List<AnalysisNode> toRemove = new ArrayList<>();

        lgn.stream().forEach(cnsmr -> {
            for (int i = 0; i < inheritatedCalls.size(); i++) {
                if (Objects.equal(inheritatedCalls.get(i).getId(), cnsmr)) {
                    toRemove.add(inheritatedCalls.get(i));
                }
            }
        });

        inheritatedCalls.removeAll(toRemove);
    }
    
    /**
     * Generates a unique identifier using the {@link Identifier} class
     * @return Returns the identifier of the {@link AnalysisNode}
     */
    public String createIdentifier(){        
        return Identifier.createIdentifier(me);
    }
}
