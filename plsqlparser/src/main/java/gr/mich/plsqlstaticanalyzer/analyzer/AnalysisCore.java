package gr.mich.plsqlstaticanalyzer.analyzer;

import gr.mich.plsqlstaticanalyzer.analyzer.inner.AnalysisNode;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * AnalysisCore is the handler class of the Analysis part. Everything 
 * that has to do with Analysis are handled by this class.
 * 
 * @author Michael Michailidis
 */
public class AnalysisCore {
    private final HashMap<Long, AnalysisNode> map;

    /**
     * Default constructor with the basic initializes
     */
    public AnalysisCore() {
        this.map = new HashMap<>();
    }

    /**
     * Initiates the basic Analysis procedure. Accepts the map with the extracted 
     * nodes from the grammar and starts a series of mutations and counting until
     * all the items are counted and placed together.
     * 
     * @param map The HashMap which contains all the items extracted from the
     * grammar
     */
    public void initiateAnalyze(HashMap<Long, UpgradedEnumMap> map) {
        //convert all the items into AnalysisNodes
        map.values()
                .stream()
                .forEach((value) -> {
                    this.map.put(
                            Long.valueOf(value.get(ItemField.ID).toString()),
                            new AnalysisNode(value)
                    );
                });

        //convert again all the items into CounterNodes for each AnalysisNode
        this.map.values()
                .stream()
                .forEach((value) -> {
                    map.keySet()
                            .stream()
                            .filter((id) -> id != value.getId())
                            .forEach((innerValue) -> {
                                value.registerNodeForCounting(
                                        map.get(innerValue)
                                );
                            });
                });

        initializeCounting();
    }

    /**
     * Count all the items and calls.
     * Clear the 0 call items that were created for counting purposes
     * Check the inheritance of calls which makes available -for example- for 
     * the package to know all the calls the child-functions do. 
     * Last it merge the calls to the same items by increasing the counter 
     */
    private void initializeCounting() {

        this.map.values()
                .stream()
                .forEach((value) -> {
                    value.count();
                });

        //TODO link chehckup -> need Link Item
        
        this.map.values()
                .stream()
                .forEach((value) -> {
                    value.clearZeroCalls();
                });

        this.map.values()
                .stream()
                .forEach(item -> {
                    item.inheritanceCheck(map);
                });

        List<Long> lgn = new ArrayList<>();

        this.map.values()
                .stream()
                .forEach(item -> {
                    lgn.addAll(item.mergeCalls());
                });

        //cleanUp of merged calls start here.. if i need it ofc
        lgn.stream()
                .forEach(value -> {
                    if (map.containsKey(value)) {
                        map.remove(value);
                    }
                });

        this.map.values()
                .stream()
                .forEach(cnsmr -> {
                    cnsmr.cleanUp(lgn);
                });
        //cleanUp ends here
        
        //check testGeneral / schema46 / debug id -> 67
    }

    /**
     * The map which the class contains
     * @return Returns the map that the class contains. Should be called 
     * after the analysis is done
     */
    public HashMap<Long, AnalysisNode> getMap() {
        return map;
    }
}
