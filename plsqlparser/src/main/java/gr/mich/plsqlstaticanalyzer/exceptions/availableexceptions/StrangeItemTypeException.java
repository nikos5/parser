package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemData;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;

/**
 * This exception should be thrown when an item which wasn't supposed to be 
 * there comes up!
 * 
 * @author Michael Michailidis
 */
public class StrangeItemTypeException extends AbstractException {
    public StrangeItemTypeException(ItemData itemType) {
        super("Found " + itemType.toString() + " while wasnt expecting it");
    }    
    
    public StrangeItemTypeException(ItemField itemField) {
        super("Found " + itemField.toString() + " while wasnt expecting it");
    }    
    
    public StrangeItemTypeException(String text) {
        super("Found " + text + " while wasnt expecting it");
    }    
}
