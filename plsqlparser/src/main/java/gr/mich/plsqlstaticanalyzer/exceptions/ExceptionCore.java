package gr.mich.plsqlstaticanalyzer.exceptions;

import org.netbeans.core.startup.logging.NbFormatter;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ExceptionCore is the handler of the exceptions. It debuggingLogsEnabled the errors
 * that are thrown while the program is running so it wont crush
 * 
 * @author Michael Michailidis
 */
public class ExceptionCore {
    private static final Logger LOG = Logger.getLogger(ExceptionCore.class.getName());
    private static boolean isActive = true;
    private static boolean wasCalled = false;

    /**
     * Writes to the log a new exception that occurred
     * @param ex The new {@link MyException} which will be declared thrown
     */
    public static void throwNew (MyException ex) {
        //After an error deactivate logger
        if (!isActive) {
            return;
        }

        wasCalled = true;

        System.out.println(ex.getMessage());

            StackTraceElement[] stack;
            stack = Thread.getAllStackTraces().get(Thread.currentThread());

            try {
                FileHandler fileHandler = new FileHandler("ErrLog.log");
                fileHandler.setFormatter(new NbFormatter());

                LOG.addHandler(fileHandler);
            } catch (IOException | SecurityException ex1) {
                isActive = false;
            }

            String err;

            err = "An error was produced with the message : " + ex.getMessage() + "\n";

                err = err + "The error was produced from : " + "\n";
                for (int i = 3; i < stack.length; i++) {
                    err = err + "\t" + (i - 2) + ". " + stack[i].getClassName() +
                            " : " + stack[i].getMethodName()
                            + "( " + stack[i].getLineNumber() + " )" + "\n";
                }
            LOG.log(Level.ALL, err);
        }
    
    /**
     * Check if the ExceptionCore was called
     * @return Returns true if the ExceptionCore was called even once
     * else it returns false;
     */
    public static boolean wasCalled(){
        return wasCalled;
    }

    /**
     * enables debugging information.
     * All information will be provided.
     */
    public static void enableDebuggingLevel() { LOG.setLevel(Level.ALL); }

    /**
     * Enables warning logs.
     * Errors and warning will be provided
     * without any debugging information.
     */
    public static void enableWarningLevel(){
        LOG.setLevel(Level.WARNING);
    }

    /**
     * Enables info logs.
     * Errors will be provided
     * without any debugging information.
     */
    public static void enableDefaultLevel(){
        LOG.setLevel(Level.INFO);
    }
}
