package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;

/**
 * This exception should be thrown when a factory class is missing
 * @author Michael Michailidis
 */
public class FactoryDoesNotExistException extends AbstractException{
    public FactoryDoesNotExistException(String factoryName) {
        super("Factory " + factoryName + " does not exist");
    }        
}
