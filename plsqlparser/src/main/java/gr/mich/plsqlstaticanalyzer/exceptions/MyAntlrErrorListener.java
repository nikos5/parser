package gr.mich.plsqlstaticanalyzer.exceptions;

import gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions.TokenException;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * This listener purpose to is handle the ANTLR errors.. if its called there is 
 * an error in the ANTLR grammar which wasn't supposed to exist
 * @author Michael Michailidis
 */
public class MyAntlrErrorListener extends ConsoleErrorListener implements ANTLRErrorListener{

    @Override
    public void syntaxError(Recognizer<?, ?> rcgnzr, Object o, int i, int i1, String string, RecognitionException re) {
        String str = string + " at " + i + ":" + i1;
        ExceptionCore.throwNew(new TokenException(str));
    }
}
