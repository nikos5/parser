package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;

/**
 * This exception should be thrown when there is an AnalysisError
 * 
 * @author Michael Michailidis
 */
public class AnalysisException extends AbstractException {

    public AnalysisException(UpgradedEnumMap me) {
        super("The id with id : " + me.get(ItemField.ID).toString() + "and name : "
                + me.get(ItemField.ITEM_NAME) + " produced an error with the item "
                + "type : "+ me.get(ItemField.ITEM_TYPE).toString());
    }
    
}
