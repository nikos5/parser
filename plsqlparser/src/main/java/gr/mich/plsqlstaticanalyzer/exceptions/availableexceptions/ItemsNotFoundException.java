package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;

/**
 * This exception should be thrown when trying to access a field in the node 
 * that doesn't exist
 * 
 * @author Michael Michailidis
 */
public class ItemsNotFoundException extends AbstractException{
    public ItemsNotFoundException( ) {
        super("Output failed cause no items found in the hashmap");
    }        
}
