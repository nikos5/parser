package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;

/**
 *
 * @author Michael Michailidis
 */
public class RenamePartForFunction extends AbstractRenamePart implements RenamePart {

    public RenamePartForFunction() {
        addItemToList(
                ItemField.MY_SCHEMA_NAME.hasDependency(ItemField.MY_PACKAGE_NAME)
        );
        addItemToList(
                ItemField.MY_PACKAGE_NAME.isDependency(ItemField.MY_SCHEMA_NAME)
        );
    }

}
