package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts;

import gr.mich.plsqlstaticanalyzer.analyzer.inner.AnalysisNode;
import gr.mich.plsqlstaticanalyzer.analyzer.inner.CounterNode;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.HashMap;

/**
 *
 * @author Michael Michailidis
 */
public class AnalyzerPartForMaterializedViewLog implements AnalyzerPart {

    @Override
    public void exec(UpgradedEnumMap<ItemField, String> me, 
            HashMap<Long, CounterNode> targets) {
    }

    @Override
    public boolean inheritanceCheck(UpgradedEnumMap<ItemField, String> me, AnalysisNode target) {
        return false;
    }
    
}
