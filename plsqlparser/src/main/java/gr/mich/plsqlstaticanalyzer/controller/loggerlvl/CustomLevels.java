package gr.mich.plsqlstaticanalyzer.controller.loggerlvl;

import java.util.logging.Level;

/**
 * Custom levels for the logger in which it will operate
 * 
 * @author Michael Michailidis
 */
public class CustomLevels extends Level{
    public static final Level FINALLIZE = new CustomLevels("FINALLIZE", Level.SEVERE.intValue() - 1 );
    public static final Level ITEM_SET = new CustomLevels("ITEM_SET", Level.SEVERE.intValue() - 2 );
    public static final Level REGISTERING_PRIVATE_FIELDS = new CustomLevels("REGISTERING_PRIVATE_FIELDS", Level.SEVERE.intValue() - 3 );
    public static final Level RESETTING_PRIVATE_FIELDS = new CustomLevels("RESETTING_PRIVATE_FIELDS", Level.SEVERE.intValue() - 4 );
    public static final Level ITEM_APPEND = new CustomLevels("ITEM_APPEND", Level.SEVERE.intValue() - 5);
    
    public static final Level ITEM_RENAME_MODE = new CustomLevels("ITEM_RENAME_MODE", Level.SEVERE.intValue() - 6);
    
    
    public CustomLevels(String string, int i) {
        super(string, i);
    }    
}
