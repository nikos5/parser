package gr.mich.plsqlstaticanalyzer.controller.enumerations.source;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.helper.CheckListItem;

/**
 * All the possible fields and item can contain
 * @author Michael Michailidis
 */
public enum ItemField {
    ID(0, "-1"),
    ITEM_TYPE(1, "no_type"),
    ITEM_NAME(2, "no_name"),
    OTHER_ITEM_NAME(6, "no_other_name"),
    MY_PACKAGE_NAME(3, "no_package"),
    OTHER_PACKAGE_NAME(7, "no_other_package"),
    MY_SCHEMA_NAME(4, "no_schema"),
    OTHER_SCHEMA_NAME(8, "no_other_schema"),
    EXTRA_NAME(5, "no_extra_name"),
    EXTRA_SCHEMA_NAME(9, "no_extra_schema_name"),
    LINK_NAME(10, "no_link_name"),
    EXTRA_LINK_NAME(11, "no_extra_link_name"),
    FROM_CLAUSE_NAME(12, "no_from_clause_name"),
    FROM_CLAUSE_SCHEMA(13, "no_from_clause_schema"),
    FROM_CLAUSE_LINK(14, "no_from_clause_link"),
    TABLE_NAME(15, "no_table"),
    APPENDABLE(16, "false");

    private final int columnCode;
    private final String defaultValue;

    private ItemField(int columnCode, String defaultValue) {
        this.columnCode = columnCode;
        this.defaultValue = defaultValue;
    }

    /**
     * The ColumnCode that this ItemField has
     * @return Returns the code of the column in which this field is
     */
    public int getColumnCode() {
        return columnCode;
    }

    /**
     * The default ItemField value
     * @return Returns the default value of the ItemField
     */
    public String getDefault() {
        return defaultValue;
    }

    /**
     * Converts the input Integer to an ItemField
     * @param i the input Integer
     * @return Returns the associated ItemField of the input Integer
     */
    public ItemField getField(int i) {
        for (ItemField vLookUp : values()) {
            if (vLookUp.getColumnCode() == i) {
                return vLookUp;
            }
        }
        throw new UnsupportedOperationException();
    }

    // thisField . isDependency . ofThatField
    //if this field will provide info for others
    /**
     * To assist the renaming part this function marks the item as a dependency 
     * on one of its fields
     * 
     * @param fieldForUpdate which field is the dependency which will pass the 
     *        value it contains to others
     * @return Returns a ChecklistItem with the correct dependencies marked
     */
    public CheckListItem isDependency(ItemField fieldForUpdate) {
        return new CheckListItem(getField(columnCode), fieldForUpdate, true);
    }

    
    /**
     * To assist the renaming part this function marks the item as a dependency 
     * on one of its fields. It supports also the masking of the field to be found by a 
     * different name
     * 
     * @param fieldForUpdate which field is the dependency which will pass the 
     *        value it contains to others
     * @param fieldForMatchMask The ItemFieldName value that will be used to mask
     *        when searching for the value to pass down
     * @return Returns a ChecklistItem with the correct dependencies marked
     */
    public CheckListItem isDependency(ItemField fieldForUpdate, 
            ItemField fieldForMatchMask) {
        return new CheckListItem(getField(columnCode), fieldForUpdate, true, fieldForMatchMask);
    }

    //if this field expect info from others
    // thisField . hasDependency . ofThatField
    /**
     * To assist the renaming part this function marks the item that has a dependency 
     * on one of its fields.
     * 
     * @param fieldForUpdate The Field that has dependency on the value of an 
     *        other item
     * @return Returns a ChecklistItem with the correct dependencies marked
     */
    public CheckListItem hasDependency(ItemField fieldForUpdate) {
        return new CheckListItem(fieldForUpdate, getField(columnCode), false);
    }
    
   /**
     * To assist the renaming part this function marks the item that has a dependency 
     * on one of its fields. It also supports the masking of the field that has
     * the dependency on an other field
     * 
     * @param fieldForUpdate The Field that has dependency on the value of an 
     *        other item
     * @param fieldForUpdateMask The mask of the field that has the dependency on an 
     *        other field
     * @return Returns a ChecklistItem with the correct dependencies marked
     */
    public CheckListItem hasDependency(ItemField fieldForUpdate, 
            ItemField fieldForUpdateMask) {
        return new CheckListItem( fieldForUpdate, getField(columnCode),false, fieldForUpdateMask);
    }

   /**
     * To assist the renaming part this function marks the item that has a dependency 
     * on one of its fields. It also supports the masking of the field that may contain 
     * the value which the dependency is on and the masking of the field that has the 
     * dependency on an other field
     * 
     * @param fieldForUpdate The Field that has dependency on the value of an 
     *        other item
     * @param fieldForMatchMask The mask of the field that contains the dependency
     * @param fieldForUpdateMask The mask of the field that has the dependency on an 
     *        other field
     * @return Returns a ChecklistItem with the correct dependencies marked
     */
    public CheckListItem hasDependency(ItemField fieldForUpdate, 
            ItemField fieldForMatchMask, ItemField fieldForUpdateMask) {
        return new CheckListItem( fieldForUpdate, getField(columnCode),false, fieldForUpdateMask, fieldForMatchMask);
    }
}
