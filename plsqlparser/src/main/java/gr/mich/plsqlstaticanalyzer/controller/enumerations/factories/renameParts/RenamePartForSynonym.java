package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.ItemGenerator;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPart;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;

/**
 *
 * @author Michael Michailidis
 */
public class RenamePartForSynonym extends AbstractRenamePart implements RenamePart {

    public RenamePartForSynonym() {
    }

    public UpgradedEnumMap synonymRenaming(UpgradedEnumMap otherValue) {
        SynonymPart synonymPart = ItemGenerator
                .getSynonymPart(
                        otherValue.
                        get(ItemField.ITEM_TYPE)
                        .toString()
                );

        return synonymPart.rename(otherValue, getMap());
    }
}
