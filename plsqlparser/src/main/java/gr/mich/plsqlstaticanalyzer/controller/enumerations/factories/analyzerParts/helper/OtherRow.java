package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.helper;

/**
 * Same as {@link FromRow} just smaller item holder. Contains only 2 Strings 
 * instead of 3
 * 
 * @author Michael Michailidis
 */
public class OtherRow {
    private final String itemName;
    private final String schemaName;

    /**
     * This item is only useful if both the variables are filled.. 
     * So no default constructor
     * 
     * @param itemName The ItemName
     * @param schemaName The SchemaName
     */
    public OtherRow(String itemName, String schemaName) {
        this.itemName = itemName;
        this.schemaName = schemaName;
    }

    /**
     * Constructor skip function to generate and get the item in one go!
     * @param itemName the ItemName
     * @param schemaName the SchemaName
     * @return Return a new ItemHolder item
     */
    public static OtherRow of (String itemName, String schemaName) {
        return new OtherRow(itemName, schemaName);
    }
    
    /**
     * The ItemName
     * 
     * @return Returns the ItemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * The SchemaName
     * 
     * @return Returns the SchemaName
     */
    public String getSchemaName() {
        return schemaName;
    }    
}
