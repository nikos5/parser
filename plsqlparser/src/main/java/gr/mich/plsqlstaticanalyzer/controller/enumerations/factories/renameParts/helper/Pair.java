package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.helper;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;

/**
 * A helper class for {@link gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePart RenamePart}
 * It contains the fields that need to be grouped together in the collections
 * 
 * @author Michael Michailidis
 */
public class Pair {
    private final String fieldForUpdateName;
    private final ItemField fieldForUpdate;
    private final String fieldForMatchName;
    private final ItemField fieldForMatch;
       
    public Pair(String fieldForUpdateName, ItemField fieldForUpdate, String fieldForMatchName, ItemField fieldForMatch ) {
        this.fieldForUpdateName = fieldForUpdateName;
        this.fieldForUpdate = fieldForUpdate;
        this.fieldForMatchName = fieldForMatchName;
        this.fieldForMatch = fieldForMatch;
    }    

    /**
     * The name of the field that will be renamed
     * @return Returns the name of the field that will be renamed
     */
    public String getFieldForUpdateName() {
        return fieldForUpdateName;
    }

    /**
     * The {@link ItemField} that will be renamed
     * @return Returns the {@link ItemField} that will be renamed
     */
    public ItemField getFieldForUpdate() {
        return fieldForUpdate;
    }    

    /**
     * The name of the field that will provide the value
     * @return Returns the name of the field that will provide the value
     */
    public String getFieldForMatchName() {
        return fieldForMatchName;
    }

    /**
     * The {@link ItemField} that will provide the value
     * @return Returns the {@link ItemField} that will provide the value
     */
    public ItemField getFieldForMatch() {
        return fieldForMatch;
    }    
}
