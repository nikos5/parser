package gr.mich.plsqlstaticanalyzer.controller.enumerations.source;

import gr.mich.plsqlstaticanalyzer.controller.ControlCore;

/**
 * The ItemLevel Enumeration that contains the Integer values
 * of each level that the {@link ControlCore} use to keep track of the parser
 * 
 * @author Michael Michailidis
 */
public enum ItemLevels {
    /**
     * Integer value : 0
     *//**
     * Integer value : 0
     */
    SCHEMA(0),
    /**
     * Integer value : 1
     */
    CONTAINER(1),
    /**
     * Integer value : 2
     */
    ITEM(2),
    /**
     * Integer value : 3
     */
    INNER(3),
    /**
     * Integer value : -1
     */
    OTHER(-1);
    
    private final int val;
    
    private ItemLevels(int i) {
        this.val = i;
    }
    
    /**
     * The integer value of the level
     * @return Returns the integer value of the level
     */
    public int getVal(){
        return val;
    }
    
    /**
     * The itemLevel finder
     * @param i the level Integer
     * @return The ItemLevel that was matched with the given Integer.
     *         If nothing was matched then it returns OTHER
     */
    public static ItemLevels getLevel(int i) {        
        for(ItemLevels vLookUp:values()) {
            if ( vLookUp.val == i ) {
                return vLookUp;
            }
        }
        
        return OTHER;    
    }
}
