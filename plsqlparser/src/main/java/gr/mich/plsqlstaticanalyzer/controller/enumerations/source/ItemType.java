package gr.mich.plsqlstaticanalyzer.controller.enumerations.source;

/**
 * The Enumeration that contains the information about each ItemType
 * with their levels and their ability to make calls to other items
 * 
 * @author Michael Michailidis
 */
public enum ItemType implements ItemData {
    PACKAGE(ItemLevels.CONTAINER.getVal(), false),
    FUNCTION(ItemLevels.ITEM.getVal(), false),
    PROCEDURE(ItemLevels.ITEM.getVal(), false),
    VIEW(ItemLevels.CONTAINER.getVal(), true), 
    MATERIALIZED_VIEW(ItemLevels.CONTAINER.getVal(), true),
    MATERIALIZED_VIEW_LOG(MATERIALIZED_VIEW.getLevel(), false), //same things. so same value
    TRIGGER(ItemLevels.CONTAINER.getVal(), true),
    SCHEMA(ItemLevels.SCHEMA.getVal(), false),
    CALL(ItemLevels.INNER.getVal(), true),
    SYNONYM(ItemLevels.CONTAINER.getVal(), false), //synonym is item?? 
    TABLE(ItemLevels.CONTAINER.getVal(), true),    
    LINK(ItemLevels.INNER.getVal(), true),
    UNKNOWN(ItemLevels.OTHER.getVal(), false); //Null is not a good policy    

    /**
     * Gets a string and converts it to an ItemType to be used in the code
     * 
     * @param str The String to be converted
     * @return Returns the converted ItemType from the String. If the String
     *         cannot be converted then it returns UNKNOWN
     */
    public static ItemType getType(String str) {
        for (ItemType vLookUp : values()) {
            if (vLookUp.toString().equalsIgnoreCase(str)) {
                return vLookUp;
            }
        }

        return UNKNOWN;
    }

    private final int val;
    private final boolean call;

    private ItemType(int i, boolean b) {
        this.val = i;
        this.call = b;
    }

    /**
     * The level of the ItemType
     * @return Returns the level of the ItemType
     */
    public int getLevel() {
        return val;
    }
    
    /**
     * The ability to make calls
     * @return Returns true if the Item can make calls. Else it returns false
     */
    public boolean canCall(){
        return call;
    }
}
