package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts;

import gr.mich.plsqlstaticanalyzer.analyzer.inner.AnalysisNode;
import gr.mich.plsqlstaticanalyzer.analyzer.inner.CounterNode;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.helper.FromRow;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Michael Michailidis
 */
public class AnalyzerPartForView implements AnalyzerPart {

    @Override
    public void exec(UpgradedEnumMap<ItemField, String> me,
            HashMap<Long, CounterNode> targets) {
        String[] fromNames = me.get(ItemField.FROM_CLAUSE_NAME).split(",");
        String[] fromSchema = me.get(ItemField.FROM_CLAUSE_SCHEMA).split(",");
        String[] fromLink = me.get(ItemField.FROM_CLAUSE_LINK).split(",");

        List<FromRow> fromRows = new ArrayList<>();

        for (int i = 0; i < fromNames.length; i++) {
            fromRows.add(
                    FromRow.of(
                            fromNames[i],
                            fromSchema[i],
                            fromLink[i]
                    )
            );
        }

        fromRows.stream()
                .forEach(fromRow -> {
                    targets.values().stream()
                            .filter(vL //calls are tricky with the names so they should be filtered out
                                    -> !vL.getItem()
                                    .get(ItemField.ITEM_TYPE)
                                    .equals(ItemType.CALL.toString())
                            ).forEach(vL -> {
                                UpgradedEnumMap<ItemField, String> item = vL.getItem();
                                if (item.containsKey(ItemField.ITEM_NAME)//validate that the key exists before trying to extract it
                                        && item.containsKey(ItemField.MY_SCHEMA_NAME)
                                        && !item.containsKey(ItemField.MY_PACKAGE_NAME)//it should not have package. if the name matches but a package exist it may be something else 
                                        && item.containsKey(ItemField.LINK_NAME)) {
                                    String myName = item.get(ItemField.ITEM_NAME);
                                    String mySchema = item.get(ItemField.MY_SCHEMA_NAME);
                                    String myLink = item.get(ItemField.LINK_NAME);//future attribute

                                    if (myName.equals(fromRow.getItemName())
                                            && mySchema.equals(fromRow.getSchemaName())) {
                                        vL.increaseCalls();
                                    }
                                     
                                    //Future attribute. count the links
                                    if (item.get(ItemField.ITEM_TYPE).equals(ItemType.LINK.toString())
                                            && myLink.equals(fromRow.getLinkName())) {
                                        vL.increaseCalls();
                                    }
                                }
                            });
                });
    }

    @Override
    public boolean inheritanceCheck(UpgradedEnumMap<ItemField, String> me, AnalysisNode target) {
        return false;
    }  
}
