package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.helper;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;

/**
 * A support class to assist the renaming part. Contains all the information 
 * on dependencies to make the renaming possible
 * 
 * @author Michael Michailidis
 */
public class CheckListItem {

    /**
     * Mask is used in case the "data" is stored in other field than what field
     * it should be matched on other items. Example : Package.
     *
     * The fields it use are ID,ITEM_TYPE,ITEM_NAME,MY_SCHEMA_NAME the value
     * that it will give is the value of MY_SCHEMA_NAME which is linked on
     * ITEM_NAME. But the other items do not know it as ITEM_NAME but as
     * MY_PACKAGE_NAME. So with the mask we will take the data from ITEM_NAME
     * and check them with the data on MY_PACKAGE_NAME
     *
     */
    private final ItemField fieldForUpdate;
    private final ItemField fieldForUpdateMask;
    private final boolean hasFieldForUpdateMask;

    private final ItemField fieldForMatch;
    private final ItemField fieldForMatchMask;
    private final boolean hasFieldForMatchMask;

    private final boolean isDependency;

    /**
     * Basic dependency builder
     *
     * @param fieldForMatch The field that will need its data to be updated
     * @param fieldForUpdate The filed that will need to be matched to activate
     * data update
     * @param isDependency Marks the item as a dependency or not
     */
    public CheckListItem(ItemField fieldForMatch, ItemField fieldForUpdate,
            boolean isDependency) {
        this(fieldForMatch, fieldForUpdate, isDependency, null, false, null, false);
    }

    /**
     * @param fieldForMatch The field that will need its data to be updated
     * @param fieldForUpdate The filed that will need to be matched to activate
     * data update
     * @param isDependency Marks the item as a dependency or not
     * @param fieldForMatchMask the field which will provide the data for
     * itemField to update
     */
    public CheckListItem(ItemField fieldForMatch, ItemField fieldForUpdate,
            boolean isDependency, ItemField fieldForMatchMask) {
        this(fieldForMatch, fieldForUpdate, isDependency, fieldForMatchMask,
                true, null, false);
    }

    /**
     * @param fieldForMatch The field that will need its data to be updated
     * @param fieldForUpdate The filed that will need to be matched to activate
     * data update
     * @param isDependency Marks the item as a dependency or not
     * @param fieldForMatchMask the field which will provide the data for
     * itemField to update
     * @param fieldForUpdateMask the field which will provide the data for
     * recognizerField to update
     */
    public CheckListItem(ItemField fieldForMatch, ItemField fieldForUpdate,
            boolean isDependency, ItemField fieldForMatchMask,
            ItemField fieldForUpdateMask) {
        this(fieldForMatch, fieldForUpdate, isDependency, fieldForMatchMask, true,
                fieldForUpdateMask, true);
    }

    /**
     *
     * @param fieldForMatch The field that will need its data to be updated
     * @param fieldForUpdate The filed that will need to be matched to activate
     * data update
     * @param isDependency Marks the item as a dependency or not
     * @param fieldForUpdateMask The mask for the field for update
     * @param hasFieldForUpdateMask True if the field for update contains a mask. 
     *        Else false
     * @param fieldForMatchMask The mask of the matching field
     * @param hasFieldForMatchMask True if the field of matching contains a mask.
     *        Else false
     */
    private CheckListItem(ItemField fieldForMatch, ItemField fieldForUpdate,
            boolean isDependency,
            ItemField fieldForMatchMask, boolean hasFieldForMatchMask,
            ItemField fieldForUpdateMask, boolean hasFieldForUpdateMask
    ) {

        this.fieldForUpdate = fieldForUpdate;
        this.fieldForMatch = fieldForMatch;
        this.isDependency = isDependency;
        this.fieldForUpdateMask = fieldForUpdateMask;
        this.hasFieldForUpdateMask = hasFieldForUpdateMask;
        this.fieldForMatchMask = fieldForMatchMask;
        this.hasFieldForMatchMask = hasFieldForMatchMask;
    }

    /**
     * The field that will have its value replaced
     *
     * @return Returns the field which will have its value replaced
     */
    public ItemField getFieldForMatch() {
        return fieldForMatch;
    }

    /**
     * The field which will be used to match which value will be replaced
     *
     * @return Returns the field which will have provide the matching value
     */
    public ItemField getFieldForUpdate() {
        return fieldForUpdate;
    }

    /**
     * The item state
     * 
     * @return returns true if the current item is marked as a dependency for 
     *         an other item
     */
    public boolean isDependency() {
        return this.isDependency;
    }

    /**
     * Dependency checker
     * @param field the {@link ItemField} to check
     * @return Returns true if the given {@link ItemField} is a dependency for the 
     *         current item
     */
    public boolean hasDependency(ItemField field) {
        return !isDependency && hasFieldForUpdateMask
                ? field.equals(fieldForUpdateMask)
                : field.equals(fieldForUpdate);
    }

    /**
     * The mask for the field which has the dependency on
     * @return Returns the mask of the field which has the dependency on
     */
    public ItemField getFieldForUpdateMask() {
        return fieldForUpdateMask;
    }

    /**
     * Mask existence checker
     * @return Returns true if the field that has a dependency on is masked
     */
    public boolean hasFieldOfUpdateMask() {
        return hasFieldForUpdateMask;
    }

    /**
     * The mask for the field which the dependency is on.
     * @return Returns the mask on which field the dependency is on
     */
    public ItemField getFieldForMatchMask() {
        return fieldForMatchMask;
    }

    /**
     * Mask existence checker
     * @return Returns true if the field in which the dependency is on is masked
     */
    public boolean hasFieldForMatchMask() {
        return hasFieldForMatchMask;
    }
    
    /**
     * Mask checker
     * @return Returns true if the item has a mask 
     */
    public boolean hasMasks(){
        return hasFieldForUpdateMask || hasFieldForMatchMask;
    }
}
