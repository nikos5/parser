package gr.mich.plsqlstaticanalyzer.controller;

import gr.mich.Application;
import gr.mich.plsqlstaticanalyzer.analyzer.AnalysisCore;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.ItemGenerator;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForSynonym;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemLevels;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import gr.mich.plsqlstaticanalyzer.controller.helper.TwoArgPair;
import gr.mich.plsqlstaticanalyzer.controller.loggerlvl.CustomLevels;
import gr.mich.plsqlstaticanalyzer.exceptions.ExceptionCore;
import gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions.StrangeItemTypeException;
import gr.mich.plsqlstaticanalyzer.output.OutputCore;
import org.netbeans.core.startup.logging.NbFormatter;

import java.io.IOException;
import java.util.*;
import java.util.logging.*;
import java.util.stream.Collectors;

/**
 * The Core of the project. It handles all the other cores of the project 
 * ({@link AnalysisCore} , {@link ExceptionCore}, {@link OutputCore} ) and the whole
 * dataflow in the project.  It speaks directly with the grammar which extracts the
 * fields and create nodes using this class functions 
 * 
 * @author Michael Michailidis
 */
public final class ControlCore {

    //<editor-fold defaultstate="collapsed" desc="private fields">
    private static final Logger LOG = Logger.getGlobal();

    private final HashMap<Integer, String> currentNames;

    private final HashMap<Long, UpgradedEnumMap> allItems;
    private final HashMap<Integer, UpgradedEnumMap> currentItems;
    private final HashMap<Integer, Boolean> itemFlags;
    private final HashMap<Integer, ItemField> itemFields;

    private final UpgradedEnumMap schemaDefault;
    private final UpgradedEnumMap packageDefault;
    private final UpgradedEnumMap itemDefault;
    private final UpgradedEnumMap innerDefault;

    private int currentLevel;
    private int currentSubLevel;

    private final int SUB_LEVEL_INITIAL_VALUE = 10;

    private Long currentID;

    private final List<String> reDistributeItems;

    private final AnalysisCore AC;

    //</editor-fold>
    /**
     * Default constructor with all the required initialize for the class to 
     * operate
     */
    public ControlCore() {
        try {
            FileHandler fileHandlerAll = new FileHandler("logAll.log");
            fileHandlerAll.setLevel(Level.ALL);

            FileHandler fileHandlerFinallize = new FileHandler("logFinallize.log");
            fileHandlerFinallize.setLevel(CustomLevels.FINALLIZE);
            fileHandlerFinallize.setFormatter(new SimpleFormatter());

            FileHandler fileHandlerRename = new FileHandler("RenameLog.log");
            fileHandlerRename.setFormatter(new NbFormatter());
            fileHandlerRename.setFilter((LogRecord lr) -> lr.getLevel().equals(CustomLevels.ITEM_RENAME_MODE));

            LOG.addHandler(fileHandlerRename);
            LOG.addHandler(fileHandlerAll);
            LOG.addHandler(fileHandlerFinallize);
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }

//        LOG.setLevel(CustomLevels.FINALLIZE);
        LOG.log(CustomLevels.ALL, "ControllerCore init");

        this.currentID = -1L;
        this.currentLevel = 0;
        this.currentSubLevel = SUB_LEVEL_INITIAL_VALUE;

        this.currentNames = new HashMap<>();

        this.allItems = new HashMap<>();
        this.currentItems = new HashMap<>();
        this.itemFlags = new HashMap<>();
        this.itemFields = new HashMap<>();

        this.schemaDefault = ItemGenerator.SCHEMA.getMap(nextID());
        this.schemaDefault.put(ItemField.ITEM_NAME, ItemField.MY_SCHEMA_NAME.getDefault());

        this.packageDefault = ItemGenerator.PACKAGE.getMap(nextID());
        this.packageDefault.put(ItemField.ITEM_NAME, ItemField.MY_PACKAGE_NAME.getDefault());
        this.packageDefault.put(ItemField.MY_SCHEMA_NAME, ItemField.MY_SCHEMA_NAME.getDefault());

        this.itemDefault = ItemGenerator.FUNCTION.getMap(nextID());
        this.itemDefault.put(ItemField.ITEM_NAME, ItemField.ITEM_NAME.getDefault());
        this.itemDefault.put(ItemField.MY_SCHEMA_NAME, ItemField.MY_SCHEMA_NAME.getDefault());
        this.itemDefault.put(ItemField.MY_PACKAGE_NAME, ItemField.MY_PACKAGE_NAME.getDefault());

        this.innerDefault = ItemGenerator.CALL.getMap(nextID());
        this.innerDefault.put(ItemField.ITEM_NAME, ItemField.ITEM_NAME.getDefault());
        this.innerDefault.put(ItemField.MY_PACKAGE_NAME, ItemField.MY_PACKAGE_NAME.getDefault());
        this.innerDefault.put(ItemField.MY_SCHEMA_NAME, ItemField.MY_SCHEMA_NAME.getDefault());
        this.innerDefault.put(ItemField.OTHER_ITEM_NAME, ItemField.OTHER_ITEM_NAME.getDefault());
        this.innerDefault.put(ItemField.OTHER_PACKAGE_NAME, ItemField.OTHER_PACKAGE_NAME.getDefault());
        this.innerDefault.put(ItemField.OTHER_SCHEMA_NAME, ItemField.OTHER_SCHEMA_NAME.getDefault());
        this.innerDefault.put(ItemField.LINK_NAME, ItemField.LINK_NAME.getDefault());

        this.reDistributeItems = new ArrayList<>();

        this.resetItemName();
        this.resetPackageName();
        this.resetSchemaName();

        this.AC = new AnalysisCore();
//        this.OC = new OutputCore(new TreeToTxtFunction(DefaultProperties.INSTANCE));
//        this.OC = new OutputCore(new TreeToTxtFunction());
    }

    /**
     * Initializes the analysis part of the program
     */
    public void executeAnalysis() {
        AC.initiateAnalyze(allItems);
    }
    
    /**
     * Name inheritance on the items Renaming synonyms Removing Synonyms from
     * the list
     */
    public void optimize() {

        initializeSynonymRename();

        initializeSchemaGenerator();

        initializeNoPackage();

        initializeLinkGenerator();
        // Optimize should apply in all the items 
//        initializeRename(1);
        initializeRename();
    }

    //<editor-fold defaultstate="collapsed" desc="Initializers">
    
    /**
     * Generates a package for the PackageLess items that were found
     */
    private void initializeNoPackage() {
        List<TwoArgPair> packages = new ArrayList<>();

        allItems
                .values()
                .stream()
                .forEach(
                        (value) -> {
                            if (value.containsKey(ItemField.MY_PACKAGE_NAME)) {
                                String packageName = value.get(ItemField.MY_PACKAGE_NAME).toString();
                                if (packageName.equals(ItemField.MY_PACKAGE_NAME.getDefault())) {

                                    TwoArgPair tap = new TwoArgPair(
                                            value.get(ItemField.MY_PACKAGE_NAME).toString(),
                                            value.get(ItemField.MY_SCHEMA_NAME).toString()
                                    );

                                    if (!packages.contains(tap)) {
                                        packages.add(tap);
                                    }
                                }
                            }
                        }
                );

        packages
                .stream()
                .forEach(
                        (value) -> {
                            UpgradedEnumMap newPackage = ItemGenerator.PACKAGE.getMap(nextID());
                            newPackage.put(ItemField.ITEM_NAME, value.getPackageName());
                            newPackage.put(ItemField.MY_SCHEMA_NAME, value.getSchemaName());

                            allItems.put(Long.valueOf(newPackage.get(ItemField.ID).toString()), newPackage);
                        }
                );

    }

    /**
     * Generates Link items if they are missing. It reads each node and checks 
     * if the {@link ItemField} Link exists and if it was already created before 
     * adding it to the ItemList
     */
    private void initializeLinkGenerator(){
        List<String> linkNames = new ArrayList<>();
        
        allItems.values()
                .stream()
                .forEach(value -> {
                    Optional.ofNullable(value.get(ItemField.LINK_NAME))
                            .ifPresent(schema -> {
                                Arrays.asList(schema.toString().split(","))
                                        .stream()
                                        .filter(item -> {
                                            if (item.isEmpty() || "".equals(item)) {
                                                return false;
                                            }
                                            //Missed this.. created empty link node
                                            if( item.equals(ItemField.LINK_NAME.getDefault()) ) {
                                                return false;
                                            }
                                            
                                            return !linkNames.contains(item);
                                        })
                                        .forEach(item -> linkNames.add(item));
                            });
                    
                    Optional.ofNullable(value.get(ItemField.FROM_CLAUSE_LINK))
                            .ifPresent(schema -> {
                                Arrays.asList(schema.toString().split(","))
                                        .stream()
                                        .filter(item -> {
                                            if (item.isEmpty() || "".equals(item)) {
                                                return false;
                                            }
                                            
                                            if( item.equals(ItemField.FROM_CLAUSE_LINK.getDefault()) ) {
                                                return false;
                                            }
                                            
                                            return !linkNames.contains(item);
                                        })
                                        .forEach(item -> linkNames.add(item));
                            });
                    
                    Optional.ofNullable(value.get(ItemField.EXTRA_LINK_NAME))
                            .ifPresent(schema -> {
                                Arrays.asList(schema.toString().split(","))
                                        .stream()
                                        .filter(item -> {
                                            if (item.isEmpty() || "".equals(item)) {
                                                return false;
                                            }
                                            
                                            if( item.equals(ItemField.EXTRA_LINK_NAME.getDefault()) ) {
                                                return false;
                                            }
                                            
                                            return !linkNames.contains(item);
                                        })
                                        .forEach(item -> linkNames.add(item));
                            });
                });
        
        
        linkNames
                .stream()
                .forEach(
                        (value) -> {
                            UpgradedEnumMap newLink = ItemGenerator.LINK.getMap(nextID());
                            newLink.put(ItemField.ITEM_NAME, value);

                            allItems.put(Long.valueOf(newLink.get(ItemField.ID).toString()), newLink);
                        }
                );
    }
    
    /**
     * Generates the schema items by reading all the nodes and extracting their 
     * schema values. If the schema name was already generated it ignores it and 
     * moves on
     */
    private void initializeSchemaGenerator() {
        List<String> schemaNames = new ArrayList<>();

        allItems
                .values()
                .stream()
                .forEach(
                        (value) -> {
                            Optional.ofNullable(value.get(ItemField.MY_SCHEMA_NAME))
                            .ifPresent(schema -> {
                                Arrays.asList(schema.toString().split(","))
                                        .stream()
                                        .filter(item -> {
                                            if (item.isEmpty() || "".equals(item)) {
                                                return false;
                                            }
                                            return !schemaNames.contains(item);
                                        })
                                        .forEach(item -> schemaNames.add(item));
                            });

                            Optional.ofNullable(value.get(ItemField.OTHER_SCHEMA_NAME))
                            .ifPresent(schema -> {
                                Arrays.asList(schema.toString().split(","))
                                        .stream()
                                        .filter(item -> {
                                            if (item.isEmpty() || "".equals(item)) {
                                                return false;
                                            }
                                            
                                            if( item.equals(ItemField.OTHER_SCHEMA_NAME.getDefault()) ) {
                                                return false;
                                            }
                                            
                                            return !schemaNames.contains(item);
                                        })
                                        .forEach(item -> schemaNames.add(item));
                            });

                            Optional.ofNullable(value.get(ItemField.FROM_CLAUSE_SCHEMA))
                            .ifPresent(schema -> {
                                Arrays.asList(schema.toString().split(","))
                                        .stream()
                                        .filter(item -> {
                                            if (item.isEmpty() || "".equals(item)) {
                                                return false;
                                            }
                                            
                                            if( item.equals(ItemField.FROM_CLAUSE_SCHEMA.getDefault()) ) {
                                                return false;
                                            }
                                            
                                            return !schemaNames.contains(item);
                                        })
                                        .forEach(item -> schemaNames.add(item));
                            });

                            Optional.ofNullable(value.get(ItemField.EXTRA_SCHEMA_NAME))
                            .ifPresent(schema -> {
                                Arrays.asList(schema.toString().split(","))
                                        .stream()
                                        .filter(item -> {
                                            if (item.isEmpty() || "".equals(item)) {
                                                return false;
                                            }
                                            
                                            if( item.equals(ItemField.EXTRA_SCHEMA_NAME.getDefault()) ) {
                                                return false;
                                            }
                                            
                                            return !schemaNames.contains(item);
                                        })
                                        .forEach(item -> schemaNames.add(item));
                            });
                        }
                );

        schemaNames
                .stream()
                .forEach(
                        (value) -> {
                            UpgradedEnumMap newSchema = ItemGenerator.SCHEMA.getMap(nextID());
                            newSchema.put(ItemField.ITEM_NAME, value);

                            allItems.put(Long.valueOf(newSchema.get(ItemField.ID).toString()), newSchema);
                        }
                );
    }

    /**
     * The rename part of the nodes. 
     * Some nodes have names that are wrong because they couldn't be retrieved
     * because they had more than 1 level difference. For example a function can't
     * get the schema name because it is not direct descendant but there is a package
     * between. The rename function will inherit the packages schema to the function 
     */
    private void initializeRename() {
        //rework the whole rename idea

        allItems.values()
                .stream()
                .filter((value) -> {
                    return value.get(ItemField.ITEM_TYPE).equals(ItemType.PACKAGE.toString());
                }).forEach((packageItem) -> {
            String packageName = packageItem.get(ItemField.ITEM_NAME).toString();
            String packageSchema = packageItem.get(ItemField.MY_SCHEMA_NAME).toString();

            allItems.values()
                    .stream()
                    .filter(item -> !item.get(ItemField.ITEM_TYPE).equals(ItemType.PACKAGE.toString()))
                    .filter(item -> !item.get(ItemField.ITEM_TYPE).equals(ItemType.SCHEMA.toString()))
                    .forEach(item -> {
                        Optional.ofNullable(item.get(ItemField.MY_PACKAGE_NAME))
                                .filter(subItem -> subItem.toString().equals(packageName))
                                .ifPresent(subItem -> {
                                    LOG.log(CustomLevels.ITEM_RENAME_MODE, "Renaming "
                                            + "from : {0}", new String[]{
                                                item.toSerializedString()
                                            });
                                    item.replace(ItemField.MY_SCHEMA_NAME, packageSchema);
                                    LOG.log(CustomLevels.ITEM_RENAME_MODE, "Renamed "
                                            + "to    : {0}", new String[]{
                                                item.toSerializedString()
                                            });
                                });

                        Optional.ofNullable(item.get(ItemField.OTHER_PACKAGE_NAME))
                                .filter(subItem -> subItem.toString().equals(packageName))
                                .ifPresent(subItem -> {
                                    LOG.log(CustomLevels.ITEM_RENAME_MODE, "Renaming "
                                            + "from : {0}", new String[]{
                                                item.toSerializedString()
                                            });
                                    item.replace(ItemField.OTHER_SCHEMA_NAME, packageSchema);
                                    LOG.log(CustomLevels.ITEM_RENAME_MODE, "Renamed "
                                            + "to    : {0}", new String[]{
                                                item.toSerializedString()
                                            });
                                });
                    });
        });
    }

    /**
     * Initialize the renaming of the synonyms in the project.
     * That way there wont be more than 1 item for 1 node.
     */
    private void initializeSynonymRename() {
        //Synonym renaming
        allItems
                .values()
                .stream()
                .filter((w) -> w.get(ItemField.ITEM_TYPE) //first we filter only the synonym items
                        .equals(ItemType.SYNONYM.toString()))
                .map((vLookUp) -> { // next we retrieve a factory stream in which every
                    //factory contains a map of each synonym
                    RenamePartForSynonym currentFactory = ((RenamePartForSynonym) ItemGenerator
                            .getRenamePart(
                                    vLookUp.get(
                                            ItemField.ITEM_TYPE
                                    ).toString()
                            ));
                    currentFactory.setMap(vLookUp);
                    return currentFactory;
                }).forEach((currentFactory) -> { //for each factory
            allItems
                    .values()
                    .stream() //we stream the items contained in the map
                    .filter((w) -> w.get(ItemField.ITEM_TYPE) // filtering only the synonyms 
                            .equals(ItemType.SYNONYM.toString()) // ignoring the rest
                            && !currentFactory
                            .idCheck( // and ignoring the same items ( according to id )
                                    Long.valueOf(
                                            w.get(ItemField.ID)
                                            .toString()
                                    )
                            )
                    )
                    .forEach(value -> { //so for each item ( Synonym ) left in the stream
                        UpgradedEnumMap synonymRenaming = currentFactory
                                .synonymRenaming(value); // we apply factories synonym rename

                        value.updateList(synonymRenaming); // and in the end we update the list
                        //with the new synonym values
                    });
        });

        //we renamed all the synonyms first so we will "gain" time on renaming the items. 
        //because there may be synonyms for synonyms and we will need more time to rename everything
        //so! root synonyms only
        //so now that we have only root synonyms we create again a stream of synonyms
        allItems
                .values()
                .stream()
                .filter((w) -> w.get(ItemField.ITEM_TYPE)
                        .equals(ItemType.SYNONYM.toString()))
                .map((vLookUp) -> {
                    RenamePartForSynonym currentFactory = ((RenamePartForSynonym) ItemGenerator
                            .getRenamePart(
                                    vLookUp.get(
                                            ItemField.ITEM_TYPE
                                    ).toString()
                            ));
                    currentFactory.setMap(vLookUp);
                    return currentFactory; // and we convert it in a stream of factories
                }).forEach((currentFactory) -> {
            allItems
                    .values() //next we give to each factory all the items 
                    .stream() //we dont use parallelSream because it makes a mess and cant be controlled
                    .filter((w) -> !w.get(ItemField.ITEM_TYPE)
                            .equals(ItemType.SYNONYM.toString()) // we ignore the synonym items cause they are roots
                            && !currentFactory
                            .idCheck( // and not sure but just in case we exclude same ids also
                                    Long.valueOf(
                                            w.get(ItemField.ID)
                                            .toString()
                                    )
                            )
                    ).forEach((value) -> { //in the end for each item left we apply renaming 
                        UpgradedEnumMap synonymRenaming = currentFactory
                                .synonymRenaming(value); //from the renaming module each factory contains

                        value.updateList(synonymRenaming); // and we update the list
                    });
        });

        //now that the synonyms are all replaced we no longer need them
        //we create a temporary list that will contain all the synonyms 
        //so we will delete them
        List<Long> tmpListWithItemsToDelete = new ArrayList<>();

        //we stream the items and filter only the synonyms to add them in the list
        //parallel for speed again cause there is not a chance for conflict
        //we just need their keys and nothing more
        allItems
                .keySet()
                .stream()
                .filter(
                        (value)
                        -> allItems
                        .get(value)
                        .get(ItemField.ITEM_TYPE)
                        .equals(ItemType.SYNONYM.toString()))
                .forEach((value)
                        -> tmpListWithItemsToDelete.add(value)
                );

        //and we remove all the keys that we found that are assosiated with the 
        //synonyms.. done!
        allItems
                .keySet()
                .removeAll(tmpListWithItemsToDelete);

        //empty the list cause its useless now
        tmpListWithItemsToDelete.clear();
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Level manager">
    
    /**
     * The level in which the grammar is reading at the given time.
     * It is useless after the grammar finish reading
     * 
     * @return Returns the current level in which the grammar is reading at the moment
     */
    private int getLevel() {
        if (this.currentSubLevel == SUB_LEVEL_INITIAL_VALUE) {
            return currentLevel;
        } else {
            return currentLevel + currentSubLevel;
        }
    }

    /**
     * The SubLevel of the current level. 
     * It is useless after the grammar finish reading
     * 
     * @return returns the SubLevel of the current level
     */
    private int getSubLevel() {
        return currentSubLevel;
    }

    /**
     * Whenever the grammar moves on to new element a new level should
     * be setup to let the ControlCore know where it should add the data
     * It is useless after the grammar finish reading
     * 
     * @param i the current level the Grammar is starting to read
     */
    private void setLevel(int i) {
        if (getLevel() == i) {
            increaseSubLevel();
        } else if (i == 3) {
            currentLevel++;
        } else if (i == 2 && getLevel() == 0) {
            currentLevel++;
        } else {
            currentLevel = i;
        }
    }

    /**
     * When the grammar finish reading an element the level should be reduced
     * This function decides if the level that will be reduced is primary or sublevel
     * It is useless after the grammar finish reading 
     */
    private void decreaseLevel() {
        if (getSubLevel() > SUB_LEVEL_INITIAL_VALUE) {
            decreaseSubLevel();
        } else {
            setLevel(getLevel() - 1);
        }
    }

    /**
     * When the primary level should stay at the same then the SubLevel will be increased
     * It is useless after the grammar finish reading
     */
    private void increaseSubLevel() {
        this.currentSubLevel++;
    }

    /**
     * When the SubLevel needs a decrease this function will do the correct adjustment
     * It is useless after the grammar finish reading
     */
    private void decreaseSubLevel() {
        this.currentSubLevel--;

        if (currentSubLevel == SUB_LEVEL_INITIAL_VALUE) {
            currentItems.forEach(
                    (k, v) -> {
                        //0,1,2,3,4 are possible values that must not be deleted
                        if (!Arrays.asList(0, 1, 2, 3, 4).contains(k)) {
                            currentItems.remove(k);
                        }
                    }
            );
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="others">
    /**
     * The id generator to keep track of the inserted nodes
     * It is useless after the grammar finish reading
     * 
     * @return Returns the next id in line that should be used for the new item
     */
    private Long nextID() {
        this.currentID = currentID + 1L;
        return currentID;
    }

    /**
     * Sets the name of the item in each level which the parser is reading
     * It is useless after the grammar finish reading
     * 
     * @param itemType The type of the item that is being parsed. It contains the level also
     * @param itemName The name of the item that is being parsed.
     */
    private void setPrivateFields(ItemType itemType, String itemName) {
        LOG.log(CustomLevels.REGISTERING_PRIVATE_FIELDS, "Field {0} value will change to : {1}",
                new String[]{
                    itemType.toString(),
                    itemName
                }
        );

        currentNames.put(itemType.getLevel(), itemName);
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="factories / finallizer">
    /**
     * Parser purpose only. 
     * This function should be called when the parser reach a terminal symbol of a rule
     * which encapsulates an item
     */
    public void finalizeV() {
        LOG.log(CustomLevels.FINALLIZE, "\nFinallized {0}\n", currentItems.get(getLevel()).toString());

        if (!currentItems.get(getLevel()).isValid()) {
            ExceptionCore.throwNew(
                    new StrangeItemTypeException(
                            currentItems
                            .get(getLevel()).toString()
                    )
            );
            return;
        }

        allItems.put(
                Long.valueOf(
                        currentItems
                        .get(getLevel())
                        .get(ItemField.ID)
                        .toString()),
                currentItems.get(getLevel()));

        this.resetCurrentItemFields();
        this.resetAppropriateField();
    }

    /**
     * Initial version ItemType should be the first to come to "initialize" the
     * levels
     * Parser purpose only. 
     *
     * @param itemType The {@link ItemType} which will be generated to add fields
     */
    public void factory(ItemType itemType) {
        LOG.log(CustomLevels.ITEM_SET, "Setting itemType : {0}}",
                new String[]{
                    itemType.toString()
                }
        );

        //retrieve the level from the type       
        setLevel(itemType.getLevel());

        //initialize the Container
        currentItems.put(getLevel(), ItemGenerator.getType(itemType.toString()).getMap(nextID()));
    }

    /**
     * use this factory after the initial to add content
     * Parser purpose only. 
     *
     * @param itemField The {@link ItemField} in which the data will be saved
     * @param itemData The data that will be saved
     */
    public void factory(ItemField itemField, String itemData) {
        LOG.log(CustomLevels.ITEM_SET, "Setting {0} with value {1}",
                new String[]{
                    itemField.toString(),
                    itemData
                }
        );

        currentItems.get(getLevel()).put(itemField, itemData);

        if (itemField.equals(ItemField.ITEM_NAME)) {
            setPrivateFields(
                    ItemType.getType(
                            currentItems
                            .get(getLevel()) //extract the item currently proccessing from the list
                            .get(ItemField.ITEM_TYPE)
                            .toString()),
                    itemData);
        }
    }

    /**
     * this factory will try to append data into the item instead of overriding
     * them append needs rework
     * Parser purpose only. 
     *
     * @param itemField The {@link ItemField} in which the data will be saved
     * @param itemData The data that will be saved
     * @param append True if the data should be appended else false to be overridden
     */
    public void factory(ItemField itemField, String itemData, Boolean append) {
        LOG.log(CustomLevels.ITEM_APPEND, "Testing append for {0} with value {1}",
                new String[]{
                    itemField.toString(),
                    itemData
                }
        );

        //If append is false normal factory will be called and the job will end
        if (!append) {
            factory(itemField, itemData);
            return;
        }

        boolean isAllowed = false;

        //Prevending exception by checking if the key exists and then converting its value
        if (currentItems.get(getLevel()).containsKey(ItemField.APPENDABLE)) {
            isAllowed = Boolean.valueOf(
                    (String) currentItems
                    .get(getLevel())
                    .get(ItemField.APPENDABLE)
            );
        }

        //If append is allowed then the data will be appended        
        if (isAllowed) {
            /*            
                The "turn" starts here checking if the append flag was activated
                and if the activated flag was from the same field
             */
            if (itemFlags.get(getLevel()) != null
                    && //if the flag is not null which means append was never checked if is active
                    itemFlags.get(getLevel())
                    && //if the flag is not false which means append is not active
                    itemFields.get(getLevel()).equals(itemField)) { //if the flagField is the same field with the incoming item

                String tmp = (String) currentItems.get(getLevel()).get(itemField);
                String[] splittedString = tmp.split(",");

                if (splittedString.length == 1) {
                    tmp = null;
                } else {
                    tmp = splittedString[0];
                    for (int i = 1; i < splittedString.length - 1; i++) {
                        tmp = tmp + "," + splittedString[i];
                    }
                }

                currentItems.get(getLevel()).put(itemField, tmp);
                itemFlags.put(getLevel(), Boolean.FALSE);
            } else {
                //if the flag wasnt active or the field wasnt the same cancel the override
                itemFlags.put(getLevel(), Boolean.FALSE);
                itemFields.put(getLevel(), null);
            }

            //if the default value comes in. check at the next turn if its replaced
            for (ItemField vLookUp : ItemField.values()) {
                if (vLookUp.equals(ItemField.ID) || vLookUp.equals(ItemField.ITEM_TYPE)) {
                    continue;
                }

                if (vLookUp.getDefault().equals(itemData)) {
                    itemFlags.put(getLevel(), true);
                    itemFields.put(getLevel(), vLookUp);
                    break;
                }
            }

            /*
                Check if the key already exist -> the key should be
                initialized by the generator with null value. If its not 
                initialized the data will be descarded by the generator itself
             */
            if (currentItems
                    .get(getLevel()).containsKey(itemField)
                    && currentItems //Check if the key already has value
                    .get(getLevel())
                    .get(itemField) != null
                    && !currentItems //and if the string is not empty
                    .get(getLevel())
                    .get(itemField).toString().isEmpty()) {

                String tmp = (String) currentItems.get(getLevel()).get(itemField);
                currentItems.get(getLevel()).put(itemField, tmp + "," + itemData);
            } else {
                currentItems.get(getLevel()).put(itemField, itemData);
            }
        }
        //Else the data will be discarded

    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="item Distribution">
    /**
     * The items that are not placed in a package or schema because its not created yet.
     * Those items will be redistributed after the parsing is over
     * It is useless after the grammar finish reading
     * 
     * @param str The item in {@link String} format which will be reDestributed
     */
    public void registerItemForDistribution(String str) {
        reDistributeItems.add(str);
    }

    /**
     * Example DBMS_OUTPUT.PUT_LINE is plsql function call.. it should be
     * ignored or just counted as plsql function
     * It is useless after the grammar finish reading
     */
    public void distributeItems() {
        if (reDistributeItems.size() > 3 || reDistributeItems.size() < 1) {
            ExceptionCore.throwNew(
                    new StrangeItemTypeException(
                            "Items expected to be distributed are more than 3 or"
                            + "less than 1. Items : " + reDistributeItems.size()
                    )
            );
            return;
        }

        switch (reDistributeItems.size()) {
            case 1:
                factory(ItemField.OTHER_ITEM_NAME, reDistributeItems.get(0));
                factory(ItemField.OTHER_PACKAGE_NAME, getPackageName());
                factory(ItemField.OTHER_SCHEMA_NAME, getSchemaName());
                break;
            case 2:
                factory(ItemField.OTHER_ITEM_NAME, reDistributeItems.get(1));
                factory(ItemField.OTHER_PACKAGE_NAME, reDistributeItems.get(0));
                factory(ItemField.OTHER_SCHEMA_NAME, getSchemaName());
                break;
            default:
                factory(ItemField.OTHER_ITEM_NAME, reDistributeItems.get(2));
                factory(ItemField.OTHER_PACKAGE_NAME, reDistributeItems.get(1));
                factory(ItemField.OTHER_SCHEMA_NAME, reDistributeItems.get(0));
                break;
        }

        reDistributeItems.clear();
    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="validators">
    /**
     * Validates that all the names changed from the default value
     * @return True only if all the names are different from the default value
     * if at least 1 name has the default value it will return false
     */
    public boolean validateNames() {
        List<UpgradedEnumMap> collect = allItems
                .values()
                .stream()
                .filter((value) -> {
                    return value.get(ItemField.ITEM_NAME).toString().equals(ItemField.ITEM_NAME.getDefault());
                })
                .collect(
                        Collectors.toList()
                );

        return collect.isEmpty();
    }

    /**
     * Validates the calls between the nodes
     */
    public void validateCall() {
        UpgradedEnumMap map = currentItems.get(getLevel());

        if (map.get(ItemField.ITEM_NAME).equals(ItemField.ITEM_NAME.getDefault())) {
            map.put(ItemField.ITEM_NAME, map.get(ItemField.MY_PACKAGE_NAME).toString());
            map.put(ItemField.MY_PACKAGE_NAME, ItemField.MY_PACKAGE_NAME.getDefault());
        }
    }

    //</editor-fold> 
    // <editor-fold defaultstate="collapsed" desc="getters / reseters">
    /**
     * The current schema name
     * @return Returns the current Schema name
     */
    public String getSchemaName() {
        return currentNames.get(ItemLevels.SCHEMA.getVal());
    }

    /**
     * the current package name
     * @return Returns the current package name
     */
    public String getPackageName() {
        return currentNames.get(ItemLevels.CONTAINER.getVal());
    }

    /**
     * the current item name
     * @return Returns the current item name
     */
    public String getItemName() {
        return currentNames.get(ItemLevels.ITEM.getVal());
    }

    /**
     * Resets the current schema name to the default value
     */
    public void resetSchemaName() {
        currentItems.put(ItemLevels.SCHEMA.getVal(), schemaDefault);
        itemFlags.put(ItemLevels.SCHEMA.getVal(), Boolean.FALSE);
        itemFields.put(ItemLevels.SCHEMA.getVal(), null);
        currentNames.put(ItemLevels.SCHEMA.getVal(), ItemField.MY_SCHEMA_NAME.getDefault());
    }

    /**
     * Resets the current package name to the default value
     */
    public void resetPackageName() {
        currentItems.put(ItemLevels.CONTAINER.getVal(), packageDefault);
        itemFlags.put(ItemLevels.CONTAINER.getVal(), Boolean.FALSE);
        itemFields.put(ItemLevels.CONTAINER.getVal(), null);
        currentNames.put(ItemLevels.CONTAINER.getVal(), ItemField.MY_PACKAGE_NAME.getDefault());
    }

    /**
     * Resets the current item name to the default value
     */
    public void resetItemName() {
        currentNames.put(ItemLevels.ITEM.getVal(), ItemField.ITEM_NAME.getDefault());
        itemFlags.put(ItemLevels.ITEM.getVal(), Boolean.FALSE);
        itemFields.put(ItemLevels.ITEM.getVal(), null);
    }

    /**
     * Resets the current itemFields according to the current Level
     */
    private void resetCurrentItemFields() {
        switch (ItemLevels.getLevel(getLevel())) {
            case SCHEMA:
                currentItems.put(getLevel(), schemaDefault);
                break;
            case CONTAINER:
                currentItems.put(getLevel(), packageDefault);
                break;
            case ITEM:
                currentItems.put(getLevel(), itemDefault);
                break;
            case INNER:
                currentItems.put(getLevel(), innerDefault);
                break;
        }
    }

    /**
     * Resets the current field according to the current level
     */
    private void resetAppropriateField() {
        LOG.log(CustomLevels.RESETTING_PRIVATE_FIELDS, "Reseting {0} level",
                new int[]{
                    getLevel()
                }
        );

        switch (ItemLevels.getLevel(getLevel())) {
            case SCHEMA:
                resetSchemaName();
                break;
            case CONTAINER:
                resetPackageName();
                decreaseLevel();
                break;
            case ITEM:
                resetItemName();
                decreaseLevel();
                break;
            case INNER:
                decreaseLevel();
                break;
        }
    }

    /**
     * The logger of the project
     * @return Returns the logger of the project
     */
    public static Logger getLog() {
        return LOG;
    }

    /**
     * Resets the logger
     */
    public static void resetLoger() {
        for (Handler vLookUp : LOG.getHandlers()) {
            LOG.removeHandler(vLookUp);
        }
    }

    /**
     * Registers a handler to the logger
     * @param h The handler to be registered
     */
    public static void registerHandler(Handler h) {
        LOG.addHandler(h);
    }
    // </editor-fold>   
}
