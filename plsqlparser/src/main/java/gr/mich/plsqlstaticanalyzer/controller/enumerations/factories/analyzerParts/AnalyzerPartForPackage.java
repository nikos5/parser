package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts;

import gr.mich.plsqlstaticanalyzer.analyzer.inner.AnalysisNode;
import gr.mich.plsqlstaticanalyzer.analyzer.inner.CounterNode;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.HashMap;

/**
 *
 * @author Michael Michailidis
 */
public class AnalyzerPartForPackage implements AnalyzerPart {

    @Override
    public void exec(UpgradedEnumMap<ItemField, String> me,
            HashMap<Long, CounterNode> targets) {
    }

    @Override
    public boolean inheritanceCheck(UpgradedEnumMap<ItemField, String> me, AnalysisNode target) {
        String itemName = me.get(ItemField.ITEM_NAME);
        String schemaName = me.get(ItemField.MY_SCHEMA_NAME);

        UpgradedEnumMap<ItemField, String> targetMap = target.getMyMap();

        if (Long.valueOf(me.get(ItemField.ID)).equals(target.getId())) {
            return false;
        }

        if (!targetMap.containsKey(ItemField.ITEM_NAME)
                || !targetMap.containsKey(ItemField.MY_PACKAGE_NAME)
                || !targetMap.containsKey(ItemField.MY_SCHEMA_NAME)
                || targetMap.get(ItemField.ITEM_TYPE)
                .equals(ItemType.CALL.toString())//hardcoded check if its call
                ) {
            return false;
        }

        String targetPackage = targetMap.get(ItemField.MY_PACKAGE_NAME);
        String targetSchema = targetMap.get(ItemField.MY_SCHEMA_NAME);

        return targetPackage.equals(itemName)
                && targetSchema.equals(schemaName);
    }
}
