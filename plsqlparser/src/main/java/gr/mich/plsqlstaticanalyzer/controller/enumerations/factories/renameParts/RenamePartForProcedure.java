package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;

/**
 *
 * @author Michael Michailidis
 */
public class RenamePartForProcedure extends AbstractRenamePart implements RenamePart {
    
    public RenamePartForProcedure() {
        
        this.addItemToList( 
                ItemField.MY_SCHEMA_NAME.hasDependency( ItemField.MY_PACKAGE_NAME )
        );
        
        this.addItemToList( 
                ItemField.MY_PACKAGE_NAME.isDependency( ItemField.MY_SCHEMA_NAME ) 
        );
    }
    
}
