package gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPart;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPart;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePart;

/**
 * The factory interface which describes how the item factory works 
 * 
 * @author Michael Michailidis
 */
public interface ItemGeneratorFunc {
    public int getVal();
    /**
     * 
     * @param l the id
     * @return Returns the node
     */
    public UpgradedEnumMap<ItemField, String> getMap(Long l);
    
    /**
     * The RenamePart of the factory
     * @return Returns the RenamePart of the factory
     */
    public RenamePart getRenamePart();
    
    /**
     * The SynonymPart of the factory
     * @return Returns the SynonymPart of the factory
     */
    public SynonymPart getSynonymPart();
    
    /**
     * The AnalyzerPart of the factory
     * @return Returns the AnalyzerPart of the factory
     */
    public AnalyzerPart getAnalyzerPart();
}
