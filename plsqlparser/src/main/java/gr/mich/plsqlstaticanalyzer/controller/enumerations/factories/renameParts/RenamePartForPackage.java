package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;

/**
 *
 * @author Michael Michailidis
 */
public class RenamePartForPackage extends AbstractRenamePart implements RenamePart {

    public RenamePartForPackage() {
        this.addItemToList( 
                ItemField.MY_PACKAGE_NAME.isDependency(  //itemField
                        ItemField.MY_SCHEMA_NAME, //Matcher -> where should put the data
                        ItemField.ITEM_NAME  //mask -> where it will find the data
                ) 
        );
    }
}
