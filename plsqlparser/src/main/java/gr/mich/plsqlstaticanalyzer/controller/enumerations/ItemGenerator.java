package gr.mich.plsqlstaticanalyzer.controller.enumerations;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForCall;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForFunction;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForMaterializedView;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForMaterializedViewLog;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForPackage;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForProcedure;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForSchema;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForSynonym;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForTable;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForTrigger;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForView;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPart;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForCall;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForFunction;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForLink;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForMaterializedView;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForMaterializedViewLog;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForPackage;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForProcedure;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForSchema;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForSynonym;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForTable;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForTrigger;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPartForView;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPart;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForCall;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForFunction;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForMaterializedView;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForMaterializedViewLog;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForPackage;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForProcedure;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForSchema;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForSynonym;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForTable;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForTrigger;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForView;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.ItemGeneratorFunc;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePart;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.RenamePartForLink;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts.SynonymPartForLink;

/**
 * The ItemGenerator Class is a factory ENUM that generates the items that can be
 * created by the parser. It setup all the basic fields they can contain and all 
 * the functions they can use 
 * 
 * @author Michael Michailidis
 */
public enum ItemGenerator implements ItemGeneratorFunc {
    /**
     * An item of package type
     */
    PACKAGE(ItemType.PACKAGE.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.PACKAGE.toString());
            tmp.put(ItemField.ITEM_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForPackage();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForPackage();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForPackage();
        }
    },
    /**
     * An item of function type
     */
    FUNCTION(ItemType.FUNCTION.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.FUNCTION.toString());
            tmp.put(ItemField.ITEM_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);
            tmp.put(ItemField.MY_PACKAGE_NAME, null);

//            tmp.put(ItemField.IS_PACKAGELESS,Boolean.FALSE.toString());
            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForFunction();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForFunction();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForFunction();
        }
    },
    /**
     * An item of procedure type
     */
    PROCEDURE(ItemType.PROCEDURE.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.PROCEDURE.toString());
            tmp.put(ItemField.ITEM_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);
            tmp.put(ItemField.MY_PACKAGE_NAME, null);

//            tmp.put(ItemField.IS_PACKAGELESS,Boolean.FALSE.toString());            
            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForProcedure();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForProcedure();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForProcedure();
        }
    },
    /**
     * An item of view type
     */
    VIEW(ItemType.VIEW.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.VIEW.toString());
            tmp.put(ItemField.ITEM_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);

            tmp.put(ItemField.FROM_CLAUSE_NAME, ""); //subquery FROM items
            tmp.put(ItemField.FROM_CLAUSE_SCHEMA, ""); //subquery FROM items
            tmp.put(ItemField.FROM_CLAUSE_LINK, ""); //subquery FROM items

            tmp.put(ItemField.APPENDABLE, Boolean.TRUE.toString());

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForView();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForView();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForView();
        }
    },
    /**
     * An item of materialized_view type
     */
    MATERIALIZED_VIEW(ItemType.MATERIALIZED_VIEW.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.MATERIALIZED_VIEW.toString());

            tmp.put(ItemField.ITEM_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);

            tmp.put(ItemField.OTHER_ITEM_NAME, null); // objects
            tmp.put(ItemField.OTHER_SCHEMA_NAME, null); // objects

            tmp.put(ItemField.FROM_CLAUSE_NAME, ""); //subquery FROM items
            tmp.put(ItemField.FROM_CLAUSE_SCHEMA, ""); //subquery FROM items
            tmp.put(ItemField.FROM_CLAUSE_LINK, ""); //subquery FROM items

            tmp.put(ItemField.APPENDABLE, Boolean.TRUE.toString());

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForMaterializedView();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForMaterializedView();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForMaterializedView();
        }
    },
    /**
     * An item of materialized_view_log type
     */
    MATERIALIZED_VIEW_LOG(ItemType.MATERIALIZED_VIEW_LOG.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.MATERIALIZED_VIEW_LOG.toString());

            tmp.put(ItemField.ITEM_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForMaterializedViewLog();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForMaterializedViewLog();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForMaterializedViewLog();
        }
    },
    /**
     * An item of table type
     */
    TABLE(ItemType.TABLE.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.TABLE.toString());

            tmp.put(ItemField.ITEM_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);

            tmp.put(ItemField.OTHER_ITEM_NAME, null); // for object Table. it would be the object
            tmp.put(ItemField.OTHER_SCHEMA_NAME, null); // for object Table. it would be the object

            tmp.put(ItemField.EXTRA_NAME, ""); // for foreign key referances
            tmp.put(ItemField.EXTRA_SCHEMA_NAME, ""); // for foreign key referances

            tmp.put(ItemField.APPENDABLE, Boolean.TRUE.toString());

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForTable();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForTable();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForTable();
        }
    },
    /**
     * An item of trigger type
     */
    TRIGGER(ItemType.TRIGGER.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));
            tmp.put(ItemField.ITEM_TYPE, ItemType.TRIGGER.toString());
            tmp.put(ItemField.MY_SCHEMA_NAME, null);
            tmp.put(ItemField.ITEM_NAME, null);

            tmp.put(ItemField.OTHER_ITEM_NAME, null);
            tmp.put(ItemField.OTHER_SCHEMA_NAME, null);

            //extras are optional so null is allowed
            tmp.put(ItemField.EXTRA_NAME, "");
            tmp.put(ItemField.EXTRA_SCHEMA_NAME, "");

            tmp.put(ItemField.APPENDABLE, Boolean.TRUE.toString());

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForTrigger();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForTrigger();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForTrigger();
        }
    },
    /**
     * An item of schema type
     */
    SCHEMA(ItemType.SCHEMA.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.SCHEMA.toString());
            tmp.put(ItemField.ITEM_NAME, null);

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForSchema();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForSchema();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForSchema();
        }
    },
    /**
     * An item of call type
     */
    CALL(ItemType.CALL.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.CALL.toString());

            tmp.put(ItemField.ITEM_NAME, null); //calls are anonymous. 
            tmp.put(ItemField.MY_PACKAGE_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);

            tmp.put(ItemField.OTHER_ITEM_NAME, null);
            tmp.put(ItemField.OTHER_PACKAGE_NAME, null);
            tmp.put(ItemField.OTHER_SCHEMA_NAME, null);

            tmp.put(ItemField.LINK_NAME, null);

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForCall();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForCall();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            return new AnalyzerPartForCall();
        }
    },
    /**
     * An item of synonym type
     */
    SYNONYM(ItemType.SYNONYM.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.SYNONYM.toString());
            tmp.put(ItemField.ITEM_NAME, null);
            tmp.put(ItemField.MY_SCHEMA_NAME, null);

            tmp.put(ItemField.OTHER_SCHEMA_NAME, null);
            tmp.put(ItemField.OTHER_ITEM_NAME, null);

            tmp.put(ItemField.LINK_NAME, null);

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForSynonym();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForSynonym();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
//            throw new UnsupportedOperationException();
            return new AnalyzerPartForSynonym();
        }
    },
    /**
     * An item of link type
     */
    LINK(ItemType.LINK.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ID, String.valueOf(l));

            tmp.put(ItemField.ITEM_TYPE, ItemType.LINK.toString());
            tmp.put(ItemField.ITEM_NAME, null);
                        
            tmp.deActivateAdding();

            return tmp;
        }
        
        @Override
        public RenamePart getRenamePart() {
            return new RenamePartForLink();
        }

        @Override
        public SynonymPart getSynonymPart() {
            return new SynonymPartForLink();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
//            throw new UnsupportedOperationException();
            return new AnalyzerPartForLink();
        }
    },
    /**
     * An item of unknown type This is meant to be used only as default value
     */
    UNKNOWN(ItemType.UNKNOWN.getLevel()) {
        @Override
        public UpgradedEnumMap<ItemField, String> getMap(Long l) {
            UpgradedEnumMap<ItemField, String> tmp = new UpgradedEnumMap<>(ItemField.class);

            tmp.put(ItemField.ITEM_TYPE, ItemType.UNKNOWN.toString());

            tmp.deActivateAdding();

            return tmp;
        }

        @Override
        public RenamePart getRenamePart() {
            throw new UnsupportedOperationException();
        }

        @Override
        public SynonymPart getSynonymPart() {
            throw new UnsupportedOperationException();
        }

        @Override
        public AnalyzerPart getAnalyzerPart() {
            throw new UnsupportedOperationException();
        }
    }; //Null is not a good policy    

    /**
     * Converts a String to the matching factory
     * @param str the String to be converted
     * @return returns a factory which is the converted string name
     * if the converted string name cannot match any factory then the 
     * UNKNOWN factory is returned
     */
    public static ItemGenerator getType(String str) {
        for (ItemGenerator vLookUp : values()) {
            if (vLookUp.toString().equalsIgnoreCase(str)) {
                return vLookUp;
            }
        }

        return UNKNOWN;
    }

    private final int val;

    private ItemGenerator(int i) {
        this.val = i;
    }

    /**
     * Convert an itemType to the RenamePart of the factory it 
     * represent
     * @param itemType The String which will be converted to factory
     * @return Returns the RenamePart of the converted factory
     */
    public static RenamePart getRenamePart(String itemType) {
        return getType(itemType).getRenamePart();
    }
    
    /**
     * Convert an itemType to the SynonymPart of the factory it 
     * represent
     * @param itemType The String which will be converted to factory
     * @return Returns the SynonymPart of the converted factory
     */
    public static SynonymPart getSynonymPart(String itemType) {
        return getType(itemType).getSynonymPart();
    }
    
    /**
     * Convert an itemType to the AnalyzerPart of the factory it 
     * represent
     * @param itemType The String which will be converted to factory
     * @return Returns the AnalyzerPart of the converted factory
     */
    public static AnalyzerPart getAnalyzerPart(String itemType) {
        return getType(itemType).getAnalyzerPart();
    }

    @Override
    public int getVal() {
        return val;
    }

}
