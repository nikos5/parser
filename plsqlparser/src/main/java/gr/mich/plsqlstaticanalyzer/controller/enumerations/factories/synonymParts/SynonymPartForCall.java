package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;

/**
 *
 * @author Michael Michailidis
 */
public class SynonymPartForCall implements SynonymPart {

    @Override
    public UpgradedEnumMap rename(UpgradedEnumMap otherValue, UpgradedEnumMap synonymValue) {
         //getting the values on string variables for easier use
        String synonymOtherSchemaName
                = synonymValue.get(ItemField.OTHER_SCHEMA_NAME).toString();

        String itemName
                = synonymValue.get(ItemField.ITEM_NAME).toString();
        String schemaName
                = synonymValue.get(ItemField.MY_SCHEMA_NAME).toString();
        
        String otherName
                = otherValue.get(ItemField.MY_PACKAGE_NAME).toString();
        String otherSchemaName
                = otherValue.get(ItemField.MY_SCHEMA_NAME).toString();

        //checking if the name / schema are matching synonyms
        if (itemName.equals(otherName) && schemaName.equals(otherSchemaName)) {
            //if they are matched they are replaced by the values that synonym 
            //points to
            otherValue.put(
                    ItemField.MY_PACKAGE_NAME,
                    synonymValue.get(
                            ItemField.OTHER_ITEM_NAME
                    )
            );

            if ( !synonymOtherSchemaName
                    .equals(ItemField.OTHER_SCHEMA_NAME.getDefault())||
                    !schemaName.equals(ItemField.MY_SCHEMA_NAME.getDefault()) ) {   
                otherValue.put(
                        ItemField.MY_SCHEMA_NAME,
                        synonymOtherSchemaName
                );
            }
            
            return otherValue;
        }
        
        //  Next.. the schema default value is checked if needs to be changed again
        // to match the second possible outcome
        if (schemaName
                .equals(ItemField.MY_SCHEMA_NAME.getDefault())) {
            schemaName = ItemField.OTHER_SCHEMA_NAME.getDefault();
        }

        otherName
                = otherValue.get(ItemField.OTHER_PACKAGE_NAME).toString();
        otherSchemaName
                = otherValue.get(ItemField.OTHER_SCHEMA_NAME).toString();
        
        //checking if the name / schema of the other fields are matching synonyms
        if (itemName.equals(otherName) && schemaName.equals(otherSchemaName)) {
            otherValue.put(
                    ItemField.OTHER_PACKAGE_NAME,
                    synonymValue.get(
                            ItemField.OTHER_ITEM_NAME
                    )
            );

            if ( !synonymOtherSchemaName
                    .equals(ItemField.OTHER_SCHEMA_NAME.getDefault()) ||
                    !schemaName.equals(ItemField.OTHER_SCHEMA_NAME.getDefault()) ) {   
                otherValue.put(
                        ItemField.OTHER_SCHEMA_NAME,
                        synonymOtherSchemaName
                );
            }
            
            //retrieving the link value of the synonym to check if its default or 
            //if it has a value
            String linkTemporary = synonymValue.get(ItemField.LINK_NAME).toString();
            
            //if the link has a value only then it will be updated in the item
            //because the item may have link on its own and we dont wont to lose it
            if( !linkTemporary.equals(ItemField.LINK_NAME.getDefault()) ) {
                otherValue.put(
                        ItemField.LINK_NAME, 
                        linkTemporary
                );
            }
        }

        return otherValue;
    }
    
}
