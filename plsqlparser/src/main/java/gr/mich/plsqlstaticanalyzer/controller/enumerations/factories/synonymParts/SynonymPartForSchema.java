package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;

/**
 *
 * @author Michael Michailidis
 */
public class SynonymPartForSchema implements SynonymPart{

    @Override
    public UpgradedEnumMap rename(UpgradedEnumMap otherValue, UpgradedEnumMap synonymValue) {
        String itemName
                = synonymValue.get(ItemField.ITEM_NAME).toString();
        
        String otherName
                = otherValue.get(ItemField.ITEM_NAME).toString();

        //checking if the name is matching synonyms
        if (itemName.equals(otherName)) {
            //if it ismatched it is replaced by the value that synonym 
            //points to
            otherValue.put(
                    ItemField.ITEM_NAME,
                    synonymValue.get(
                            ItemField.OTHER_ITEM_NAME
                    )
            );
        }

        return otherValue;
    }
    
}
