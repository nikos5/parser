package gr.mich.plsqlstaticanalyzer.controller.helper;

import java.util.Objects;

/**
 * Pairs two strings in one item
 * those string are the PackageName and the SchemaName
 * @author Michael Michailidis
 */
public class TwoArgPair {
    private final String packageName;
    private final String schemaName;

    /**
     * The pairing constructor. 
     * @param packageName the name of the package
     * @param schemaName the name of the schema
     */
    public TwoArgPair(String packageName, String schemaName) {
        this.packageName = packageName;
        this.schemaName = schemaName;
    }

    /**
     * The package name
     * @return Returns the package Name
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * The schema name
     * @return Returns the schema Name
     */
    public String getSchemaName() {
        return schemaName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.packageName);
        hash = 53 * hash + Objects.hashCode(this.schemaName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TwoArgPair other = (TwoArgPair) obj;
        if (!Objects.equals(this.packageName, other.packageName)) {
            return false;
        }
        if (!Objects.equals(this.schemaName, other.schemaName)) {
            return false;
        }
        return true;
    }
    
}
