package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;

/**
 * An interface to assist the {@link gr.mich.plsqlstaticanalyzer.controller.ControlCore ControlCore} 
 * on the synonym renaming function. Each node should have its own SynonymPart that 
 * will affect only the correct fields when its being renamed
 * 
 * @author Michael Michailidis
 */
public interface SynonymPart {
    /**
     * The renaming function for the synonym rename
     * 
     * @param otherValue The other node
     * @param synonymValue The synonym value that will rename
     * @return Returns the renamed node
     */
    UpgradedEnumMap rename(UpgradedEnumMap otherValue, 
            UpgradedEnumMap synonymValue);
}
