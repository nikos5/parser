package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.helper.Pair;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.List;

/**
 * An interface to assist the {@link gr.mich.plsqlstaticanalyzer.controller.ControlCore ControlCore} 
 * on the renaming function. Each ItemType should have its own RenamePart that 
 * will affect only the correct fields when its being renamed
 * 
 * @author Michael Michailidis
 */
public interface RenamePart {

    /**
     * Sets the node that will be renamed
     * @param map The node which will be setup
     */
    void setMap(UpgradedEnumMap map);

    /**
     * The fields that will be renamed
     * @return Returns the fields that will be renamed
     */
    List<Pair> getFieldForUpdate();
    
    /**
     * Rename the fields if they can be renamed
     * @param fields the fields to rename
     */
    void updateFields(List<Pair> fields);
    
    /**
     * The node that the rename took place on
     * @return Returns the node that the rename took place on
     */
    UpgradedEnumMap getMap();
    
    /**
     * Checks the id of the node that it contains with the id that it was given
     * @param id The input id to be checked
     * @return Returns true if the id of the node is equals to the input id
     */
    boolean idCheck(long id);
    
    
}
