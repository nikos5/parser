package gr.mich;

import gr.mich.plsqlstaticanalyzer.controller.ControlCore;
import gr.mich.plsqlstaticanalyzer.controller.antlrCode.plsqlLexer;
import gr.mich.plsqlstaticanalyzer.controller.antlrCode.plsqlParser;
import gr.mich.plsqlstaticanalyzer.controller.antlrCode.strippingLexer;
import gr.mich.plsqlstaticanalyzer.exceptions.ExceptionCore;
import gr.mich.plsqlstaticanalyzer.exceptions.MyAntlrErrorListener;
import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.Lexer;
import org.apache.commons.cli.*;
import org.openide.util.Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;

/**
 *
 * @author Michael Michailidis
 */
public class Application {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException If the program cant read from the disk
     */
    public static void main(String[] args) throws IOException {
        boolean isDebug = false;

        if (isDebug) {
            URL resource;
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();

            resource = classLoader.getResource("lastTestScrabbled.sql");
//          resource = classLoader.getResource("testFullMap.sql");
//          resource = classLoader.getResource("testGeneral.sql");
//          resource = classLoader.getResource("testAnalyzer.sql");
//          resource = classLoader.getResource("testOutput.sql");
//          resource = classLoader.getResource("testNameInheritance.sql");
//          resource = classLoader.getResource("testCallError.sql");
//          resource = classLoader.getResource("developerSql.sql");

            execute(resource);
            return;
        }

        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine;

        //Create acceptable arguments
        Options options = createOptions();

        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException exp) {
            // automatically generate the help statement
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("Parser Options", options);

            System.err.println("Parsing failed.  Reason: " + exp.getMessage());

            return;
        }

        if (args.length > 0) {
            defineArguments(commandLine, options);
        } else {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("Parser Options", options);
        }
    }

    private static Options createOptions() {
        // create Options object
        Options options = new Options();

        // add help option
        options.addOption("h", "help", false, "A list of available commands.");
        // add file option
        options.addOption("f", "file", true, "Define a SQL file for parsing.");
        // add remove useless code option
        options.addOption("ruc", "remove useless code", false, "Remove useless code for efficiency.");
        // add debugging mode
        options.addOption("d", "debug",false, "All information will be provided for debugging.");
        // add warning mode
        options.addOption("w", "warning", false, "Errors and warnings will be provided without any debugging information.");

        return options;
    }

    private static void defineArguments(CommandLine commandLine, Options options) throws IOException {
        File file;
        URL resource;

        //Help option
        if (commandLine.hasOption("h")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("Parser Options", options);
            return;
        }

        //File option
        if (commandLine.hasOption("f")) {
            file = new File(commandLine.getOptionValue("f"));
            resource = Utilities.toURI(file).toURL();

            if (!fileValidation(file)) {
                return;
            }
        } else {
            System.out.println("\nThe Parser requires a SQL file to continue. Please set it by using the file attribute.\n");
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("Parser Options", options);

            return;
        }

        //Remove useless code option
        if (commandLine.hasOption("ruc")) {
            removeUselessCode(resource);
        }

        //Log level options
        if (commandLine.hasOption("d")) {
            ExceptionCore.enableDebuggingLevel();
        } else if (commandLine.hasOption("w")) {
            ExceptionCore.enableWarningLevel();
        } else {
            ExceptionCore.enableDefaultLevel();
        }

        //Run the program
        execute(resource);
    }

    /**
     * Will strip any comments or useless commands
     * that are found and wil replace the original
     * SQL file with the new one which doesn't include any comments.
     *
     * @param input is the file location.
     * @throws IOException If the program can't read from the disk
     */
    private static void removeUselessCode(URL input) throws IOException {
        CharStream in = new ANTLRFileStream(input.getFile());
        strippingLexer lexer = new strippingLexer(in);

        CommonTokenStream stream = new CommonTokenStream(lexer);
        stream.fill();

        replaceFile(input, stream);
    }

    /**
     * @param input is the file resource
     * @param stream is the new content that will
     *               replace the old content of the
     *               file.
     */
    private static void replaceFile(URL input, CommonTokenStream stream) {

        try(PrintStream out = new PrintStream(new FileOutputStream(input.getFile()))) {
            out.print(stream.getText());
        }
        catch(Exception e)
            {
                e.printStackTrace();
            }
    }


    /**
     * Will check if the file exists and
     * follows the SQL format.
     *
     * @param file is the file that will be checked.
     * @return true if the file exists and it's in SQL format.
     */
    private static boolean fileValidation(File file) {
        if(file.exists()) {
            if (getFileExtension(file).equals("sql")){
                return true;
            }else{
                System.out.println("'" + file.getName() + "' is not a compatible file.");
            }
        }else{
            System.out.println("File '" + file.getName() + "' does not exist.");
        }
        return false;
    }

    /**
     * Will find the extension of a file and then
     * return it in a String format.
     *
     * @param file is the file we want the extension from.
     * @return the extension or an empty string if it didn't find any extension.
     */
    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
    
    /**
     * Executes the full API by reading the input
     * 
     * @param input The {@link URL} which contains the {@link File} which will 
     *        be the input
     * @throws IOException If the program fails to read from the disk
     */
    public static void execute(URL input) throws IOException {
        CharStream in = new ANTLRFileStream(input.getFile());

        Lexer lexer = new plsqlLexer(in);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        plsqlParser parser = new plsqlParser(tokens);
        parser.addErrorListener(new MyAntlrErrorListener());

        parser.compilation_unit();

        ControlCore CC = parser.getCore();

        boolean flag = CC.validateNames();

        if(!flag) {
            System.out.println("Critical error - item without name");
        }
                        
        CC.optimize();
        CC.executeAnalysis();

        if (ExceptionCore.wasCalled()) {
            System.out.println("Exceptions occurred. Check out the 'ErrLog' file for more information.");
        }else{
            System.out.println("The analysis has successfully finished for " + input.toString());
        }
    }
}
